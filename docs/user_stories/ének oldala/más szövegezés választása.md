Más szövegezés választása

---

Egy énekhez több szövegezés is elérhető, ezek közül többet is megvizsgálhatok
 
---

**Mint egy** felhasználó
**Amikor** egy másik szövegezést választok
**Akkor** más szöveg jelenik meg a nézegetőben a hozzá rendelt adatokkal

---

prioritás: 