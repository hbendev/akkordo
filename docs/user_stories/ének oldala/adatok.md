Énekhez megjelenő adatok

---

Ezek az adatok jelennek meg, amikor egy ének részletei oldalon tartózkodunk

---

**Mint egy** felhasználó
**Amikor** egy ének részletei oldalán vagyok
**Akkor** a következő adatokat látom: Ének címe, előadója, fordítója, feltöltött kották, feltöltött felvételek, hozzárendelt cimkék, szövegek és kommentek.

---

prioritás: 10
