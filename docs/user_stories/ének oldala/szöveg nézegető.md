Szöveg megtekintése

---

Az ének részletei odlalon lehetőség van a szövegezések megtekintésére
 
---

**Mint egy** felhasználó
**Amikor** az ének részletei oldalon vagyok,
**Akkor** megtudom tekinteni a dal különböző szövegezéseit. A következő adatok is megjelennek a szöveg mellett: hányszor volt felhasználva, melyik felhasználó adta hozzá. Egy vetítő gomb is megjelenik.

---

prioritás: 