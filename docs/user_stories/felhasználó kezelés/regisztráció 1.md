Regisztráció

---

Fiók létrehozásának lehetősége
 
---

**Mint egy** fiók nélküli felhasználó
**Amikor** kitöltöm az adataimat és a regisztráció gombra nyomok
**Akkor** egy fiókom létrejön, amibe automatikusan be is léptetődök

---

prioritás: 10