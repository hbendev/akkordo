Énekrend és vetítés szerkesztő felület

---

Egy közös felületen lehet szerkeszteni egy énekrendet és a hozzá tartozó vetítést
 
---

**Mint egy** belépett felhasználó
**Amikor** egy énekrendet szeretnék szerkeszteni / készíteni
**Akkor** rendelkezésemre áll egy felület, ahol az énekrendet és a hozzá tartozó vetítést egy helyen szerkeszthetem.

---

prioritás: 