Vetítésbe egyedi dia beillesztése

---

Lehetséges egy szövegszerkesztőn át egyedi tartalmat felvenni a vetítésünkbe.

---

**Mint egy** belépett felhasználó
**Amikor** egy diához nem rendelek hozzá egy éneket
**Akkor** lehetőségem van egy szövegszerkesztő segítségével egyedi szövegs tartalmat hozzáadni a diához

---

prioritás:
