Énekek tartalmai

---

Ezek a mezők jelennek meg az énekek listájában
 
---

**Mint egy** ének
**Amikor** egy listában jelenek meg
**Akkor** a következő mezőim és gombjaim jelennek meg, amennyiben elérhetőek: cím, szerző, lejátszás gomb, vetítés gomb, kotta gomb, cimkék.

---

prioritás: 10