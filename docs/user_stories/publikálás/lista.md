Publikálható tartalmak listája

---

Az oldalon több különböző fajta tartalom publikussá tehető, ezek lista nézetben csak a tartalomkezelhető fiókoknak látszódnak

---

**Mint egy** belépett adminisztrátor felhasználó
**Amikor** az énekek, felvételek, kották, kommentek vagy szövegek oldalra navigálok
**Akkor** ezek egy táblázatban megjelennek

---

prioritás:
