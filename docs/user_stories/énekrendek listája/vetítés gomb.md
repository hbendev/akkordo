Vetítés elérése a listából

---

Lehetőség van a vetítést elindítani a listanézetből

---

**Mint egy** belépett felhasználó
**Amikor** egy kilistázott, korábban létrehozott énekrenden a vetítés gombra nyomok
**Akkor** a vetítés teljes képernyőt lefedő módon megjelenik

---

prioritás:
