Tartalom megjelenítése, amikor a felhasználónak még nincs énekrendje

---

Előfordulhat, hogy a felhasználó még nem készített énekrendet/vetítést, ekkor más tartalom jelenik meg az oldalon

---

**Mint egy** belépett felhasználó
**Amikor** még nem készítettem énekrendet
**Akkor** az üres lista alatt egy gomb jelenik meg, amivel új énekrendet készíthetek

---

prioritás:
