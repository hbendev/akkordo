# Setup

## System requirements

### UI

- Node v14^

### API

- Node v12^

## Additional steps required

### UI

- Npm run gql-generate needs to be ran every time the types exported by the GraphQL server change
- Currently presentation styles are manually moved to the public folder from node_modules. The following files need be copied from reveal.js: the main .css file, the black theme and the fonts.

### API

- If schema changes have been finalized run npm run deploy
- Run new seed file if needed
