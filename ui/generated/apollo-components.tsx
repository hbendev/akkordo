import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: Date;
  Json: { [key: string]: any };
  /** The `Upload` scalar type represents a file upload. */
  Upload: File | string;
};

export enum AppError {
  AuthInvalid = 'AUTH_INVALID',
  AuthLocked = 'AUTH_LOCKED',
  AuthNoPermission = 'AUTH_NO_PERMISSION',
  FileTooLarge = 'FILE_TOO_LARGE',
  NoSuchRecord = 'NO_SUCH_RECORD',
  ServerError = 'SERVER_ERROR'
}

export type AuthPayload = {
  token: Scalars['String'];
  user?: Maybe<User>;
};

/** Batch payloads from prisma. */
export type BatchPayload = {
  /** Prisma Batch Payload */
  count: Scalars['Int'];
};

export type BoolFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['Boolean']>;
};

export type BoolFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolFilter>;
};

export type BoolNullableFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolNullableFilter>;
};

export type BoolNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedBoolNullableFilter>;
  _min?: InputMaybe<NestedBoolNullableFilter>;
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolNullableWithAggregatesFilter>;
};

export type BoolWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedBoolFilter>;
  _min?: InputMaybe<NestedBoolFilter>;
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolWithAggregatesFilter>;
};

export type DateTimeFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['DateTime']>;
};

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<NestedDateTimeFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export type DateTimeWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedDateTimeFilter>;
  _min?: InputMaybe<NestedDateTimeFilter>;
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<NestedDateTimeWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export type EnumPublishStatusNullableFilter = {
  equals?: InputMaybe<PublishStatus>;
  in?: InputMaybe<Array<PublishStatus>>;
  not?: InputMaybe<PublishStatus>;
  notIn?: InputMaybe<Array<PublishStatus>>;
};

export type EnumPublishStatusNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedEnumPublishStatusNullableFilter>;
  _min?: InputMaybe<NestedEnumPublishStatusNullableFilter>;
  equals?: InputMaybe<PublishStatus>;
  in?: InputMaybe<Array<PublishStatus>>;
  not?: InputMaybe<PublishStatus>;
  notIn?: InputMaybe<Array<PublishStatus>>;
};

export type EnumRecordingTypeNullableFilter = {
  equals?: InputMaybe<RecordingType>;
  in?: InputMaybe<Array<RecordingType>>;
  not?: InputMaybe<RecordingType>;
  notIn?: InputMaybe<Array<RecordingType>>;
};

export type EnumRecordingTypeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedEnumRecordingTypeNullableFilter>;
  _min?: InputMaybe<NestedEnumRecordingTypeNullableFilter>;
  equals?: InputMaybe<RecordingType>;
  in?: InputMaybe<Array<RecordingType>>;
  not?: InputMaybe<RecordingType>;
  notIn?: InputMaybe<Array<RecordingType>>;
};

export type EnumVerseTypeNullableFilter = {
  equals?: InputMaybe<VerseType>;
  in?: InputMaybe<Array<VerseType>>;
  not?: InputMaybe<VerseType>;
  notIn?: InputMaybe<Array<VerseType>>;
};

export type EnumVerseTypeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedEnumVerseTypeNullableFilter>;
  _min?: InputMaybe<NestedEnumVerseTypeNullableFilter>;
  equals?: InputMaybe<VerseType>;
  in?: InputMaybe<Array<VerseType>>;
  not?: InputMaybe<VerseType>;
  notIn?: InputMaybe<Array<VerseType>>;
};

export type IntFieldUpdateOperationsInput = {
  decrement?: InputMaybe<Scalars['Int']>;
  divide?: InputMaybe<Scalars['Int']>;
  increment?: InputMaybe<Scalars['Int']>;
  multiply?: InputMaybe<Scalars['Int']>;
  set?: InputMaybe<Scalars['Int']>;
};

export type IntFilter = {
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type IntNullableFilter = {
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type IntNullableWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatNullableFilter>;
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedIntNullableFilter>;
  _min?: InputMaybe<NestedIntNullableFilter>;
  _sum?: InputMaybe<NestedIntNullableFilter>;
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type IntWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatFilter>;
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedIntFilter>;
  _min?: InputMaybe<NestedIntFilter>;
  _sum?: InputMaybe<NestedIntFilter>;
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type JsonFilter = {
  array_contains?: InputMaybe<Scalars['Json']>;
  array_ends_with?: InputMaybe<Scalars['Json']>;
  array_starts_with?: InputMaybe<Scalars['Json']>;
  equals?: InputMaybe<Scalars['Json']>;
  gt?: InputMaybe<Scalars['Json']>;
  gte?: InputMaybe<Scalars['Json']>;
  lt?: InputMaybe<Scalars['Json']>;
  lte?: InputMaybe<Scalars['Json']>;
  not?: InputMaybe<Scalars['Json']>;
  path?: InputMaybe<Array<Scalars['String']>>;
  string_contains?: InputMaybe<Scalars['String']>;
  string_ends_with?: InputMaybe<Scalars['String']>;
  string_starts_with?: InputMaybe<Scalars['String']>;
};

export enum JsonNullValueFilter {
  AnyNull = 'AnyNull',
  DbNull = 'DbNull',
  JsonNull = 'JsonNull'
}

export enum JsonNullValueInput {
  JsonNull = 'JsonNull'
}

export type JsonNullableFilter = {
  array_contains?: InputMaybe<Scalars['Json']>;
  array_ends_with?: InputMaybe<Scalars['Json']>;
  array_starts_with?: InputMaybe<Scalars['Json']>;
  equals?: InputMaybe<Scalars['Json']>;
  gt?: InputMaybe<Scalars['Json']>;
  gte?: InputMaybe<Scalars['Json']>;
  lt?: InputMaybe<Scalars['Json']>;
  lte?: InputMaybe<Scalars['Json']>;
  not?: InputMaybe<Scalars['Json']>;
  path?: InputMaybe<Array<Scalars['String']>>;
  string_contains?: InputMaybe<Scalars['String']>;
  string_ends_with?: InputMaybe<Scalars['String']>;
  string_starts_with?: InputMaybe<Scalars['String']>;
};

export type JsonNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedJsonNullableFilter>;
  _min?: InputMaybe<NestedJsonNullableFilter>;
  array_contains?: InputMaybe<Scalars['Json']>;
  array_ends_with?: InputMaybe<Scalars['Json']>;
  array_starts_with?: InputMaybe<Scalars['Json']>;
  equals?: InputMaybe<Scalars['Json']>;
  gt?: InputMaybe<Scalars['Json']>;
  gte?: InputMaybe<Scalars['Json']>;
  lt?: InputMaybe<Scalars['Json']>;
  lte?: InputMaybe<Scalars['Json']>;
  not?: InputMaybe<Scalars['Json']>;
  path?: InputMaybe<Array<Scalars['String']>>;
  string_contains?: InputMaybe<Scalars['String']>;
  string_ends_with?: InputMaybe<Scalars['String']>;
  string_starts_with?: InputMaybe<Scalars['String']>;
};

export type JsonWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedJsonFilter>;
  _min?: InputMaybe<NestedJsonFilter>;
  array_contains?: InputMaybe<Scalars['Json']>;
  array_ends_with?: InputMaybe<Scalars['Json']>;
  array_starts_with?: InputMaybe<Scalars['Json']>;
  equals?: InputMaybe<Scalars['Json']>;
  gt?: InputMaybe<Scalars['Json']>;
  gte?: InputMaybe<Scalars['Json']>;
  lt?: InputMaybe<Scalars['Json']>;
  lte?: InputMaybe<Scalars['Json']>;
  not?: InputMaybe<Scalars['Json']>;
  path?: InputMaybe<Array<Scalars['String']>>;
  string_contains?: InputMaybe<Scalars['String']>;
  string_ends_with?: InputMaybe<Scalars['String']>;
  string_starts_with?: InputMaybe<Scalars['String']>;
};

export type LikeCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type LikeCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutLikesInput;
  user: UserCreateNestedOneWithoutLikesInput;
};

export type LikeCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
  userId: Scalars['String'];
};

export type LikeCreateManySongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  userId: Scalars['String'];
};

export type LikeCreateManySongInputEnvelope = {
  data: Array<LikeCreateManySongInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type LikeCreateManyUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
};

export type LikeCreateManyUserInputEnvelope = {
  data: Array<LikeCreateManyUserInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type LikeCreateNestedManyWithoutSongInput = {
  connect?: InputMaybe<Array<LikeWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LikeCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<LikeCreateWithoutSongInput>;
  createMany?: InputMaybe<LikeCreateManySongInputEnvelope>;
};

export type LikeCreateNestedManyWithoutUserInput = {
  connect?: InputMaybe<Array<LikeWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LikeCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<LikeCreateWithoutUserInput>;
  createMany?: InputMaybe<LikeCreateManyUserInputEnvelope>;
};

export type LikeCreateOrConnectWithoutSongInput = {
  create: LikeCreateWithoutSongInput;
  where: LikeWhereUniqueInput;
};

export type LikeCreateOrConnectWithoutUserInput = {
  create: LikeCreateWithoutUserInput;
  where: LikeWhereUniqueInput;
};

export type LikeCreateWithoutSongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  user: UserCreateNestedOneWithoutLikesInput;
};

export type LikeCreateWithoutUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutLikesInput;
};

export type LikeListRelationFilter = {
  every?: InputMaybe<LikeWhereInput>;
  none?: InputMaybe<LikeWhereInput>;
  some?: InputMaybe<LikeWhereInput>;
};

export type LikeMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type LikeMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type LikeOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type LikeOrderByWithAggregationInput = {
  _count?: InputMaybe<LikeCountOrderByAggregateInput>;
  _max?: InputMaybe<LikeMaxOrderByAggregateInput>;
  _min?: InputMaybe<LikeMinOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type LikeOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  song?: InputMaybe<SongOrderByWithRelationInput>;
  songId?: InputMaybe<SortOrder>;
  user?: InputMaybe<UserOrderByWithRelationInput>;
  userId?: InputMaybe<SortOrder>;
};

export enum LikeScalarFieldEnum {
  CreatedAt = 'createdAt',
  Id = 'id',
  SongId = 'songId',
  UserId = 'userId'
}

export type LikeScalarWhereInput = {
  AND?: InputMaybe<Array<LikeScalarWhereInput>>;
  NOT?: InputMaybe<Array<LikeScalarWhereInput>>;
  OR?: InputMaybe<Array<LikeScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  songId?: InputMaybe<StringFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type LikeScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<LikeScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<LikeScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<LikeScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  songId?: InputMaybe<StringWithAggregatesFilter>;
  userId?: InputMaybe<StringWithAggregatesFilter>;
};

export type LikeUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutLikesNestedInput>;
  user?: InputMaybe<UserUpdateOneRequiredWithoutLikesNestedInput>;
};

export type LikeUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type LikeUpdateManyWithWhereWithoutSongInput = {
  data: LikeUpdateManyMutationInput;
  where: LikeScalarWhereInput;
};

export type LikeUpdateManyWithWhereWithoutUserInput = {
  data: LikeUpdateManyMutationInput;
  where: LikeScalarWhereInput;
};

export type LikeUpdateManyWithoutSongNestedInput = {
  connect?: InputMaybe<Array<LikeWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LikeCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<LikeCreateWithoutSongInput>;
  createMany?: InputMaybe<LikeCreateManySongInputEnvelope>;
  delete?: InputMaybe<Array<LikeWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<LikeScalarWhereInput>>;
  disconnect?: InputMaybe<Array<LikeWhereUniqueInput>>;
  set?: InputMaybe<Array<LikeWhereUniqueInput>>;
  update?: InputMaybe<Array<LikeUpdateWithWhereUniqueWithoutSongInput>>;
  updateMany?: InputMaybe<Array<LikeUpdateManyWithWhereWithoutSongInput>>;
  upsert?: InputMaybe<Array<LikeUpsertWithWhereUniqueWithoutSongInput>>;
};

export type LikeUpdateManyWithoutUserNestedInput = {
  connect?: InputMaybe<Array<LikeWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LikeCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<LikeCreateWithoutUserInput>;
  createMany?: InputMaybe<LikeCreateManyUserInputEnvelope>;
  delete?: InputMaybe<Array<LikeWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<LikeScalarWhereInput>>;
  disconnect?: InputMaybe<Array<LikeWhereUniqueInput>>;
  set?: InputMaybe<Array<LikeWhereUniqueInput>>;
  update?: InputMaybe<Array<LikeUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: InputMaybe<Array<LikeUpdateManyWithWhereWithoutUserInput>>;
  upsert?: InputMaybe<Array<LikeUpsertWithWhereUniqueWithoutUserInput>>;
};

export type LikeUpdateWithWhereUniqueWithoutSongInput = {
  data: LikeUpdateWithoutSongInput;
  where: LikeWhereUniqueInput;
};

export type LikeUpdateWithWhereUniqueWithoutUserInput = {
  data: LikeUpdateWithoutUserInput;
  where: LikeWhereUniqueInput;
};

export type LikeUpdateWithoutSongInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  user?: InputMaybe<UserUpdateOneRequiredWithoutLikesNestedInput>;
};

export type LikeUpdateWithoutUserInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutLikesNestedInput>;
};

export type LikeUpsertWithWhereUniqueWithoutSongInput = {
  create: LikeCreateWithoutSongInput;
  update: LikeUpdateWithoutSongInput;
  where: LikeWhereUniqueInput;
};

export type LikeUpsertWithWhereUniqueWithoutUserInput = {
  create: LikeCreateWithoutUserInput;
  update: LikeUpdateWithoutUserInput;
  where: LikeWhereUniqueInput;
};

export type LikeWhereInput = {
  AND?: InputMaybe<Array<LikeWhereInput>>;
  NOT?: InputMaybe<Array<LikeWhereInput>>;
  OR?: InputMaybe<Array<LikeWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserWhereInput>;
  userId?: InputMaybe<StringFilter>;
};

export type LikeWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type Lyrics = {
  createdAt: Scalars['DateTime'];
  creator: User;
  id: Scalars['ID'];
  publishStatus?: Maybe<PublishStatus>;
  song?: Maybe<Song>;
  updatedAt: Scalars['DateTime'];
  verses: Array<Verse>;
};


export type LyricsVersesArgs = {
  cursor?: InputMaybe<VerseWhereUniqueInput>;
  distinct?: InputMaybe<Array<VerseScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<VerseOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VerseWhereInput>;
};

export type LyricsCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  verseIds?: InputMaybe<SortOrder>;
};

export type LyricsCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  song?: InputMaybe<SongCreateNestedOneWithoutLyricsInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
  verses?: InputMaybe<VerseCreateNestedManyWithoutLyricsInput>;
};

export type LyricsCreateManyCreatorInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songId?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
};

export type LyricsCreateManyCreatorInputEnvelope = {
  data: Array<LyricsCreateManyCreatorInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type LyricsCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  creatorId: Scalars['String'];
  id?: InputMaybe<Scalars['String']>;
  songId?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
};

export type LyricsCreateManySongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  creatorId: Scalars['String'];
  id?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
};

export type LyricsCreateManySongInputEnvelope = {
  data: Array<LyricsCreateManySongInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type LyricsCreateNestedManyWithoutCreatorInput = {
  connect?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LyricsCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<LyricsCreateWithoutCreatorInput>;
  createMany?: InputMaybe<LyricsCreateManyCreatorInputEnvelope>;
};

export type LyricsCreateNestedManyWithoutSongInput = {
  connect?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LyricsCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<LyricsCreateWithoutSongInput>;
  createMany?: InputMaybe<LyricsCreateManySongInputEnvelope>;
};

export type LyricsCreateNestedOneWithoutVersesInput = {
  connect?: InputMaybe<LyricsWhereUniqueInput>;
  connectOrCreate?: InputMaybe<LyricsCreateOrConnectWithoutVersesInput>;
  create?: InputMaybe<LyricsCreateWithoutVersesInput>;
};

export type LyricsCreateOrConnectWithoutCreatorInput = {
  create: LyricsCreateWithoutCreatorInput;
  where: LyricsWhereUniqueInput;
};

export type LyricsCreateOrConnectWithoutSongInput = {
  create: LyricsCreateWithoutSongInput;
  where: LyricsWhereUniqueInput;
};

export type LyricsCreateOrConnectWithoutVersesInput = {
  create: LyricsCreateWithoutVersesInput;
  where: LyricsWhereUniqueInput;
};

export type LyricsCreateWithoutCreatorInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  song?: InputMaybe<SongCreateNestedOneWithoutLyricsInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
  verses?: InputMaybe<VerseCreateNestedManyWithoutLyricsInput>;
};

export type LyricsCreateWithoutSongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
  verses?: InputMaybe<VerseCreateNestedManyWithoutLyricsInput>;
};

export type LyricsCreateWithoutVersesInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  song?: InputMaybe<SongCreateNestedOneWithoutLyricsInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
};

export type LyricsCreateverseIdsInput = {
  set: Array<Scalars['String']>;
};

export type LyricsListRelationFilter = {
  every?: InputMaybe<LyricsWhereInput>;
  none?: InputMaybe<LyricsWhereInput>;
  some?: InputMaybe<LyricsWhereInput>;
};

export type LyricsMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type LyricsMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type LyricsOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type LyricsOrderByWithAggregationInput = {
  _count?: InputMaybe<LyricsCountOrderByAggregateInput>;
  _max?: InputMaybe<LyricsMaxOrderByAggregateInput>;
  _min?: InputMaybe<LyricsMinOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  verseIds?: InputMaybe<SortOrder>;
};

export type LyricsOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  creator?: InputMaybe<UserOrderByWithRelationInput>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  song?: InputMaybe<SongOrderByWithRelationInput>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  verseIds?: InputMaybe<SortOrder>;
  verses?: InputMaybe<VerseOrderByRelationAggregateInput>;
};

export type LyricsRelationFilter = {
  is?: InputMaybe<LyricsWhereInput>;
  isNot?: InputMaybe<LyricsWhereInput>;
};

export enum LyricsScalarFieldEnum {
  CreatedAt = 'createdAt',
  CreatorId = 'creatorId',
  Id = 'id',
  PublishStatus = 'publishStatus',
  SongId = 'songId',
  UpdatedAt = 'updatedAt',
  VerseIds = 'verseIds'
}

export type LyricsScalarWhereInput = {
  AND?: InputMaybe<Array<LyricsScalarWhereInput>>;
  NOT?: InputMaybe<Array<LyricsScalarWhereInput>>;
  OR?: InputMaybe<Array<LyricsScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creatorId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  songId?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  verseIds?: InputMaybe<StringNullableListFilter>;
};

export type LyricsScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<LyricsScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<LyricsScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<LyricsScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  creatorId?: InputMaybe<StringWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableWithAggregatesFilter>;
  songId?: InputMaybe<StringNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  verseIds?: InputMaybe<StringNullableListFilter>;
};

export type LyricsUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneWithoutLyricsNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
  verses?: InputMaybe<VerseUpdateManyWithoutLyricsNestedInput>;
};

export type LyricsUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
};

export type LyricsUpdateManyWithWhereWithoutCreatorInput = {
  data: LyricsUpdateManyMutationInput;
  where: LyricsScalarWhereInput;
};

export type LyricsUpdateManyWithWhereWithoutSongInput = {
  data: LyricsUpdateManyMutationInput;
  where: LyricsScalarWhereInput;
};

export type LyricsUpdateManyWithoutCreatorNestedInput = {
  connect?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LyricsCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<LyricsCreateWithoutCreatorInput>;
  createMany?: InputMaybe<LyricsCreateManyCreatorInputEnvelope>;
  delete?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<LyricsScalarWhereInput>>;
  disconnect?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  set?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  update?: InputMaybe<Array<LyricsUpdateWithWhereUniqueWithoutCreatorInput>>;
  updateMany?: InputMaybe<Array<LyricsUpdateManyWithWhereWithoutCreatorInput>>;
  upsert?: InputMaybe<Array<LyricsUpsertWithWhereUniqueWithoutCreatorInput>>;
};

export type LyricsUpdateManyWithoutSongNestedInput = {
  connect?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<LyricsCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<LyricsCreateWithoutSongInput>;
  createMany?: InputMaybe<LyricsCreateManySongInputEnvelope>;
  delete?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<LyricsScalarWhereInput>>;
  disconnect?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  set?: InputMaybe<Array<LyricsWhereUniqueInput>>;
  update?: InputMaybe<Array<LyricsUpdateWithWhereUniqueWithoutSongInput>>;
  updateMany?: InputMaybe<Array<LyricsUpdateManyWithWhereWithoutSongInput>>;
  upsert?: InputMaybe<Array<LyricsUpsertWithWhereUniqueWithoutSongInput>>;
};

export type LyricsUpdateOneRequiredWithoutVersesNestedInput = {
  connect?: InputMaybe<LyricsWhereUniqueInput>;
  connectOrCreate?: InputMaybe<LyricsCreateOrConnectWithoutVersesInput>;
  create?: InputMaybe<LyricsCreateWithoutVersesInput>;
  update?: InputMaybe<LyricsUpdateWithoutVersesInput>;
  upsert?: InputMaybe<LyricsUpsertWithoutVersesInput>;
};

export type LyricsUpdateWithWhereUniqueWithoutCreatorInput = {
  data: LyricsUpdateWithoutCreatorInput;
  where: LyricsWhereUniqueInput;
};

export type LyricsUpdateWithWhereUniqueWithoutSongInput = {
  data: LyricsUpdateWithoutSongInput;
  where: LyricsWhereUniqueInput;
};

export type LyricsUpdateWithoutCreatorInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneWithoutLyricsNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
  verses?: InputMaybe<VerseUpdateManyWithoutLyricsNestedInput>;
};

export type LyricsUpdateWithoutSongInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
  verses?: InputMaybe<VerseUpdateManyWithoutLyricsNestedInput>;
};

export type LyricsUpdateWithoutVersesInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneWithoutLyricsNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  verseIds?: InputMaybe<Array<Scalars['String']>>;
};

export type LyricsUpdateverseIdsInput = {
  push?: InputMaybe<Array<Scalars['String']>>;
  set?: InputMaybe<Array<Scalars['String']>>;
};

export type LyricsUpsertWithWhereUniqueWithoutCreatorInput = {
  create: LyricsCreateWithoutCreatorInput;
  update: LyricsUpdateWithoutCreatorInput;
  where: LyricsWhereUniqueInput;
};

export type LyricsUpsertWithWhereUniqueWithoutSongInput = {
  create: LyricsCreateWithoutSongInput;
  update: LyricsUpdateWithoutSongInput;
  where: LyricsWhereUniqueInput;
};

export type LyricsUpsertWithoutVersesInput = {
  create: LyricsCreateWithoutVersesInput;
  update: LyricsUpdateWithoutVersesInput;
};

export type LyricsWhereInput = {
  AND?: InputMaybe<Array<LyricsWhereInput>>;
  NOT?: InputMaybe<Array<LyricsWhereInput>>;
  OR?: InputMaybe<Array<LyricsWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creator?: InputMaybe<UserWhereInput>;
  creatorId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  verseIds?: InputMaybe<StringNullableListFilter>;
  verses?: InputMaybe<VerseListRelationFilter>;
};

export type LyricsWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type Mutation = {
  addTagToSong?: Maybe<Song>;
  changeCommentPublicity: SongComment;
  changeLyricsPublicity: Lyrics;
  changeRecordingPublicity: Recording;
  changeScorePublicity: Score;
  changeSongOrderTemplatePublicity: SongOrderTemplate;
  changeSongPublicity: Song;
  createOneLyrics: Lyrics;
  createOneRecording: Recording;
  createOneScore: Score;
  createOneSlideGroup: SlideGroup;
  createOneSong: Song;
  createOneSongComment: SongComment;
  createOneSongOrder: SongOrder;
  createOneSongOrderTemplate: SongOrderTemplate;
  createOneUser: User;
  deleteOneSlideGroup?: Maybe<SlideGroup>;
  deleteTagFromSong: Song;
  insertSlideGroup: SlideGroup;
  login: AuthPayload;
  signup: AuthPayload;
  swapSlideGroups: Array<SlideGroup>;
  updateOneSlideGroup?: Maybe<SlideGroup>;
  updateOneSongOrder?: Maybe<SongOrder>;
  updateOneUser?: Maybe<User>;
};


export type MutationAddTagToSongArgs = {
  songId: Scalars['String'];
  tagId?: InputMaybe<Scalars['String']>;
  text: Scalars['String'];
};


export type MutationChangeCommentPublicityArgs = {
  publishStatus?: InputMaybe<Publicity>;
  where?: InputMaybe<SongCommentWhereUniqueInput>;
};


export type MutationChangeLyricsPublicityArgs = {
  publishStatus?: InputMaybe<Publicity>;
  where?: InputMaybe<LyricsWhereUniqueInput>;
};


export type MutationChangeRecordingPublicityArgs = {
  publishStatus?: InputMaybe<Publicity>;
  where?: InputMaybe<RecordingWhereUniqueInput>;
};


export type MutationChangeScorePublicityArgs = {
  publishStatus?: InputMaybe<Publicity>;
  where?: InputMaybe<ScoreWhereUniqueInput>;
};


export type MutationChangeSongOrderTemplatePublicityArgs = {
  publishStatus?: InputMaybe<Publicity>;
  where?: InputMaybe<SongOrderTemplateWhereUniqueInput>;
};


export type MutationChangeSongPublicityArgs = {
  publishStatus?: InputMaybe<Publicity>;
  where?: InputMaybe<SongWhereUniqueInput>;
};


export type MutationCreateOneLyricsArgs = {
  data: LyricsCreateInput;
};


export type MutationCreateOneRecordingArgs = {
  data: RecordingCreateInputWithFile;
};


export type MutationCreateOneScoreArgs = {
  data: ScoreCreateInputWithFile;
};


export type MutationCreateOneSlideGroupArgs = {
  data: SlideGroupCreateInput;
};


export type MutationCreateOneSongArgs = {
  data: SongCreateInput;
};


export type MutationCreateOneSongCommentArgs = {
  data: SongCommentCreateInput;
};


export type MutationCreateOneSongOrderArgs = {
  data: SongOrderCreateInput;
};


export type MutationCreateOneSongOrderTemplateArgs = {
  data: SongOrderTemplateCreateInput;
};


export type MutationCreateOneUserArgs = {
  data: UserCreateInput;
};


export type MutationDeleteOneSlideGroupArgs = {
  where: SlideGroupWhereUniqueInput;
};


export type MutationDeleteTagFromSongArgs = {
  songId: Scalars['String'];
  tagId: Scalars['String'];
};


export type MutationInsertSlideGroupArgs = {
  data: SlideGroupCreateInput;
  whereSongOrder: SongOrderWhereUniqueInput;
};


export type MutationLoginArgs = {
  emailOrName?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
};


export type MutationSignupArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
  userName: Scalars['String'];
};


export type MutationSwapSlideGroupsArgs = {
  groupId1: Scalars['String'];
  groupId2: Scalars['String'];
};


export type MutationUpdateOneSlideGroupArgs = {
  data: SlideGroupUpdateInput;
  where: SlideGroupWhereUniqueInput;
};


export type MutationUpdateOneSongOrderArgs = {
  data: SongOrderUpdateInput;
  where: SongOrderWhereUniqueInput;
};


export type MutationUpdateOneUserArgs = {
  data: UserUpdateInputWithFile;
  where: UserWhereUniqueInput;
};

export type NestedBoolFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolFilter>;
};

export type NestedBoolNullableFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolNullableFilter>;
};

export type NestedBoolNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedBoolNullableFilter>;
  _min?: InputMaybe<NestedBoolNullableFilter>;
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolNullableWithAggregatesFilter>;
};

export type NestedBoolWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedBoolFilter>;
  _min?: InputMaybe<NestedBoolFilter>;
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<NestedBoolWithAggregatesFilter>;
};

export type NestedDateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<NestedDateTimeFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export type NestedDateTimeWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedDateTimeFilter>;
  _min?: InputMaybe<NestedDateTimeFilter>;
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<NestedDateTimeWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export type NestedEnumPublishStatusNullableFilter = {
  equals?: InputMaybe<PublishStatus>;
  in?: InputMaybe<Array<PublishStatus>>;
  not?: InputMaybe<PublishStatus>;
  notIn?: InputMaybe<Array<PublishStatus>>;
};

export type NestedEnumPublishStatusNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedEnumPublishStatusNullableFilter>;
  _min?: InputMaybe<NestedEnumPublishStatusNullableFilter>;
  equals?: InputMaybe<PublishStatus>;
  in?: InputMaybe<Array<PublishStatus>>;
  not?: InputMaybe<PublishStatus>;
  notIn?: InputMaybe<Array<PublishStatus>>;
};

export type NestedEnumRecordingTypeNullableFilter = {
  equals?: InputMaybe<RecordingType>;
  in?: InputMaybe<Array<RecordingType>>;
  not?: InputMaybe<RecordingType>;
  notIn?: InputMaybe<Array<RecordingType>>;
};

export type NestedEnumRecordingTypeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedEnumRecordingTypeNullableFilter>;
  _min?: InputMaybe<NestedEnumRecordingTypeNullableFilter>;
  equals?: InputMaybe<RecordingType>;
  in?: InputMaybe<Array<RecordingType>>;
  not?: InputMaybe<RecordingType>;
  notIn?: InputMaybe<Array<RecordingType>>;
};

export type NestedEnumVerseTypeNullableFilter = {
  equals?: InputMaybe<VerseType>;
  in?: InputMaybe<Array<VerseType>>;
  not?: InputMaybe<VerseType>;
  notIn?: InputMaybe<Array<VerseType>>;
};

export type NestedEnumVerseTypeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedEnumVerseTypeNullableFilter>;
  _min?: InputMaybe<NestedEnumVerseTypeNullableFilter>;
  equals?: InputMaybe<VerseType>;
  in?: InputMaybe<Array<VerseType>>;
  not?: InputMaybe<VerseType>;
  notIn?: InputMaybe<Array<VerseType>>;
};

export type NestedFloatFilter = {
  equals?: InputMaybe<Scalars['Float']>;
  gt?: InputMaybe<Scalars['Float']>;
  gte?: InputMaybe<Scalars['Float']>;
  in?: InputMaybe<Array<Scalars['Float']>>;
  lt?: InputMaybe<Scalars['Float']>;
  lte?: InputMaybe<Scalars['Float']>;
  not?: InputMaybe<NestedFloatFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']>>;
};

export type NestedFloatNullableFilter = {
  equals?: InputMaybe<Scalars['Float']>;
  gt?: InputMaybe<Scalars['Float']>;
  gte?: InputMaybe<Scalars['Float']>;
  in?: InputMaybe<Array<Scalars['Float']>>;
  lt?: InputMaybe<Scalars['Float']>;
  lte?: InputMaybe<Scalars['Float']>;
  not?: InputMaybe<NestedFloatNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']>>;
};

export type NestedIntFilter = {
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type NestedIntNullableFilter = {
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type NestedIntNullableWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatNullableFilter>;
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedIntNullableFilter>;
  _min?: InputMaybe<NestedIntNullableFilter>;
  _sum?: InputMaybe<NestedIntNullableFilter>;
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type NestedIntWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatFilter>;
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedIntFilter>;
  _min?: InputMaybe<NestedIntFilter>;
  _sum?: InputMaybe<NestedIntFilter>;
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  not?: InputMaybe<NestedIntWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type NestedJsonFilter = {
  array_contains?: InputMaybe<Scalars['Json']>;
  array_ends_with?: InputMaybe<Scalars['Json']>;
  array_starts_with?: InputMaybe<Scalars['Json']>;
  equals?: InputMaybe<Scalars['Json']>;
  gt?: InputMaybe<Scalars['Json']>;
  gte?: InputMaybe<Scalars['Json']>;
  lt?: InputMaybe<Scalars['Json']>;
  lte?: InputMaybe<Scalars['Json']>;
  not?: InputMaybe<Scalars['Json']>;
  path?: InputMaybe<Array<Scalars['String']>>;
  string_contains?: InputMaybe<Scalars['String']>;
  string_ends_with?: InputMaybe<Scalars['String']>;
  string_starts_with?: InputMaybe<Scalars['String']>;
};

export type NestedJsonNullableFilter = {
  array_contains?: InputMaybe<Scalars['Json']>;
  array_ends_with?: InputMaybe<Scalars['Json']>;
  array_starts_with?: InputMaybe<Scalars['Json']>;
  equals?: InputMaybe<Scalars['Json']>;
  gt?: InputMaybe<Scalars['Json']>;
  gte?: InputMaybe<Scalars['Json']>;
  lt?: InputMaybe<Scalars['Json']>;
  lte?: InputMaybe<Scalars['Json']>;
  not?: InputMaybe<Scalars['Json']>;
  path?: InputMaybe<Array<Scalars['String']>>;
  string_contains?: InputMaybe<Scalars['String']>;
  string_ends_with?: InputMaybe<Scalars['String']>;
  string_starts_with?: InputMaybe<Scalars['String']>;
};

export type NestedStringFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type NestedStringNullableFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<NestedStringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type NestedStringNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedStringNullableFilter>;
  _min?: InputMaybe<NestedStringNullableFilter>;
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<NestedStringNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type NestedStringWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedStringFilter>;
  _min?: InputMaybe<NestedStringFilter>;
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<NestedStringWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type NullableBoolFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['Boolean']>;
};

export type NullableEnumPublishStatusFieldUpdateOperationsInput = {
  set?: InputMaybe<PublishStatus>;
};

export type NullableEnumRecordingTypeFieldUpdateOperationsInput = {
  set?: InputMaybe<RecordingType>;
};

export type NullableEnumVerseTypeFieldUpdateOperationsInput = {
  set?: InputMaybe<VerseType>;
};

export type NullableIntFieldUpdateOperationsInput = {
  decrement?: InputMaybe<Scalars['Int']>;
  divide?: InputMaybe<Scalars['Int']>;
  increment?: InputMaybe<Scalars['Int']>;
  multiply?: InputMaybe<Scalars['Int']>;
  set?: InputMaybe<Scalars['Int']>;
};

export enum NullableJsonNullValueInput {
  DbNull = 'DbNull',
  JsonNull = 'JsonNull'
}

export type NullableStringFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['String']>;
};

export enum Publicity {
  Published = 'Published',
  Rejected = 'Rejected'
}

export enum PublishStatus {
  Published = 'Published',
  Rejected = 'Rejected',
  Unpublished = 'Unpublished'
}

export type Query = {
  commentsCount: Scalars['Int'];
  findUniqueSongOrder?: Maybe<SongOrder>;
  findUniqueSongOrderTemplate?: Maybe<SongOrderTemplate>;
  lyrics?: Maybe<Lyrics>;
  publishedSongs: Array<Song>;
  publishedSongsCount: Scalars['Int'];
  recordings: Array<Recording>;
  recordingsCount: Scalars['Int'];
  scores: Array<Score>;
  scoresCount: Scalars['Int'];
  searchComments: Array<SongComment>;
  searchLyrics: Array<Lyrics>;
  song?: Maybe<Song>;
  songOrder?: Maybe<SongOrder>;
  songOrderTemplate?: Maybe<SongOrderTemplate>;
  songlyricsCount: Scalars['Int'];
  user?: Maybe<User>;
  userSongOrders: Array<SongOrder>;
  userSongOrdersCount: Scalars['Int'];
  userTags: Array<Tag>;
  userTagsCount: Scalars['Int'];
  userTemplates: Array<SongOrderTemplate>;
  userTemplatesCount: Scalars['Int'];
};


export type QueryCommentsCountArgs = {
  cursor?: InputMaybe<SongCommentWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongCommentScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongCommentOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongCommentWhereInput>;
};


export type QueryFindUniqueSongOrderArgs = {
  where: SongOrderWhereUniqueInput;
};


export type QueryFindUniqueSongOrderTemplateArgs = {
  where: SongOrderTemplateWhereUniqueInput;
};


export type QueryLyricsArgs = {
  where: LyricsWhereUniqueInput;
};


export type QueryPublishedSongsArgs = {
  cursor?: InputMaybe<SongWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongWhereInput>;
};


export type QueryPublishedSongsCountArgs = {
  cursor?: InputMaybe<SongWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongWhereInput>;
};


export type QueryRecordingsArgs = {
  cursor?: InputMaybe<RecordingWhereUniqueInput>;
  distinct?: InputMaybe<Array<RecordingScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<RecordingOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ScoreWhereWithPrivateField>;
};


export type QueryRecordingsCountArgs = {
  cursor?: InputMaybe<RecordingWhereUniqueInput>;
  distinct?: InputMaybe<Array<RecordingScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<RecordingOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ScoreWhereWithPrivateField>;
};


export type QueryScoresArgs = {
  cursor?: InputMaybe<ScoreWhereUniqueInput>;
  distinct?: InputMaybe<Array<ScoreScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<ScoreOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ScoreWhereWithPrivateField>;
};


export type QueryScoresCountArgs = {
  cursor?: InputMaybe<ScoreWhereUniqueInput>;
  distinct?: InputMaybe<Array<ScoreScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<ScoreOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ScoreWhereWithPrivateField>;
};


export type QuerySearchCommentsArgs = {
  cursor?: InputMaybe<SongCommentWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongCommentScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongCommentOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongCommentWhereInput>;
};


export type QuerySearchLyricsArgs = {
  cursor?: InputMaybe<LyricsWhereUniqueInput>;
  distinct?: InputMaybe<Array<LyricsScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<LyricsOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LyricsWhereInput>;
};


export type QuerySongArgs = {
  where: SongWhereUniqueInput;
};


export type QuerySongOrderArgs = {
  where: SongOrderWhereUniqueInput;
};


export type QuerySongOrderTemplateArgs = {
  where: SongOrderTemplateWhereUniqueInput;
};


export type QuerySonglyricsCountArgs = {
  cursor?: InputMaybe<LyricsWhereUniqueInput>;
  distinct?: InputMaybe<Array<LyricsScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<LyricsOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LyricsWhereInput>;
};


export type QueryUserArgs = {
  where: UserWhereUniqueInput;
};


export type QueryUserSongOrdersArgs = {
  cursor?: InputMaybe<SongOrderWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongOrderScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongOrderWhereInput>;
};


export type QueryUserSongOrdersCountArgs = {
  cursor?: InputMaybe<SongOrderWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongOrderScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongOrderWhereInput>;
};


export type QueryUserTagsArgs = {
  cursor?: InputMaybe<TagWhereUniqueInput>;
  distinct?: InputMaybe<Array<TagScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<TagOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagWhereInput>;
};


export type QueryUserTagsCountArgs = {
  cursor?: InputMaybe<TagWhereUniqueInput>;
  distinct?: InputMaybe<Array<TagScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<TagOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagWhereInput>;
};


export type QueryUserTemplatesArgs = {
  cursor?: InputMaybe<SongOrderTemplateWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongOrderTemplateScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderTemplateOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongOrderTemplateWhereInput>;
};


export type QueryUserTemplatesCountArgs = {
  cursor?: InputMaybe<SongOrderTemplateWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongOrderTemplateScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderTemplateOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongOrderTemplateWhereInput>;
};

export enum QueryMode {
  Default = 'default',
  Insensitive = 'insensitive'
}

export type Recording = {
  createdAt: Scalars['DateTime'];
  description: Scalars['String'];
  id: Scalars['ID'];
  publishStatus?: Maybe<PublishStatus>;
  song: Song;
  type?: Maybe<RecordingType>;
  updatedAt: Scalars['DateTime'];
  url: Scalars['String'];
  user: User;
};

export type RecordingCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type RecordingCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutRecordingsInput;
  type?: InputMaybe<RecordingType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type RecordingCreateInputWithFile = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutRecordingsInput;
  type?: InputMaybe<RecordingType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url?: InputMaybe<Scalars['Upload']>;
};

export type RecordingCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
  type?: InputMaybe<RecordingType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
  userId: Scalars['String'];
};

export type RecordingCreateManySongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<RecordingType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
  userId: Scalars['String'];
};

export type RecordingCreateManySongInputEnvelope = {
  data: Array<RecordingCreateManySongInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type RecordingCreateManyUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
  type?: InputMaybe<RecordingType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type RecordingCreateManyUserInputEnvelope = {
  data: Array<RecordingCreateManyUserInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type RecordingCreateNestedManyWithoutSongInput = {
  connect?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<RecordingCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<RecordingCreateWithoutSongInput>;
  createMany?: InputMaybe<RecordingCreateManySongInputEnvelope>;
};

export type RecordingCreateNestedManyWithoutUserInput = {
  connect?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<RecordingCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<RecordingCreateWithoutUserInput>;
  createMany?: InputMaybe<RecordingCreateManyUserInputEnvelope>;
};

export type RecordingCreateOrConnectWithoutSongInput = {
  create: RecordingCreateWithoutSongInput;
  where: RecordingWhereUniqueInput;
};

export type RecordingCreateOrConnectWithoutUserInput = {
  create: RecordingCreateWithoutUserInput;
  where: RecordingWhereUniqueInput;
};

export type RecordingCreateWithoutSongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<RecordingType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type RecordingCreateWithoutUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutRecordingsInput;
  type?: InputMaybe<RecordingType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type RecordingListRelationFilter = {
  every?: InputMaybe<RecordingWhereInput>;
  none?: InputMaybe<RecordingWhereInput>;
  some?: InputMaybe<RecordingWhereInput>;
};

export type RecordingMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type RecordingMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type RecordingOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type RecordingOrderByWithAggregationInput = {
  _count?: InputMaybe<RecordingCountOrderByAggregateInput>;
  _max?: InputMaybe<RecordingMaxOrderByAggregateInput>;
  _min?: InputMaybe<RecordingMinOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type RecordingOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  song?: InputMaybe<SongOrderByWithRelationInput>;
  songId?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  user?: InputMaybe<UserOrderByWithRelationInput>;
  userId?: InputMaybe<SortOrder>;
};

export enum RecordingScalarFieldEnum {
  CreatedAt = 'createdAt',
  Description = 'description',
  Id = 'id',
  PublishStatus = 'publishStatus',
  SongId = 'songId',
  Type = 'type',
  UpdatedAt = 'updatedAt',
  Url = 'url',
  UserId = 'userId'
}

export type RecordingScalarWhereInput = {
  AND?: InputMaybe<Array<RecordingScalarWhereInput>>;
  NOT?: InputMaybe<Array<RecordingScalarWhereInput>>;
  OR?: InputMaybe<Array<RecordingScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  description?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  songId?: InputMaybe<StringFilter>;
  type?: InputMaybe<EnumRecordingTypeNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  url?: InputMaybe<StringFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type RecordingScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<RecordingScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<RecordingScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<RecordingScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  description?: InputMaybe<StringNullableWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableWithAggregatesFilter>;
  songId?: InputMaybe<StringWithAggregatesFilter>;
  type?: InputMaybe<EnumRecordingTypeNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  url?: InputMaybe<StringWithAggregatesFilter>;
  userId?: InputMaybe<StringWithAggregatesFilter>;
};

export enum RecordingType {
  AppleMusic = 'AppleMusic',
  Other = 'Other',
  Raw = 'Raw',
  SoundCloud = 'SoundCloud',
  Spotify = 'Spotify',
  YouTube = 'YouTube'
}

export type RecordingUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutRecordingsNestedInput>;
  type?: InputMaybe<NullableEnumRecordingTypeFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type RecordingUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  type?: InputMaybe<NullableEnumRecordingTypeFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type RecordingUpdateManyWithWhereWithoutSongInput = {
  data: RecordingUpdateManyMutationInput;
  where: RecordingScalarWhereInput;
};

export type RecordingUpdateManyWithWhereWithoutUserInput = {
  data: RecordingUpdateManyMutationInput;
  where: RecordingScalarWhereInput;
};

export type RecordingUpdateManyWithoutSongNestedInput = {
  connect?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<RecordingCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<RecordingCreateWithoutSongInput>;
  createMany?: InputMaybe<RecordingCreateManySongInputEnvelope>;
  delete?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<RecordingScalarWhereInput>>;
  disconnect?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  set?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  update?: InputMaybe<Array<RecordingUpdateWithWhereUniqueWithoutSongInput>>;
  updateMany?: InputMaybe<Array<RecordingUpdateManyWithWhereWithoutSongInput>>;
  upsert?: InputMaybe<Array<RecordingUpsertWithWhereUniqueWithoutSongInput>>;
};

export type RecordingUpdateManyWithoutUserNestedInput = {
  connect?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<RecordingCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<RecordingCreateWithoutUserInput>;
  createMany?: InputMaybe<RecordingCreateManyUserInputEnvelope>;
  delete?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<RecordingScalarWhereInput>>;
  disconnect?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  set?: InputMaybe<Array<RecordingWhereUniqueInput>>;
  update?: InputMaybe<Array<RecordingUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: InputMaybe<Array<RecordingUpdateManyWithWhereWithoutUserInput>>;
  upsert?: InputMaybe<Array<RecordingUpsertWithWhereUniqueWithoutUserInput>>;
};

export type RecordingUpdateWithWhereUniqueWithoutSongInput = {
  data: RecordingUpdateWithoutSongInput;
  where: RecordingWhereUniqueInput;
};

export type RecordingUpdateWithWhereUniqueWithoutUserInput = {
  data: RecordingUpdateWithoutUserInput;
  where: RecordingWhereUniqueInput;
};

export type RecordingUpdateWithoutSongInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  type?: InputMaybe<NullableEnumRecordingTypeFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type RecordingUpdateWithoutUserInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutRecordingsNestedInput>;
  type?: InputMaybe<NullableEnumRecordingTypeFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type RecordingUpsertWithWhereUniqueWithoutSongInput = {
  create: RecordingCreateWithoutSongInput;
  update: RecordingUpdateWithoutSongInput;
  where: RecordingWhereUniqueInput;
};

export type RecordingUpsertWithWhereUniqueWithoutUserInput = {
  create: RecordingCreateWithoutUserInput;
  update: RecordingUpdateWithoutUserInput;
  where: RecordingWhereUniqueInput;
};

export type RecordingWhereInput = {
  AND?: InputMaybe<Array<RecordingWhereInput>>;
  NOT?: InputMaybe<Array<RecordingWhereInput>>;
  OR?: InputMaybe<Array<RecordingWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  description?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringFilter>;
  type?: InputMaybe<EnumRecordingTypeNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  url?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserWhereInput>;
  userId?: InputMaybe<StringFilter>;
};

export type RecordingWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type RecordingWhereWithPrivateField = {
  AND?: InputMaybe<Array<RecordingWhereInput>>;
  NOT?: InputMaybe<Array<RecordingWhereInput>>;
  OR?: InputMaybe<Array<RecordingWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  description?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<StringFilter>;
  private: Scalars['Boolean'];
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringFilter>;
  type?: InputMaybe<EnumRecordingTypeNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  url?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserWhereInput>;
  userId?: InputMaybe<StringFilter>;
};

export type Score = {
  createdAt: Scalars['DateTime'];
  description: Scalars['String'];
  id: Scalars['ID'];
  publishStatus?: Maybe<PublishStatus>;
  song: Song;
  updatedAt: Scalars['DateTime'];
  url: Scalars['String'];
  user: User;
};

export type ScoreCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type ScoreCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutScoresInput;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type ScoreCreateInputWithFile = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutScoresInput;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url?: InputMaybe<Scalars['Upload']>;
};

export type ScoreCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
  userId: Scalars['String'];
};

export type ScoreCreateManySongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
  userId: Scalars['String'];
};

export type ScoreCreateManySongInputEnvelope = {
  data: Array<ScoreCreateManySongInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type ScoreCreateManyUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type ScoreCreateManyUserInputEnvelope = {
  data: Array<ScoreCreateManyUserInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type ScoreCreateNestedManyWithoutSongInput = {
  connect?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<ScoreCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<ScoreCreateWithoutSongInput>;
  createMany?: InputMaybe<ScoreCreateManySongInputEnvelope>;
};

export type ScoreCreateNestedManyWithoutUserInput = {
  connect?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<ScoreCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<ScoreCreateWithoutUserInput>;
  createMany?: InputMaybe<ScoreCreateManyUserInputEnvelope>;
};

export type ScoreCreateOrConnectWithoutSongInput = {
  create: ScoreCreateWithoutSongInput;
  where: ScoreWhereUniqueInput;
};

export type ScoreCreateOrConnectWithoutUserInput = {
  create: ScoreCreateWithoutUserInput;
  where: ScoreWhereUniqueInput;
};

export type ScoreCreateWithoutSongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type ScoreCreateWithoutUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutScoresInput;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type ScoreListRelationFilter = {
  every?: InputMaybe<ScoreWhereInput>;
  none?: InputMaybe<ScoreWhereInput>;
  some?: InputMaybe<ScoreWhereInput>;
};

export type ScoreMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type ScoreMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type ScoreOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type ScoreOrderByWithAggregationInput = {
  _count?: InputMaybe<ScoreCountOrderByAggregateInput>;
  _max?: InputMaybe<ScoreMaxOrderByAggregateInput>;
  _min?: InputMaybe<ScoreMinOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type ScoreOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  description?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  song?: InputMaybe<SongOrderByWithRelationInput>;
  songId?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  url?: InputMaybe<SortOrder>;
  user?: InputMaybe<UserOrderByWithRelationInput>;
  userId?: InputMaybe<SortOrder>;
};

export enum ScoreScalarFieldEnum {
  CreatedAt = 'createdAt',
  Description = 'description',
  Id = 'id',
  PublishStatus = 'publishStatus',
  SongId = 'songId',
  UpdatedAt = 'updatedAt',
  Url = 'url',
  UserId = 'userId'
}

export type ScoreScalarWhereInput = {
  AND?: InputMaybe<Array<ScoreScalarWhereInput>>;
  NOT?: InputMaybe<Array<ScoreScalarWhereInput>>;
  OR?: InputMaybe<Array<ScoreScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  description?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  songId?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  url?: InputMaybe<StringFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type ScoreScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<ScoreScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<ScoreScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<ScoreScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  description?: InputMaybe<StringNullableWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableWithAggregatesFilter>;
  songId?: InputMaybe<StringWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  url?: InputMaybe<StringWithAggregatesFilter>;
  userId?: InputMaybe<StringWithAggregatesFilter>;
};

export type ScoreUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutScoresNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type ScoreUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type ScoreUpdateManyWithWhereWithoutSongInput = {
  data: ScoreUpdateManyMutationInput;
  where: ScoreScalarWhereInput;
};

export type ScoreUpdateManyWithWhereWithoutUserInput = {
  data: ScoreUpdateManyMutationInput;
  where: ScoreScalarWhereInput;
};

export type ScoreUpdateManyWithoutSongNestedInput = {
  connect?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<ScoreCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<ScoreCreateWithoutSongInput>;
  createMany?: InputMaybe<ScoreCreateManySongInputEnvelope>;
  delete?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<ScoreScalarWhereInput>>;
  disconnect?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  set?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  update?: InputMaybe<Array<ScoreUpdateWithWhereUniqueWithoutSongInput>>;
  updateMany?: InputMaybe<Array<ScoreUpdateManyWithWhereWithoutSongInput>>;
  upsert?: InputMaybe<Array<ScoreUpsertWithWhereUniqueWithoutSongInput>>;
};

export type ScoreUpdateManyWithoutUserNestedInput = {
  connect?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<ScoreCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<ScoreCreateWithoutUserInput>;
  createMany?: InputMaybe<ScoreCreateManyUserInputEnvelope>;
  delete?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<ScoreScalarWhereInput>>;
  disconnect?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  set?: InputMaybe<Array<ScoreWhereUniqueInput>>;
  update?: InputMaybe<Array<ScoreUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: InputMaybe<Array<ScoreUpdateManyWithWhereWithoutUserInput>>;
  upsert?: InputMaybe<Array<ScoreUpsertWithWhereUniqueWithoutUserInput>>;
};

export type ScoreUpdateWithWhereUniqueWithoutSongInput = {
  data: ScoreUpdateWithoutSongInput;
  where: ScoreWhereUniqueInput;
};

export type ScoreUpdateWithWhereUniqueWithoutUserInput = {
  data: ScoreUpdateWithoutUserInput;
  where: ScoreWhereUniqueInput;
};

export type ScoreUpdateWithoutSongInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type ScoreUpdateWithoutUserInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  description?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutScoresNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  url?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type ScoreUpsertWithWhereUniqueWithoutSongInput = {
  create: ScoreCreateWithoutSongInput;
  update: ScoreUpdateWithoutSongInput;
  where: ScoreWhereUniqueInput;
};

export type ScoreUpsertWithWhereUniqueWithoutUserInput = {
  create: ScoreCreateWithoutUserInput;
  update: ScoreUpdateWithoutUserInput;
  where: ScoreWhereUniqueInput;
};

export type ScoreWhereInput = {
  AND?: InputMaybe<Array<ScoreWhereInput>>;
  NOT?: InputMaybe<Array<ScoreWhereInput>>;
  OR?: InputMaybe<Array<ScoreWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  description?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  url?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserWhereInput>;
  userId?: InputMaybe<StringFilter>;
};

export type ScoreWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type ScoreWhereWithPrivateField = {
  AND?: InputMaybe<Array<ScoreWhereInput>>;
  NOT?: InputMaybe<Array<ScoreWhereInput>>;
  OR?: InputMaybe<Array<ScoreWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  description?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<StringFilter>;
  private: Scalars['Boolean'];
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  url?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserWhereInput>;
  userId?: InputMaybe<StringFilter>;
};

export type Slide = {
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  group: SlideGroup;
  id: Scalars['ID'];
  order: Scalars['Int'];
  title?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
};

export type SlideAvgOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export type SlideCountOrderByAggregateInput = {
  config?: InputMaybe<SortOrder>;
  content?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  slideGroupId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideCreateInput = {
  config?: InputMaybe<Scalars['Json']>;
  content: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  group: SlideGroupCreateNestedOneWithoutSlidesInput;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideCreateManyGroupInput = {
  config?: InputMaybe<Scalars['Json']>;
  content: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideCreateManyGroupInputEnvelope = {
  data: Array<SlideCreateManyGroupInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SlideCreateManyInput = {
  config?: InputMaybe<Scalars['Json']>;
  content: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  slideGroupId: Scalars['String'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideCreateNestedManyWithoutGroupInput = {
  connect?: InputMaybe<Array<SlideWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SlideCreateOrConnectWithoutGroupInput>>;
  create?: InputMaybe<SlideCreateWithoutGroupInput>;
  createMany?: InputMaybe<SlideCreateManyGroupInputEnvelope>;
};

export type SlideCreateOrConnectWithoutGroupInput = {
  create: SlideCreateWithoutGroupInput;
  where: SlideWhereUniqueInput;
};

export type SlideCreateWithoutGroupInput = {
  config?: InputMaybe<Scalars['Json']>;
  content: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroup = {
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isSongOrderItem?: Maybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  slides: Array<Slide>;
  song?: Maybe<Song>;
  songOrder: SongOrder;
  title?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
};


export type SlideGroupSlidesArgs = {
  cursor?: InputMaybe<SlideWhereUniqueInput>;
  distinct?: InputMaybe<Array<SlideScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SlideOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SlideWhereInput>;
};

export type SlideGroupAvgOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export type SlideGroupCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isSongOrderItem?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  songOrderId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideGroupCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  isSongOrderItem?: InputMaybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  slides?: InputMaybe<SlideCreateNestedManyWithoutGroupInput>;
  song?: InputMaybe<SongCreateNestedOneWithoutUsagesInput>;
  songOrder: SongOrderCreateNestedOneWithoutGroupsInput;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroupCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  isSongOrderItem?: InputMaybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  songId?: InputMaybe<Scalars['String']>;
  songOrderId: Scalars['String'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroupCreateManySongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  isSongOrderItem?: InputMaybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  songOrderId: Scalars['String'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroupCreateManySongInputEnvelope = {
  data: Array<SlideGroupCreateManySongInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SlideGroupCreateManySongOrderInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  isSongOrderItem?: InputMaybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  songId?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroupCreateManySongOrderInputEnvelope = {
  data: Array<SlideGroupCreateManySongOrderInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SlideGroupCreateNestedManyWithoutSongInput = {
  connect?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SlideGroupCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<SlideGroupCreateWithoutSongInput>;
  createMany?: InputMaybe<SlideGroupCreateManySongInputEnvelope>;
};

export type SlideGroupCreateNestedManyWithoutSongOrderInput = {
  connect?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SlideGroupCreateOrConnectWithoutSongOrderInput>>;
  create?: InputMaybe<SlideGroupCreateWithoutSongOrderInput>;
  createMany?: InputMaybe<SlideGroupCreateManySongOrderInputEnvelope>;
};

export type SlideGroupCreateNestedOneWithoutSlidesInput = {
  connect?: InputMaybe<SlideGroupWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SlideGroupCreateOrConnectWithoutSlidesInput>;
  create?: InputMaybe<SlideGroupCreateWithoutSlidesInput>;
};

export type SlideGroupCreateOrConnectWithoutSlidesInput = {
  create: SlideGroupCreateWithoutSlidesInput;
  where: SlideGroupWhereUniqueInput;
};

export type SlideGroupCreateOrConnectWithoutSongInput = {
  create: SlideGroupCreateWithoutSongInput;
  where: SlideGroupWhereUniqueInput;
};

export type SlideGroupCreateOrConnectWithoutSongOrderInput = {
  create: SlideGroupCreateWithoutSongOrderInput;
  where: SlideGroupWhereUniqueInput;
};

export type SlideGroupCreateWithoutSlidesInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  isSongOrderItem?: InputMaybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  song?: InputMaybe<SongCreateNestedOneWithoutUsagesInput>;
  songOrder: SongOrderCreateNestedOneWithoutGroupsInput;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroupCreateWithoutSongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  isSongOrderItem?: InputMaybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  slides?: InputMaybe<SlideCreateNestedManyWithoutGroupInput>;
  songOrder: SongOrderCreateNestedOneWithoutGroupsInput;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroupCreateWithoutSongOrderInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  isSongOrderItem?: InputMaybe<Scalars['Boolean']>;
  order: Scalars['Int'];
  slides?: InputMaybe<SlideCreateNestedManyWithoutGroupInput>;
  song?: InputMaybe<SongCreateNestedOneWithoutUsagesInput>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SlideGroupListRelationFilter = {
  every?: InputMaybe<SlideGroupWhereInput>;
  none?: InputMaybe<SlideGroupWhereInput>;
  some?: InputMaybe<SlideGroupWhereInput>;
};

export type SlideGroupMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isSongOrderItem?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  songOrderId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideGroupMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isSongOrderItem?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  songOrderId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideGroupOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type SlideGroupOrderByWithAggregationInput = {
  _avg?: InputMaybe<SlideGroupAvgOrderByAggregateInput>;
  _count?: InputMaybe<SlideGroupCountOrderByAggregateInput>;
  _max?: InputMaybe<SlideGroupMaxOrderByAggregateInput>;
  _min?: InputMaybe<SlideGroupMinOrderByAggregateInput>;
  _sum?: InputMaybe<SlideGroupSumOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isSongOrderItem?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  songOrderId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideGroupOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isSongOrderItem?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  slides?: InputMaybe<SlideOrderByRelationAggregateInput>;
  song?: InputMaybe<SongOrderByWithRelationInput>;
  songId?: InputMaybe<SortOrder>;
  songOrder?: InputMaybe<SongOrderOrderByWithRelationInput>;
  songOrderId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideGroupRelationFilter = {
  is?: InputMaybe<SlideGroupWhereInput>;
  isNot?: InputMaybe<SlideGroupWhereInput>;
};

export enum SlideGroupScalarFieldEnum {
  CreatedAt = 'createdAt',
  Id = 'id',
  IsSongOrderItem = 'isSongOrderItem',
  Order = 'order',
  SongId = 'songId',
  SongOrderId = 'songOrderId',
  Title = 'title',
  UpdatedAt = 'updatedAt'
}

export type SlideGroupScalarWhereInput = {
  AND?: InputMaybe<Array<SlideGroupScalarWhereInput>>;
  NOT?: InputMaybe<Array<SlideGroupScalarWhereInput>>;
  OR?: InputMaybe<Array<SlideGroupScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  isSongOrderItem?: InputMaybe<BoolNullableFilter>;
  order?: InputMaybe<IntFilter>;
  songId?: InputMaybe<StringNullableFilter>;
  songOrderId?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SlideGroupScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<SlideGroupScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<SlideGroupScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<SlideGroupScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  isSongOrderItem?: InputMaybe<BoolNullableWithAggregatesFilter>;
  order?: InputMaybe<IntWithAggregatesFilter>;
  songId?: InputMaybe<StringNullableWithAggregatesFilter>;
  songOrderId?: InputMaybe<StringWithAggregatesFilter>;
  title?: InputMaybe<StringNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
};

export type SlideGroupSumOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export type SlideGroupUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isSongOrderItem?: InputMaybe<NullableBoolFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  slides?: InputMaybe<SlideUpdateManyWithoutGroupNestedInput>;
  song?: InputMaybe<SongUpdateOneWithoutUsagesNestedInput>;
  songOrder?: InputMaybe<SongOrderUpdateOneRequiredWithoutGroupsNestedInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideGroupUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isSongOrderItem?: InputMaybe<NullableBoolFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideGroupUpdateManyWithWhereWithoutSongInput = {
  data: SlideGroupUpdateManyMutationInput;
  where: SlideGroupScalarWhereInput;
};

export type SlideGroupUpdateManyWithWhereWithoutSongOrderInput = {
  data: SlideGroupUpdateManyMutationInput;
  where: SlideGroupScalarWhereInput;
};

export type SlideGroupUpdateManyWithoutSongNestedInput = {
  connect?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SlideGroupCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<SlideGroupCreateWithoutSongInput>;
  createMany?: InputMaybe<SlideGroupCreateManySongInputEnvelope>;
  delete?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SlideGroupScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  set?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  update?: InputMaybe<Array<SlideGroupUpdateWithWhereUniqueWithoutSongInput>>;
  updateMany?: InputMaybe<Array<SlideGroupUpdateManyWithWhereWithoutSongInput>>;
  upsert?: InputMaybe<Array<SlideGroupUpsertWithWhereUniqueWithoutSongInput>>;
};

export type SlideGroupUpdateManyWithoutSongOrderNestedInput = {
  connect?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SlideGroupCreateOrConnectWithoutSongOrderInput>>;
  create?: InputMaybe<SlideGroupCreateWithoutSongOrderInput>;
  createMany?: InputMaybe<SlideGroupCreateManySongOrderInputEnvelope>;
  delete?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SlideGroupScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  set?: InputMaybe<Array<SlideGroupWhereUniqueInput>>;
  update?: InputMaybe<Array<SlideGroupUpdateWithWhereUniqueWithoutSongOrderInput>>;
  updateMany?: InputMaybe<Array<SlideGroupUpdateManyWithWhereWithoutSongOrderInput>>;
  upsert?: InputMaybe<Array<SlideGroupUpsertWithWhereUniqueWithoutSongOrderInput>>;
};

export type SlideGroupUpdateOneRequiredWithoutSlidesNestedInput = {
  connect?: InputMaybe<SlideGroupWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SlideGroupCreateOrConnectWithoutSlidesInput>;
  create?: InputMaybe<SlideGroupCreateWithoutSlidesInput>;
  update?: InputMaybe<SlideGroupUpdateWithoutSlidesInput>;
  upsert?: InputMaybe<SlideGroupUpsertWithoutSlidesInput>;
};

export type SlideGroupUpdateWithWhereUniqueWithoutSongInput = {
  data: SlideGroupUpdateWithoutSongInput;
  where: SlideGroupWhereUniqueInput;
};

export type SlideGroupUpdateWithWhereUniqueWithoutSongOrderInput = {
  data: SlideGroupUpdateWithoutSongOrderInput;
  where: SlideGroupWhereUniqueInput;
};

export type SlideGroupUpdateWithoutSlidesInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isSongOrderItem?: InputMaybe<NullableBoolFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneWithoutUsagesNestedInput>;
  songOrder?: InputMaybe<SongOrderUpdateOneRequiredWithoutGroupsNestedInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideGroupUpdateWithoutSongInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isSongOrderItem?: InputMaybe<NullableBoolFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  slides?: InputMaybe<SlideUpdateManyWithoutGroupNestedInput>;
  songOrder?: InputMaybe<SongOrderUpdateOneRequiredWithoutGroupsNestedInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideGroupUpdateWithoutSongOrderInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isSongOrderItem?: InputMaybe<NullableBoolFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  slides?: InputMaybe<SlideUpdateManyWithoutGroupNestedInput>;
  song?: InputMaybe<SongUpdateOneWithoutUsagesNestedInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideGroupUpsertWithWhereUniqueWithoutSongInput = {
  create: SlideGroupCreateWithoutSongInput;
  update: SlideGroupUpdateWithoutSongInput;
  where: SlideGroupWhereUniqueInput;
};

export type SlideGroupUpsertWithWhereUniqueWithoutSongOrderInput = {
  create: SlideGroupCreateWithoutSongOrderInput;
  update: SlideGroupUpdateWithoutSongOrderInput;
  where: SlideGroupWhereUniqueInput;
};

export type SlideGroupUpsertWithoutSlidesInput = {
  create: SlideGroupCreateWithoutSlidesInput;
  update: SlideGroupUpdateWithoutSlidesInput;
};

export type SlideGroupWhereInput = {
  AND?: InputMaybe<Array<SlideGroupWhereInput>>;
  NOT?: InputMaybe<Array<SlideGroupWhereInput>>;
  OR?: InputMaybe<Array<SlideGroupWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  isSongOrderItem?: InputMaybe<BoolNullableFilter>;
  order?: InputMaybe<IntFilter>;
  slides?: InputMaybe<SlideListRelationFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringNullableFilter>;
  songOrder?: InputMaybe<SongOrderWhereInput>;
  songOrderId?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SlideGroupWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type SlideListRelationFilter = {
  every?: InputMaybe<SlideWhereInput>;
  none?: InputMaybe<SlideWhereInput>;
  some?: InputMaybe<SlideWhereInput>;
};

export type SlideMaxOrderByAggregateInput = {
  content?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  slideGroupId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideMinOrderByAggregateInput = {
  content?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  slideGroupId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type SlideOrderByWithAggregationInput = {
  _avg?: InputMaybe<SlideAvgOrderByAggregateInput>;
  _count?: InputMaybe<SlideCountOrderByAggregateInput>;
  _max?: InputMaybe<SlideMaxOrderByAggregateInput>;
  _min?: InputMaybe<SlideMinOrderByAggregateInput>;
  _sum?: InputMaybe<SlideSumOrderByAggregateInput>;
  config?: InputMaybe<SortOrder>;
  content?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  slideGroupId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SlideOrderByWithRelationInput = {
  config?: InputMaybe<SortOrder>;
  content?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  group?: InputMaybe<SlideGroupOrderByWithRelationInput>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  slideGroupId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export enum SlideScalarFieldEnum {
  Config = 'config',
  Content = 'content',
  CreatedAt = 'createdAt',
  Id = 'id',
  Order = 'order',
  SlideGroupId = 'slideGroupId',
  Title = 'title',
  UpdatedAt = 'updatedAt'
}

export type SlideScalarWhereInput = {
  AND?: InputMaybe<Array<SlideScalarWhereInput>>;
  NOT?: InputMaybe<Array<SlideScalarWhereInput>>;
  OR?: InputMaybe<Array<SlideScalarWhereInput>>;
  config?: InputMaybe<JsonNullableFilter>;
  content?: InputMaybe<StringFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  order?: InputMaybe<IntFilter>;
  slideGroupId?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SlideScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<SlideScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<SlideScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<SlideScalarWhereWithAggregatesInput>>;
  config?: InputMaybe<JsonNullableWithAggregatesFilter>;
  content?: InputMaybe<StringWithAggregatesFilter>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  order?: InputMaybe<IntWithAggregatesFilter>;
  slideGroupId?: InputMaybe<StringWithAggregatesFilter>;
  title?: InputMaybe<StringNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
};

export type SlideSumOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export type SlideUpdateInput = {
  config?: InputMaybe<Scalars['Json']>;
  content?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  group?: InputMaybe<SlideGroupUpdateOneRequiredWithoutSlidesNestedInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideUpdateManyMutationInput = {
  config?: InputMaybe<Scalars['Json']>;
  content?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideUpdateManyWithWhereWithoutGroupInput = {
  data: SlideUpdateManyMutationInput;
  where: SlideScalarWhereInput;
};

export type SlideUpdateManyWithoutGroupNestedInput = {
  connect?: InputMaybe<Array<SlideWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SlideCreateOrConnectWithoutGroupInput>>;
  create?: InputMaybe<SlideCreateWithoutGroupInput>;
  createMany?: InputMaybe<SlideCreateManyGroupInputEnvelope>;
  delete?: InputMaybe<Array<SlideWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SlideScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SlideWhereUniqueInput>>;
  set?: InputMaybe<Array<SlideWhereUniqueInput>>;
  update?: InputMaybe<Array<SlideUpdateWithWhereUniqueWithoutGroupInput>>;
  updateMany?: InputMaybe<Array<SlideUpdateManyWithWhereWithoutGroupInput>>;
  upsert?: InputMaybe<Array<SlideUpsertWithWhereUniqueWithoutGroupInput>>;
};

export type SlideUpdateWithWhereUniqueWithoutGroupInput = {
  data: SlideUpdateWithoutGroupInput;
  where: SlideWhereUniqueInput;
};

export type SlideUpdateWithoutGroupInput = {
  config?: InputMaybe<Scalars['Json']>;
  content?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SlideUpsertWithWhereUniqueWithoutGroupInput = {
  create: SlideCreateWithoutGroupInput;
  update: SlideUpdateWithoutGroupInput;
  where: SlideWhereUniqueInput;
};

export type SlideWhereInput = {
  AND?: InputMaybe<Array<SlideWhereInput>>;
  NOT?: InputMaybe<Array<SlideWhereInput>>;
  OR?: InputMaybe<Array<SlideWhereInput>>;
  config?: InputMaybe<JsonNullableFilter>;
  content?: InputMaybe<StringFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  group?: InputMaybe<SlideGroupWhereInput>;
  id?: InputMaybe<StringFilter>;
  order?: InputMaybe<IntFilter>;
  slideGroupId?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SlideWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type Song = {
  comments: Array<SongComment>;
  createdAt: Scalars['DateTime'];
  creator: User;
  id: Scalars['ID'];
  lyrics: Array<Lyrics>;
  name: Scalars['String'];
  publishStatus?: Maybe<PublishStatus>;
  recordings: Array<Recording>;
  scores: Array<Score>;
  translator?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  userTags: Array<Tag>;
  writer?: Maybe<Scalars['String']>;
};


export type SongCommentsArgs = {
  cursor?: InputMaybe<SongCommentWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongCommentScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongCommentOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongCommentWhereInput>;
};


export type SongLyricsArgs = {
  cursor?: InputMaybe<LyricsWhereUniqueInput>;
  distinct?: InputMaybe<Array<LyricsScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<LyricsOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LyricsWhereInput>;
};


export type SongRecordingsArgs = {
  cursor?: InputMaybe<RecordingWhereUniqueInput>;
  distinct?: InputMaybe<Array<RecordingScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<RecordingOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RecordingWhereInput>;
};


export type SongScoresArgs = {
  cursor?: InputMaybe<ScoreWhereUniqueInput>;
  distinct?: InputMaybe<Array<ScoreScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<ScoreOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ScoreWhereInput>;
};

export type SongComment = {
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  publishStatus?: Maybe<PublishStatus>;
  song: Song;
  text: Scalars['String'];
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type SongCommentCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type SongCommentCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutCommentsInput;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongCommentCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  userId: Scalars['String'];
};

export type SongCommentCreateManySongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  userId: Scalars['String'];
};

export type SongCommentCreateManySongInputEnvelope = {
  data: Array<SongCommentCreateManySongInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SongCommentCreateManyUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songId: Scalars['String'];
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongCommentCreateManyUserInputEnvelope = {
  data: Array<SongCommentCreateManyUserInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SongCommentCreateNestedManyWithoutSongInput = {
  connect?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCommentCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<SongCommentCreateWithoutSongInput>;
  createMany?: InputMaybe<SongCommentCreateManySongInputEnvelope>;
};

export type SongCommentCreateNestedManyWithoutUserInput = {
  connect?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCommentCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<SongCommentCreateWithoutUserInput>;
  createMany?: InputMaybe<SongCommentCreateManyUserInputEnvelope>;
};

export type SongCommentCreateOrConnectWithoutSongInput = {
  create: SongCommentCreateWithoutSongInput;
  where: SongCommentWhereUniqueInput;
};

export type SongCommentCreateOrConnectWithoutUserInput = {
  create: SongCommentCreateWithoutUserInput;
  where: SongCommentWhereUniqueInput;
};

export type SongCommentCreateWithoutSongInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongCommentCreateWithoutUserInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  song: SongCreateNestedOneWithoutCommentsInput;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongCommentListRelationFilter = {
  every?: InputMaybe<SongCommentWhereInput>;
  none?: InputMaybe<SongCommentWhereInput>;
  some?: InputMaybe<SongCommentWhereInput>;
};

export type SongCommentMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type SongCommentMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type SongCommentOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type SongCommentOrderByWithAggregationInput = {
  _count?: InputMaybe<SongCommentCountOrderByAggregateInput>;
  _max?: InputMaybe<SongCommentMaxOrderByAggregateInput>;
  _min?: InputMaybe<SongCommentMinOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  songId?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type SongCommentOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  song?: InputMaybe<SongOrderByWithRelationInput>;
  songId?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  user?: InputMaybe<UserOrderByWithRelationInput>;
  userId?: InputMaybe<SortOrder>;
};

export enum SongCommentScalarFieldEnum {
  CreatedAt = 'createdAt',
  Id = 'id',
  PublishStatus = 'publishStatus',
  SongId = 'songId',
  Text = 'text',
  UpdatedAt = 'updatedAt',
  UserId = 'userId'
}

export type SongCommentScalarWhereInput = {
  AND?: InputMaybe<Array<SongCommentScalarWhereInput>>;
  NOT?: InputMaybe<Array<SongCommentScalarWhereInput>>;
  OR?: InputMaybe<Array<SongCommentScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  songId?: InputMaybe<StringFilter>;
  text?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type SongCommentScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<SongCommentScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<SongCommentScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<SongCommentScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableWithAggregatesFilter>;
  songId?: InputMaybe<StringWithAggregatesFilter>;
  text?: InputMaybe<StringWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  userId?: InputMaybe<StringWithAggregatesFilter>;
};

export type SongCommentUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutCommentsNestedInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongCommentUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongCommentUpdateManyWithWhereWithoutSongInput = {
  data: SongCommentUpdateManyMutationInput;
  where: SongCommentScalarWhereInput;
};

export type SongCommentUpdateManyWithWhereWithoutUserInput = {
  data: SongCommentUpdateManyMutationInput;
  where: SongCommentScalarWhereInput;
};

export type SongCommentUpdateManyWithoutSongNestedInput = {
  connect?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCommentCreateOrConnectWithoutSongInput>>;
  create?: InputMaybe<SongCommentCreateWithoutSongInput>;
  createMany?: InputMaybe<SongCommentCreateManySongInputEnvelope>;
  delete?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongCommentScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  set?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  update?: InputMaybe<Array<SongCommentUpdateWithWhereUniqueWithoutSongInput>>;
  updateMany?: InputMaybe<Array<SongCommentUpdateManyWithWhereWithoutSongInput>>;
  upsert?: InputMaybe<Array<SongCommentUpsertWithWhereUniqueWithoutSongInput>>;
};

export type SongCommentUpdateManyWithoutUserNestedInput = {
  connect?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCommentCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<SongCommentCreateWithoutUserInput>;
  createMany?: InputMaybe<SongCommentCreateManyUserInputEnvelope>;
  delete?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongCommentScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  set?: InputMaybe<Array<SongCommentWhereUniqueInput>>;
  update?: InputMaybe<Array<SongCommentUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: InputMaybe<Array<SongCommentUpdateManyWithWhereWithoutUserInput>>;
  upsert?: InputMaybe<Array<SongCommentUpsertWithWhereUniqueWithoutUserInput>>;
};

export type SongCommentUpdateWithWhereUniqueWithoutSongInput = {
  data: SongCommentUpdateWithoutSongInput;
  where: SongCommentWhereUniqueInput;
};

export type SongCommentUpdateWithWhereUniqueWithoutUserInput = {
  data: SongCommentUpdateWithoutUserInput;
  where: SongCommentWhereUniqueInput;
};

export type SongCommentUpdateWithoutSongInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongCommentUpdateWithoutUserInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  song?: InputMaybe<SongUpdateOneRequiredWithoutCommentsNestedInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongCommentUpsertWithWhereUniqueWithoutSongInput = {
  create: SongCommentCreateWithoutSongInput;
  update: SongCommentUpdateWithoutSongInput;
  where: SongCommentWhereUniqueInput;
};

export type SongCommentUpsertWithWhereUniqueWithoutUserInput = {
  create: SongCommentCreateWithoutUserInput;
  update: SongCommentUpdateWithoutUserInput;
  where: SongCommentWhereUniqueInput;
};

export type SongCommentWhereInput = {
  AND?: InputMaybe<Array<SongCommentWhereInput>>;
  NOT?: InputMaybe<Array<SongCommentWhereInput>>;
  OR?: InputMaybe<Array<SongCommentWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  song?: InputMaybe<SongWhereInput>;
  songId?: InputMaybe<StringFilter>;
  text?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  user?: InputMaybe<UserWhereInput>;
  userId?: InputMaybe<StringFilter>;
};

export type SongCommentWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type SongCountOrderByAggregateInput = {
  canonicalName?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  translator?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  writer?: InputMaybe<SortOrder>;
};

export type SongCreateInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateManyCreatorInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateManyCreatorInputEnvelope = {
  data: Array<SongCreateManyCreatorInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SongCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  creatorId: Scalars['String'];
  id?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateNestedManyWithoutCreatorInput = {
  connect?: InputMaybe<Array<SongWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<SongCreateWithoutCreatorInput>;
  createMany?: InputMaybe<SongCreateManyCreatorInputEnvelope>;
};

export type SongCreateNestedManyWithoutLabelsInput = {
  connect?: InputMaybe<Array<SongWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCreateOrConnectWithoutLabelsInput>>;
  create?: InputMaybe<SongCreateWithoutLabelsInput>;
};

export type SongCreateNestedOneWithoutCommentsInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutCommentsInput>;
  create?: InputMaybe<SongCreateWithoutCommentsInput>;
};

export type SongCreateNestedOneWithoutLikesInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutLikesInput>;
  create?: InputMaybe<SongCreateWithoutLikesInput>;
};

export type SongCreateNestedOneWithoutLyricsInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutLyricsInput>;
  create?: InputMaybe<SongCreateWithoutLyricsInput>;
};

export type SongCreateNestedOneWithoutRecordingsInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutRecordingsInput>;
  create?: InputMaybe<SongCreateWithoutRecordingsInput>;
};

export type SongCreateNestedOneWithoutScoresInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutScoresInput>;
  create?: InputMaybe<SongCreateWithoutScoresInput>;
};

export type SongCreateNestedOneWithoutUsagesInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutUsagesInput>;
  create?: InputMaybe<SongCreateWithoutUsagesInput>;
};

export type SongCreateOrConnectWithoutCommentsInput = {
  create: SongCreateWithoutCommentsInput;
  where: SongWhereUniqueInput;
};

export type SongCreateOrConnectWithoutCreatorInput = {
  create: SongCreateWithoutCreatorInput;
  where: SongWhereUniqueInput;
};

export type SongCreateOrConnectWithoutLabelsInput = {
  create: SongCreateWithoutLabelsInput;
  where: SongWhereUniqueInput;
};

export type SongCreateOrConnectWithoutLikesInput = {
  create: SongCreateWithoutLikesInput;
  where: SongWhereUniqueInput;
};

export type SongCreateOrConnectWithoutLyricsInput = {
  create: SongCreateWithoutLyricsInput;
  where: SongWhereUniqueInput;
};

export type SongCreateOrConnectWithoutRecordingsInput = {
  create: SongCreateWithoutRecordingsInput;
  where: SongWhereUniqueInput;
};

export type SongCreateOrConnectWithoutScoresInput = {
  create: SongCreateWithoutScoresInput;
  where: SongWhereUniqueInput;
};

export type SongCreateOrConnectWithoutUsagesInput = {
  create: SongCreateWithoutUsagesInput;
  where: SongWhereUniqueInput;
};

export type SongCreateWithoutCommentsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateWithoutCreatorInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateWithoutLabelsInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateWithoutLikesInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateWithoutLyricsInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateWithoutRecordingsInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateWithoutScoresInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  usages?: InputMaybe<SlideGroupCreateNestedManyWithoutSongInput>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongCreateWithoutUsagesInput = {
  comments?: InputMaybe<SongCommentCreateNestedManyWithoutSongInput>;
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongsInput>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutSongInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutSongInput>;
  name: Scalars['String'];
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutSongInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutSongInput>;
  translator?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  writer?: InputMaybe<Scalars['String']>;
};

export type SongListRelationFilter = {
  every?: InputMaybe<SongWhereInput>;
  none?: InputMaybe<SongWhereInput>;
  some?: InputMaybe<SongWhereInput>;
};

export type SongMaxOrderByAggregateInput = {
  canonicalName?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  translator?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  writer?: InputMaybe<SortOrder>;
};

export type SongMinOrderByAggregateInput = {
  canonicalName?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  translator?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  writer?: InputMaybe<SortOrder>;
};

export type SongOrder = {
  createdAt: Scalars['DateTime'];
  groups: Array<SlideGroup>;
  id: Scalars['ID'];
  note?: Maybe<Scalars['String']>;
  presentationConfig: Scalars['Json'];
  title?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
};


export type SongOrderGroupsArgs = {
  cursor?: InputMaybe<SlideGroupWhereUniqueInput>;
  distinct?: InputMaybe<Array<SlideGroupScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SlideGroupOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SlideGroupWhereInput>;
};

export type SongOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type SongOrderByWithAggregationInput = {
  _count?: InputMaybe<SongCountOrderByAggregateInput>;
  _max?: InputMaybe<SongMaxOrderByAggregateInput>;
  _min?: InputMaybe<SongMinOrderByAggregateInput>;
  canonicalName?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  translator?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  writer?: InputMaybe<SortOrder>;
};

export type SongOrderByWithRelationInput = {
  canonicalName?: InputMaybe<SortOrder>;
  comments?: InputMaybe<SongCommentOrderByRelationAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  creator?: InputMaybe<UserOrderByWithRelationInput>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  labels?: InputMaybe<TagOrderByRelationAggregateInput>;
  likes?: InputMaybe<LikeOrderByRelationAggregateInput>;
  lyrics?: InputMaybe<LyricsOrderByRelationAggregateInput>;
  name?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  recordings?: InputMaybe<RecordingOrderByRelationAggregateInput>;
  scores?: InputMaybe<ScoreOrderByRelationAggregateInput>;
  translator?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  usages?: InputMaybe<SlideGroupOrderByRelationAggregateInput>;
  writer?: InputMaybe<SortOrder>;
};

export type SongOrderCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  note?: InputMaybe<SortOrder>;
  presentationConfig?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  groups?: InputMaybe<SlideGroupCreateNestedManyWithoutSongOrderInput>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongOrdersInput>;
  note?: InputMaybe<Scalars['String']>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderCreateManyCreatorInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderCreateManyCreatorInputEnvelope = {
  data: Array<SongOrderCreateManyCreatorInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SongOrderCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  creatorId: Scalars['String'];
  id?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderCreateNestedManyWithoutCreatorInput = {
  connect?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<SongOrderCreateWithoutCreatorInput>;
  createMany?: InputMaybe<SongOrderCreateManyCreatorInputEnvelope>;
};

export type SongOrderCreateNestedManyWithoutLabelsInput = {
  connect?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderCreateOrConnectWithoutLabelsInput>>;
  create?: InputMaybe<SongOrderCreateWithoutLabelsInput>;
};

export type SongOrderCreateNestedOneWithoutGroupsInput = {
  connect?: InputMaybe<SongOrderWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongOrderCreateOrConnectWithoutGroupsInput>;
  create?: InputMaybe<SongOrderCreateWithoutGroupsInput>;
};

export type SongOrderCreateOrConnectWithoutCreatorInput = {
  create: SongOrderCreateWithoutCreatorInput;
  where: SongOrderWhereUniqueInput;
};

export type SongOrderCreateOrConnectWithoutGroupsInput = {
  create: SongOrderCreateWithoutGroupsInput;
  where: SongOrderWhereUniqueInput;
};

export type SongOrderCreateOrConnectWithoutLabelsInput = {
  create: SongOrderCreateWithoutLabelsInput;
  where: SongOrderWhereUniqueInput;
};

export type SongOrderCreateWithoutCreatorInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  groups?: InputMaybe<SlideGroupCreateNestedManyWithoutSongOrderInput>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongOrdersInput>;
  note?: InputMaybe<Scalars['String']>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderCreateWithoutGroupsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  labels?: InputMaybe<TagCreateNestedManyWithoutSongOrdersInput>;
  note?: InputMaybe<Scalars['String']>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderCreateWithoutLabelsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  groups?: InputMaybe<SlideGroupCreateNestedManyWithoutSongOrderInput>;
  id?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderListRelationFilter = {
  every?: InputMaybe<SongOrderWhereInput>;
  none?: InputMaybe<SongOrderWhereInput>;
  some?: InputMaybe<SongOrderWhereInput>;
};

export type SongOrderMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  note?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  note?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type SongOrderOrderByWithAggregationInput = {
  _count?: InputMaybe<SongOrderCountOrderByAggregateInput>;
  _max?: InputMaybe<SongOrderMaxOrderByAggregateInput>;
  _min?: InputMaybe<SongOrderMinOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  note?: InputMaybe<SortOrder>;
  presentationConfig?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  creator?: InputMaybe<UserOrderByWithRelationInput>;
  creatorId?: InputMaybe<SortOrder>;
  groups?: InputMaybe<SlideGroupOrderByRelationAggregateInput>;
  id?: InputMaybe<SortOrder>;
  labels?: InputMaybe<TagOrderByRelationAggregateInput>;
  note?: InputMaybe<SortOrder>;
  presentationConfig?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderRelationFilter = {
  is?: InputMaybe<SongOrderWhereInput>;
  isNot?: InputMaybe<SongOrderWhereInput>;
};

export enum SongOrderScalarFieldEnum {
  CreatedAt = 'createdAt',
  CreatorId = 'creatorId',
  Id = 'id',
  Note = 'note',
  PresentationConfig = 'presentationConfig',
  Title = 'title',
  UpdatedAt = 'updatedAt'
}

export type SongOrderScalarWhereInput = {
  AND?: InputMaybe<Array<SongOrderScalarWhereInput>>;
  NOT?: InputMaybe<Array<SongOrderScalarWhereInput>>;
  OR?: InputMaybe<Array<SongOrderScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creatorId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  note?: InputMaybe<StringNullableFilter>;
  presentationConfig?: InputMaybe<JsonFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SongOrderScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<SongOrderScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<SongOrderScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<SongOrderScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  creatorId?: InputMaybe<StringWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  note?: InputMaybe<StringNullableWithAggregatesFilter>;
  presentationConfig?: InputMaybe<JsonWithAggregatesFilter>;
  title?: InputMaybe<StringNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
};

export type SongOrderTemplate = {
  createdAt: Scalars['DateTime'];
  creator: User;
  id: Scalars['ID'];
  items: Array<SongOrderTemplateItem>;
  publishStatus?: Maybe<PublishStatus>;
  title: Scalars['String'];
  updatedAt: Scalars['DateTime'];
};


export type SongOrderTemplateItemsArgs = {
  cursor?: InputMaybe<SongOrderTemplateItemWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongOrderTemplateItemScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderTemplateItemOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongOrderTemplateItemWhereInput>;
};

export type SongOrderTemplateCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  items?: InputMaybe<SongOrderTemplateItemCreateNestedManyWithoutTemplateInput>;
  title: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateCreateManyCreatorInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateCreateManyCreatorInputEnvelope = {
  data: Array<SongOrderTemplateCreateManyCreatorInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SongOrderTemplateCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  creatorId: Scalars['String'];
  id?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateCreateNestedManyWithoutCreatorInput = {
  connect?: InputMaybe<Array<SongOrderTemplateWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderTemplateCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<SongOrderTemplateCreateWithoutCreatorInput>;
  createMany?: InputMaybe<SongOrderTemplateCreateManyCreatorInputEnvelope>;
};

export type SongOrderTemplateCreateNestedOneWithoutItemsInput = {
  connect?: InputMaybe<SongOrderTemplateWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongOrderTemplateCreateOrConnectWithoutItemsInput>;
  create?: InputMaybe<SongOrderTemplateCreateWithoutItemsInput>;
};

export type SongOrderTemplateCreateOrConnectWithoutCreatorInput = {
  create: SongOrderTemplateCreateWithoutCreatorInput;
  where: SongOrderTemplateWhereUniqueInput;
};

export type SongOrderTemplateCreateOrConnectWithoutItemsInput = {
  create: SongOrderTemplateCreateWithoutItemsInput;
  where: SongOrderTemplateWhereUniqueInput;
};

export type SongOrderTemplateCreateWithoutCreatorInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  items?: InputMaybe<SongOrderTemplateItemCreateNestedManyWithoutTemplateInput>;
  title: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateCreateWithoutItemsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateItem = {
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  order: Scalars['Int'];
  template: SongOrderTemplate;
  title?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
};

export type SongOrderTemplateItemAvgOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateItemCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  templateId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateItemCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  template: SongOrderTemplateCreateNestedOneWithoutItemsInput;
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateItemCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  templateId: Scalars['String'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateItemCreateManyTemplateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateItemCreateManyTemplateInputEnvelope = {
  data: Array<SongOrderTemplateItemCreateManyTemplateInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type SongOrderTemplateItemCreateNestedManyWithoutTemplateInput = {
  connect?: InputMaybe<Array<SongOrderTemplateItemWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderTemplateItemCreateOrConnectWithoutTemplateInput>>;
  create?: InputMaybe<SongOrderTemplateItemCreateWithoutTemplateInput>;
  createMany?: InputMaybe<SongOrderTemplateItemCreateManyTemplateInputEnvelope>;
};

export type SongOrderTemplateItemCreateOrConnectWithoutTemplateInput = {
  create: SongOrderTemplateItemCreateWithoutTemplateInput;
  where: SongOrderTemplateItemWhereUniqueInput;
};

export type SongOrderTemplateItemCreateWithoutTemplateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order: Scalars['Int'];
  title?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type SongOrderTemplateItemListRelationFilter = {
  every?: InputMaybe<SongOrderTemplateItemWhereInput>;
  none?: InputMaybe<SongOrderTemplateItemWhereInput>;
  some?: InputMaybe<SongOrderTemplateItemWhereInput>;
};

export type SongOrderTemplateItemMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  templateId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateItemMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  templateId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateItemOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateItemOrderByWithAggregationInput = {
  _avg?: InputMaybe<SongOrderTemplateItemAvgOrderByAggregateInput>;
  _count?: InputMaybe<SongOrderTemplateItemCountOrderByAggregateInput>;
  _max?: InputMaybe<SongOrderTemplateItemMaxOrderByAggregateInput>;
  _min?: InputMaybe<SongOrderTemplateItemMinOrderByAggregateInput>;
  _sum?: InputMaybe<SongOrderTemplateItemSumOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  templateId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateItemOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  template?: InputMaybe<SongOrderTemplateOrderByWithRelationInput>;
  templateId?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export enum SongOrderTemplateItemScalarFieldEnum {
  CreatedAt = 'createdAt',
  Id = 'id',
  Order = 'order',
  TemplateId = 'templateId',
  Title = 'title',
  UpdatedAt = 'updatedAt'
}

export type SongOrderTemplateItemScalarWhereInput = {
  AND?: InputMaybe<Array<SongOrderTemplateItemScalarWhereInput>>;
  NOT?: InputMaybe<Array<SongOrderTemplateItemScalarWhereInput>>;
  OR?: InputMaybe<Array<SongOrderTemplateItemScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  order?: InputMaybe<IntFilter>;
  templateId?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SongOrderTemplateItemScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<SongOrderTemplateItemScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<SongOrderTemplateItemScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<SongOrderTemplateItemScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  order?: InputMaybe<IntWithAggregatesFilter>;
  templateId?: InputMaybe<StringWithAggregatesFilter>;
  title?: InputMaybe<StringNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
};

export type SongOrderTemplateItemSumOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateItemUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  template?: InputMaybe<SongOrderTemplateUpdateOneRequiredWithoutItemsNestedInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderTemplateItemUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderTemplateItemUpdateManyWithWhereWithoutTemplateInput = {
  data: SongOrderTemplateItemUpdateManyMutationInput;
  where: SongOrderTemplateItemScalarWhereInput;
};

export type SongOrderTemplateItemUpdateManyWithoutTemplateNestedInput = {
  connect?: InputMaybe<Array<SongOrderTemplateItemWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderTemplateItemCreateOrConnectWithoutTemplateInput>>;
  create?: InputMaybe<SongOrderTemplateItemCreateWithoutTemplateInput>;
  createMany?: InputMaybe<SongOrderTemplateItemCreateManyTemplateInputEnvelope>;
  delete?: InputMaybe<Array<SongOrderTemplateItemWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongOrderTemplateItemScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongOrderTemplateItemWhereUniqueInput>>;
  set?: InputMaybe<Array<SongOrderTemplateItemWhereUniqueInput>>;
  update?: InputMaybe<Array<SongOrderTemplateItemUpdateWithWhereUniqueWithoutTemplateInput>>;
  updateMany?: InputMaybe<Array<SongOrderTemplateItemUpdateManyWithWhereWithoutTemplateInput>>;
  upsert?: InputMaybe<Array<SongOrderTemplateItemUpsertWithWhereUniqueWithoutTemplateInput>>;
};

export type SongOrderTemplateItemUpdateWithWhereUniqueWithoutTemplateInput = {
  data: SongOrderTemplateItemUpdateWithoutTemplateInput;
  where: SongOrderTemplateItemWhereUniqueInput;
};

export type SongOrderTemplateItemUpdateWithoutTemplateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<IntFieldUpdateOperationsInput>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderTemplateItemUpsertWithWhereUniqueWithoutTemplateInput = {
  create: SongOrderTemplateItemCreateWithoutTemplateInput;
  update: SongOrderTemplateItemUpdateWithoutTemplateInput;
  where: SongOrderTemplateItemWhereUniqueInput;
};

export type SongOrderTemplateItemWhereInput = {
  AND?: InputMaybe<Array<SongOrderTemplateItemWhereInput>>;
  NOT?: InputMaybe<Array<SongOrderTemplateItemWhereInput>>;
  OR?: InputMaybe<Array<SongOrderTemplateItemWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  order?: InputMaybe<IntFilter>;
  template?: InputMaybe<SongOrderTemplateWhereInput>;
  templateId?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SongOrderTemplateItemWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type SongOrderTemplateListRelationFilter = {
  every?: InputMaybe<SongOrderTemplateWhereInput>;
  none?: InputMaybe<SongOrderTemplateWhereInput>;
  some?: InputMaybe<SongOrderTemplateWhereInput>;
};

export type SongOrderTemplateMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateOrderByWithAggregationInput = {
  _count?: InputMaybe<SongOrderTemplateCountOrderByAggregateInput>;
  _max?: InputMaybe<SongOrderTemplateMaxOrderByAggregateInput>;
  _min?: InputMaybe<SongOrderTemplateMinOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  publishStatus?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  creator?: InputMaybe<UserOrderByWithRelationInput>;
  creatorId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  items?: InputMaybe<SongOrderTemplateItemOrderByRelationAggregateInput>;
  publishStatus?: InputMaybe<SortOrder>;
  title?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type SongOrderTemplateRelationFilter = {
  is?: InputMaybe<SongOrderTemplateWhereInput>;
  isNot?: InputMaybe<SongOrderTemplateWhereInput>;
};

export enum SongOrderTemplateScalarFieldEnum {
  CreatedAt = 'createdAt',
  CreatorId = 'creatorId',
  Id = 'id',
  PublishStatus = 'publishStatus',
  Title = 'title',
  UpdatedAt = 'updatedAt'
}

export type SongOrderTemplateScalarWhereInput = {
  AND?: InputMaybe<Array<SongOrderTemplateScalarWhereInput>>;
  NOT?: InputMaybe<Array<SongOrderTemplateScalarWhereInput>>;
  OR?: InputMaybe<Array<SongOrderTemplateScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creatorId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  title?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SongOrderTemplateScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<SongOrderTemplateScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<SongOrderTemplateScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<SongOrderTemplateScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  creatorId?: InputMaybe<StringWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableWithAggregatesFilter>;
  title?: InputMaybe<StringWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
};

export type SongOrderTemplateUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  items?: InputMaybe<SongOrderTemplateItemUpdateManyWithoutTemplateNestedInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  title?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderTemplateUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  title?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderTemplateUpdateManyWithWhereWithoutCreatorInput = {
  data: SongOrderTemplateUpdateManyMutationInput;
  where: SongOrderTemplateScalarWhereInput;
};

export type SongOrderTemplateUpdateManyWithoutCreatorNestedInput = {
  connect?: InputMaybe<Array<SongOrderTemplateWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderTemplateCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<SongOrderTemplateCreateWithoutCreatorInput>;
  createMany?: InputMaybe<SongOrderTemplateCreateManyCreatorInputEnvelope>;
  delete?: InputMaybe<Array<SongOrderTemplateWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongOrderTemplateScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongOrderTemplateWhereUniqueInput>>;
  set?: InputMaybe<Array<SongOrderTemplateWhereUniqueInput>>;
  update?: InputMaybe<Array<SongOrderTemplateUpdateWithWhereUniqueWithoutCreatorInput>>;
  updateMany?: InputMaybe<Array<SongOrderTemplateUpdateManyWithWhereWithoutCreatorInput>>;
  upsert?: InputMaybe<Array<SongOrderTemplateUpsertWithWhereUniqueWithoutCreatorInput>>;
};

export type SongOrderTemplateUpdateOneRequiredWithoutItemsNestedInput = {
  connect?: InputMaybe<SongOrderTemplateWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongOrderTemplateCreateOrConnectWithoutItemsInput>;
  create?: InputMaybe<SongOrderTemplateCreateWithoutItemsInput>;
  update?: InputMaybe<SongOrderTemplateUpdateWithoutItemsInput>;
  upsert?: InputMaybe<SongOrderTemplateUpsertWithoutItemsInput>;
};

export type SongOrderTemplateUpdateWithWhereUniqueWithoutCreatorInput = {
  data: SongOrderTemplateUpdateWithoutCreatorInput;
  where: SongOrderTemplateWhereUniqueInput;
};

export type SongOrderTemplateUpdateWithoutCreatorInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  items?: InputMaybe<SongOrderTemplateItemUpdateManyWithoutTemplateNestedInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  title?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderTemplateUpdateWithoutItemsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  title?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderTemplateUpsertWithWhereUniqueWithoutCreatorInput = {
  create: SongOrderTemplateCreateWithoutCreatorInput;
  update: SongOrderTemplateUpdateWithoutCreatorInput;
  where: SongOrderTemplateWhereUniqueInput;
};

export type SongOrderTemplateUpsertWithoutItemsInput = {
  create: SongOrderTemplateCreateWithoutItemsInput;
  update: SongOrderTemplateUpdateWithoutItemsInput;
};

export type SongOrderTemplateWhereInput = {
  AND?: InputMaybe<Array<SongOrderTemplateWhereInput>>;
  NOT?: InputMaybe<Array<SongOrderTemplateWhereInput>>;
  OR?: InputMaybe<Array<SongOrderTemplateWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creator?: InputMaybe<UserWhereInput>;
  creatorId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  items?: InputMaybe<SongOrderTemplateItemListRelationFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  title?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SongOrderTemplateWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type SongOrderUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  groups?: InputMaybe<SlideGroupUpdateManyWithoutSongOrderNestedInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongOrdersNestedInput>;
  note?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  note?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderUpdateManyWithWhereWithoutCreatorInput = {
  data: SongOrderUpdateManyMutationInput;
  where: SongOrderScalarWhereInput;
};

export type SongOrderUpdateManyWithWhereWithoutLabelsInput = {
  data: SongOrderUpdateManyMutationInput;
  where: SongOrderScalarWhereInput;
};

export type SongOrderUpdateManyWithoutCreatorNestedInput = {
  connect?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<SongOrderCreateWithoutCreatorInput>;
  createMany?: InputMaybe<SongOrderCreateManyCreatorInputEnvelope>;
  delete?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongOrderScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  set?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  update?: InputMaybe<Array<SongOrderUpdateWithWhereUniqueWithoutCreatorInput>>;
  updateMany?: InputMaybe<Array<SongOrderUpdateManyWithWhereWithoutCreatorInput>>;
  upsert?: InputMaybe<Array<SongOrderUpsertWithWhereUniqueWithoutCreatorInput>>;
};

export type SongOrderUpdateManyWithoutLabelsNestedInput = {
  connect?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongOrderCreateOrConnectWithoutLabelsInput>>;
  create?: InputMaybe<SongOrderCreateWithoutLabelsInput>;
  delete?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongOrderScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  set?: InputMaybe<Array<SongOrderWhereUniqueInput>>;
  update?: InputMaybe<Array<SongOrderUpdateWithWhereUniqueWithoutLabelsInput>>;
  updateMany?: InputMaybe<Array<SongOrderUpdateManyWithWhereWithoutLabelsInput>>;
  upsert?: InputMaybe<Array<SongOrderUpsertWithWhereUniqueWithoutLabelsInput>>;
};

export type SongOrderUpdateOneRequiredWithoutGroupsNestedInput = {
  connect?: InputMaybe<SongOrderWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongOrderCreateOrConnectWithoutGroupsInput>;
  create?: InputMaybe<SongOrderCreateWithoutGroupsInput>;
  update?: InputMaybe<SongOrderUpdateWithoutGroupsInput>;
  upsert?: InputMaybe<SongOrderUpsertWithoutGroupsInput>;
};

export type SongOrderUpdateWithWhereUniqueWithoutCreatorInput = {
  data: SongOrderUpdateWithoutCreatorInput;
  where: SongOrderWhereUniqueInput;
};

export type SongOrderUpdateWithWhereUniqueWithoutLabelsInput = {
  data: SongOrderUpdateWithoutLabelsInput;
  where: SongOrderWhereUniqueInput;
};

export type SongOrderUpdateWithoutCreatorInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  groups?: InputMaybe<SlideGroupUpdateManyWithoutSongOrderNestedInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongOrdersNestedInput>;
  note?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderUpdateWithoutGroupsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongOrdersNestedInput>;
  note?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderUpdateWithoutLabelsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  groups?: InputMaybe<SlideGroupUpdateManyWithoutSongOrderNestedInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  note?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  presentationConfig?: InputMaybe<Scalars['Json']>;
  title?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type SongOrderUpsertWithWhereUniqueWithoutCreatorInput = {
  create: SongOrderCreateWithoutCreatorInput;
  update: SongOrderUpdateWithoutCreatorInput;
  where: SongOrderWhereUniqueInput;
};

export type SongOrderUpsertWithWhereUniqueWithoutLabelsInput = {
  create: SongOrderCreateWithoutLabelsInput;
  update: SongOrderUpdateWithoutLabelsInput;
  where: SongOrderWhereUniqueInput;
};

export type SongOrderUpsertWithoutGroupsInput = {
  create: SongOrderCreateWithoutGroupsInput;
  update: SongOrderUpdateWithoutGroupsInput;
};

export type SongOrderWhereInput = {
  AND?: InputMaybe<Array<SongOrderWhereInput>>;
  NOT?: InputMaybe<Array<SongOrderWhereInput>>;
  OR?: InputMaybe<Array<SongOrderWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creator?: InputMaybe<UserWhereInput>;
  creatorId?: InputMaybe<StringFilter>;
  groups?: InputMaybe<SlideGroupListRelationFilter>;
  id?: InputMaybe<StringFilter>;
  labels?: InputMaybe<TagListRelationFilter>;
  note?: InputMaybe<StringNullableFilter>;
  presentationConfig?: InputMaybe<JsonFilter>;
  title?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type SongOrderWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type SongRelationFilter = {
  is?: InputMaybe<SongWhereInput>;
  isNot?: InputMaybe<SongWhereInput>;
};

export enum SongScalarFieldEnum {
  CanonicalName = 'canonicalName',
  CreatedAt = 'createdAt',
  CreatorId = 'creatorId',
  Id = 'id',
  Name = 'name',
  PublishStatus = 'publishStatus',
  Translator = 'translator',
  UpdatedAt = 'updatedAt',
  Writer = 'writer'
}

export type SongScalarWhereInput = {
  AND?: InputMaybe<Array<SongScalarWhereInput>>;
  NOT?: InputMaybe<Array<SongScalarWhereInput>>;
  OR?: InputMaybe<Array<SongScalarWhereInput>>;
  canonicalName?: InputMaybe<StringFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creatorId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  name?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  translator?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  writer?: InputMaybe<StringNullableFilter>;
};

export type SongScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<SongScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<SongScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<SongScalarWhereWithAggregatesInput>>;
  canonicalName?: InputMaybe<StringWithAggregatesFilter>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  creatorId?: InputMaybe<StringWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  name?: InputMaybe<StringWithAggregatesFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableWithAggregatesFilter>;
  translator?: InputMaybe<StringNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  writer?: InputMaybe<StringNullableWithAggregatesFilter>;
};

export type SongUpdateInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateManyWithWhereWithoutCreatorInput = {
  data: SongUpdateManyMutationInput;
  where: SongScalarWhereInput;
};

export type SongUpdateManyWithWhereWithoutLabelsInput = {
  data: SongUpdateManyMutationInput;
  where: SongScalarWhereInput;
};

export type SongUpdateManyWithoutCreatorNestedInput = {
  connect?: InputMaybe<Array<SongWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCreateOrConnectWithoutCreatorInput>>;
  create?: InputMaybe<SongCreateWithoutCreatorInput>;
  createMany?: InputMaybe<SongCreateManyCreatorInputEnvelope>;
  delete?: InputMaybe<Array<SongWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongWhereUniqueInput>>;
  set?: InputMaybe<Array<SongWhereUniqueInput>>;
  update?: InputMaybe<Array<SongUpdateWithWhereUniqueWithoutCreatorInput>>;
  updateMany?: InputMaybe<Array<SongUpdateManyWithWhereWithoutCreatorInput>>;
  upsert?: InputMaybe<Array<SongUpsertWithWhereUniqueWithoutCreatorInput>>;
};

export type SongUpdateManyWithoutLabelsNestedInput = {
  connect?: InputMaybe<Array<SongWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<SongCreateOrConnectWithoutLabelsInput>>;
  create?: InputMaybe<SongCreateWithoutLabelsInput>;
  delete?: InputMaybe<Array<SongWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<SongScalarWhereInput>>;
  disconnect?: InputMaybe<Array<SongWhereUniqueInput>>;
  set?: InputMaybe<Array<SongWhereUniqueInput>>;
  update?: InputMaybe<Array<SongUpdateWithWhereUniqueWithoutLabelsInput>>;
  updateMany?: InputMaybe<Array<SongUpdateManyWithWhereWithoutLabelsInput>>;
  upsert?: InputMaybe<Array<SongUpsertWithWhereUniqueWithoutLabelsInput>>;
};

export type SongUpdateOneRequiredWithoutCommentsNestedInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutCommentsInput>;
  create?: InputMaybe<SongCreateWithoutCommentsInput>;
  update?: InputMaybe<SongUpdateWithoutCommentsInput>;
  upsert?: InputMaybe<SongUpsertWithoutCommentsInput>;
};

export type SongUpdateOneRequiredWithoutLikesNestedInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutLikesInput>;
  create?: InputMaybe<SongCreateWithoutLikesInput>;
  update?: InputMaybe<SongUpdateWithoutLikesInput>;
  upsert?: InputMaybe<SongUpsertWithoutLikesInput>;
};

export type SongUpdateOneRequiredWithoutRecordingsNestedInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutRecordingsInput>;
  create?: InputMaybe<SongCreateWithoutRecordingsInput>;
  update?: InputMaybe<SongUpdateWithoutRecordingsInput>;
  upsert?: InputMaybe<SongUpsertWithoutRecordingsInput>;
};

export type SongUpdateOneRequiredWithoutScoresNestedInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutScoresInput>;
  create?: InputMaybe<SongCreateWithoutScoresInput>;
  update?: InputMaybe<SongUpdateWithoutScoresInput>;
  upsert?: InputMaybe<SongUpsertWithoutScoresInput>;
};

export type SongUpdateOneWithoutLyricsNestedInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutLyricsInput>;
  create?: InputMaybe<SongCreateWithoutLyricsInput>;
  delete?: InputMaybe<Scalars['Boolean']>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
  update?: InputMaybe<SongUpdateWithoutLyricsInput>;
  upsert?: InputMaybe<SongUpsertWithoutLyricsInput>;
};

export type SongUpdateOneWithoutUsagesNestedInput = {
  connect?: InputMaybe<SongWhereUniqueInput>;
  connectOrCreate?: InputMaybe<SongCreateOrConnectWithoutUsagesInput>;
  create?: InputMaybe<SongCreateWithoutUsagesInput>;
  delete?: InputMaybe<Scalars['Boolean']>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
  update?: InputMaybe<SongUpdateWithoutUsagesInput>;
  upsert?: InputMaybe<SongUpsertWithoutUsagesInput>;
};

export type SongUpdateWithWhereUniqueWithoutCreatorInput = {
  data: SongUpdateWithoutCreatorInput;
  where: SongWhereUniqueInput;
};

export type SongUpdateWithWhereUniqueWithoutLabelsInput = {
  data: SongUpdateWithoutLabelsInput;
  where: SongWhereUniqueInput;
};

export type SongUpdateWithoutCommentsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateWithoutCreatorInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateWithoutLabelsInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateWithoutLikesInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateWithoutLyricsInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateWithoutRecordingsInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateWithoutScoresInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  usages?: InputMaybe<SlideGroupUpdateManyWithoutSongNestedInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpdateWithoutUsagesInput = {
  comments?: InputMaybe<SongCommentUpdateManyWithoutSongNestedInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  labels?: InputMaybe<TagUpdateManyWithoutSongsNestedInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutSongNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutSongNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  publishStatus?: InputMaybe<NullableEnumPublishStatusFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutSongNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutSongNestedInput>;
  translator?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  writer?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
};

export type SongUpsertWithWhereUniqueWithoutCreatorInput = {
  create: SongCreateWithoutCreatorInput;
  update: SongUpdateWithoutCreatorInput;
  where: SongWhereUniqueInput;
};

export type SongUpsertWithWhereUniqueWithoutLabelsInput = {
  create: SongCreateWithoutLabelsInput;
  update: SongUpdateWithoutLabelsInput;
  where: SongWhereUniqueInput;
};

export type SongUpsertWithoutCommentsInput = {
  create: SongCreateWithoutCommentsInput;
  update: SongUpdateWithoutCommentsInput;
};

export type SongUpsertWithoutLikesInput = {
  create: SongCreateWithoutLikesInput;
  update: SongUpdateWithoutLikesInput;
};

export type SongUpsertWithoutLyricsInput = {
  create: SongCreateWithoutLyricsInput;
  update: SongUpdateWithoutLyricsInput;
};

export type SongUpsertWithoutRecordingsInput = {
  create: SongCreateWithoutRecordingsInput;
  update: SongUpdateWithoutRecordingsInput;
};

export type SongUpsertWithoutScoresInput = {
  create: SongCreateWithoutScoresInput;
  update: SongUpdateWithoutScoresInput;
};

export type SongUpsertWithoutUsagesInput = {
  create: SongCreateWithoutUsagesInput;
  update: SongUpdateWithoutUsagesInput;
};

export type SongWhereInput = {
  AND?: InputMaybe<Array<SongWhereInput>>;
  NOT?: InputMaybe<Array<SongWhereInput>>;
  OR?: InputMaybe<Array<SongWhereInput>>;
  canonicalName?: InputMaybe<StringFilter>;
  comments?: InputMaybe<SongCommentListRelationFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  creator?: InputMaybe<UserWhereInput>;
  creatorId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  labels?: InputMaybe<TagListRelationFilter>;
  likes?: InputMaybe<LikeListRelationFilter>;
  lyrics?: InputMaybe<LyricsListRelationFilter>;
  name?: InputMaybe<StringFilter>;
  publishStatus?: InputMaybe<EnumPublishStatusNullableFilter>;
  recordings?: InputMaybe<RecordingListRelationFilter>;
  scores?: InputMaybe<ScoreListRelationFilter>;
  translator?: InputMaybe<StringNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  usages?: InputMaybe<SlideGroupListRelationFilter>;
  writer?: InputMaybe<StringNullableFilter>;
};

export type SongWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export enum SortOrder {
  Asc = 'asc',
  Desc = 'desc'
}

export type StringFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['String']>;
};

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type StringNullableFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type StringNullableListFilter = {
  equals?: InputMaybe<Array<Scalars['String']>>;
  has?: InputMaybe<Scalars['String']>;
  hasEvery?: InputMaybe<Array<Scalars['String']>>;
  hasSome?: InputMaybe<Array<Scalars['String']>>;
  isEmpty?: InputMaybe<Scalars['Boolean']>;
};

export type StringNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedStringNullableFilter>;
  _min?: InputMaybe<NestedStringNullableFilter>;
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type StringWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedStringFilter>;
  _min?: InputMaybe<NestedStringFilter>;
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type Tag = {
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  scores: Array<Song>;
  text: Scalars['String'];
  updatedAt: Scalars['DateTime'];
  user?: Maybe<User>;
};


export type TagScoresArgs = {
  cursor?: InputMaybe<SongWhereUniqueInput>;
  distinct?: InputMaybe<Array<SongScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<SongOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SongWhereInput>;
};

export type TagCountOrderByAggregateInput = {
  canonicalText?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type TagCreateInput = {
  canonicalText: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutLabelsInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutLabelsInput>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  user?: InputMaybe<UserCreateNestedOneWithoutTagsInput>;
};

export type TagCreateManyInput = {
  canonicalText: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  userId?: InputMaybe<Scalars['String']>;
};

export type TagCreateManyUserInput = {
  canonicalText: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type TagCreateManyUserInputEnvelope = {
  data: Array<TagCreateManyUserInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type TagCreateNestedManyWithoutSongOrdersInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<TagCreateOrConnectWithoutSongOrdersInput>>;
  create?: InputMaybe<TagCreateWithoutSongOrdersInput>;
};

export type TagCreateNestedManyWithoutSongsInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<TagCreateOrConnectWithoutSongsInput>>;
  create?: InputMaybe<TagCreateWithoutSongsInput>;
};

export type TagCreateNestedManyWithoutUserInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<TagCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<TagCreateWithoutUserInput>;
  createMany?: InputMaybe<TagCreateManyUserInputEnvelope>;
};

export type TagCreateOrConnectWithoutSongOrdersInput = {
  create: TagCreateWithoutSongOrdersInput;
  where: TagWhereUniqueInput;
};

export type TagCreateOrConnectWithoutSongsInput = {
  create: TagCreateWithoutSongsInput;
  where: TagWhereUniqueInput;
};

export type TagCreateOrConnectWithoutUserInput = {
  create: TagCreateWithoutUserInput;
  where: TagWhereUniqueInput;
};

export type TagCreateWithoutSongOrdersInput = {
  canonicalText: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songs?: InputMaybe<SongCreateNestedManyWithoutLabelsInput>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  user?: InputMaybe<UserCreateNestedOneWithoutTagsInput>;
};

export type TagCreateWithoutSongsInput = {
  canonicalText: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutLabelsInput>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
  user?: InputMaybe<UserCreateNestedOneWithoutTagsInput>;
};

export type TagCreateWithoutUserInput = {
  canonicalText: Scalars['String'];
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutLabelsInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutLabelsInput>;
  text: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type TagListRelationFilter = {
  every?: InputMaybe<TagWhereInput>;
  none?: InputMaybe<TagWhereInput>;
  some?: InputMaybe<TagWhereInput>;
};

export type TagMaxOrderByAggregateInput = {
  canonicalText?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type TagMinOrderByAggregateInput = {
  canonicalText?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type TagOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type TagOrderByWithAggregationInput = {
  _count?: InputMaybe<TagCountOrderByAggregateInput>;
  _max?: InputMaybe<TagMaxOrderByAggregateInput>;
  _min?: InputMaybe<TagMinOrderByAggregateInput>;
  canonicalText?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type TagOrderByWithRelationInput = {
  canonicalText?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  songOrders?: InputMaybe<SongOrderOrderByRelationAggregateInput>;
  songs?: InputMaybe<SongOrderByRelationAggregateInput>;
  text?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
  user?: InputMaybe<UserOrderByWithRelationInput>;
  userId?: InputMaybe<SortOrder>;
};

export enum TagScalarFieldEnum {
  CanonicalText = 'canonicalText',
  CreatedAt = 'createdAt',
  Id = 'id',
  Text = 'text',
  UpdatedAt = 'updatedAt',
  UserId = 'userId'
}

export type TagScalarWhereInput = {
  AND?: InputMaybe<Array<TagScalarWhereInput>>;
  NOT?: InputMaybe<Array<TagScalarWhereInput>>;
  OR?: InputMaybe<Array<TagScalarWhereInput>>;
  canonicalText?: InputMaybe<StringFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  text?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  userId?: InputMaybe<StringNullableFilter>;
};

export type TagScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<TagScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<TagScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<TagScalarWhereWithAggregatesInput>>;
  canonicalText?: InputMaybe<StringWithAggregatesFilter>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  text?: InputMaybe<StringWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  userId?: InputMaybe<StringNullableWithAggregatesFilter>;
};

export type TagUpdateInput = {
  canonicalText?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutLabelsNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutLabelsNestedInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  user?: InputMaybe<UserUpdateOneWithoutTagsNestedInput>;
};

export type TagUpdateManyMutationInput = {
  canonicalText?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type TagUpdateManyWithWhereWithoutSongOrdersInput = {
  data: TagUpdateManyMutationInput;
  where: TagScalarWhereInput;
};

export type TagUpdateManyWithWhereWithoutSongsInput = {
  data: TagUpdateManyMutationInput;
  where: TagScalarWhereInput;
};

export type TagUpdateManyWithWhereWithoutUserInput = {
  data: TagUpdateManyMutationInput;
  where: TagScalarWhereInput;
};

export type TagUpdateManyWithoutSongOrdersNestedInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<TagCreateOrConnectWithoutSongOrdersInput>>;
  create?: InputMaybe<TagCreateWithoutSongOrdersInput>;
  delete?: InputMaybe<Array<TagWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<TagScalarWhereInput>>;
  disconnect?: InputMaybe<Array<TagWhereUniqueInput>>;
  set?: InputMaybe<Array<TagWhereUniqueInput>>;
  update?: InputMaybe<Array<TagUpdateWithWhereUniqueWithoutSongOrdersInput>>;
  updateMany?: InputMaybe<Array<TagUpdateManyWithWhereWithoutSongOrdersInput>>;
  upsert?: InputMaybe<Array<TagUpsertWithWhereUniqueWithoutSongOrdersInput>>;
};

export type TagUpdateManyWithoutSongsNestedInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<TagCreateOrConnectWithoutSongsInput>>;
  create?: InputMaybe<TagCreateWithoutSongsInput>;
  delete?: InputMaybe<Array<TagWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<TagScalarWhereInput>>;
  disconnect?: InputMaybe<Array<TagWhereUniqueInput>>;
  set?: InputMaybe<Array<TagWhereUniqueInput>>;
  update?: InputMaybe<Array<TagUpdateWithWhereUniqueWithoutSongsInput>>;
  updateMany?: InputMaybe<Array<TagUpdateManyWithWhereWithoutSongsInput>>;
  upsert?: InputMaybe<Array<TagUpsertWithWhereUniqueWithoutSongsInput>>;
};

export type TagUpdateManyWithoutUserNestedInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<TagCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<TagCreateWithoutUserInput>;
  createMany?: InputMaybe<TagCreateManyUserInputEnvelope>;
  delete?: InputMaybe<Array<TagWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<TagScalarWhereInput>>;
  disconnect?: InputMaybe<Array<TagWhereUniqueInput>>;
  set?: InputMaybe<Array<TagWhereUniqueInput>>;
  update?: InputMaybe<Array<TagUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: InputMaybe<Array<TagUpdateManyWithWhereWithoutUserInput>>;
  upsert?: InputMaybe<Array<TagUpsertWithWhereUniqueWithoutUserInput>>;
};

export type TagUpdateWithWhereUniqueWithoutSongOrdersInput = {
  data: TagUpdateWithoutSongOrdersInput;
  where: TagWhereUniqueInput;
};

export type TagUpdateWithWhereUniqueWithoutSongsInput = {
  data: TagUpdateWithoutSongsInput;
  where: TagWhereUniqueInput;
};

export type TagUpdateWithWhereUniqueWithoutUserInput = {
  data: TagUpdateWithoutUserInput;
  where: TagWhereUniqueInput;
};

export type TagUpdateWithoutSongOrdersInput = {
  canonicalText?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  songs?: InputMaybe<SongUpdateManyWithoutLabelsNestedInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  user?: InputMaybe<UserUpdateOneWithoutTagsNestedInput>;
};

export type TagUpdateWithoutSongsInput = {
  canonicalText?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutLabelsNestedInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  user?: InputMaybe<UserUpdateOneWithoutTagsNestedInput>;
};

export type TagUpdateWithoutUserInput = {
  canonicalText?: InputMaybe<StringFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutLabelsNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutLabelsNestedInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type TagUpsertWithWhereUniqueWithoutSongOrdersInput = {
  create: TagCreateWithoutSongOrdersInput;
  update: TagUpdateWithoutSongOrdersInput;
  where: TagWhereUniqueInput;
};

export type TagUpsertWithWhereUniqueWithoutSongsInput = {
  create: TagCreateWithoutSongsInput;
  update: TagUpdateWithoutSongsInput;
  where: TagWhereUniqueInput;
};

export type TagUpsertWithWhereUniqueWithoutUserInput = {
  create: TagCreateWithoutUserInput;
  update: TagUpdateWithoutUserInput;
  where: TagWhereUniqueInput;
};

export type TagWhereInput = {
  AND?: InputMaybe<Array<TagWhereInput>>;
  NOT?: InputMaybe<Array<TagWhereInput>>;
  OR?: InputMaybe<Array<TagWhereInput>>;
  canonicalText?: InputMaybe<StringFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  songOrders?: InputMaybe<SongOrderListRelationFilter>;
  songs?: InputMaybe<SongListRelationFilter>;
  text?: InputMaybe<StringFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
  user?: InputMaybe<UserWhereInput>;
  userId?: InputMaybe<StringNullableFilter>;
};

export type TagWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export enum TransactionIsolationLevel {
  ReadCommitted = 'ReadCommitted',
  ReadUncommitted = 'ReadUncommitted',
  RepeatableRead = 'RepeatableRead',
  Serializable = 'Serializable'
}

export type User = {
  createdAt: Scalars['DateTime'];
  email: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  profileImage?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
};

export type UserAvgOrderByAggregateInput = {
  failCount?: InputMaybe<SortOrder>;
};

export type UserCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  email?: InputMaybe<SortOrder>;
  failCount?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isActive?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  isLocked?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  profileImage?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type UserCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateNestedOneWithoutLikesInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutLikesInput>;
  create?: InputMaybe<UserCreateWithoutLikesInput>;
};

export type UserCreateNestedOneWithoutLyricsInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutLyricsInput>;
  create?: InputMaybe<UserCreateWithoutLyricsInput>;
};

export type UserCreateNestedOneWithoutRecordingsInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutRecordingsInput>;
  create?: InputMaybe<UserCreateWithoutRecordingsInput>;
};

export type UserCreateNestedOneWithoutScoresInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutScoresInput>;
  create?: InputMaybe<UserCreateWithoutScoresInput>;
};

export type UserCreateNestedOneWithoutSongCommentsInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongCommentsInput>;
  create?: InputMaybe<UserCreateWithoutSongCommentsInput>;
};

export type UserCreateNestedOneWithoutSongOrderTemplatesInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongOrderTemplatesInput>;
  create?: InputMaybe<UserCreateWithoutSongOrderTemplatesInput>;
};

export type UserCreateNestedOneWithoutSongOrdersInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongOrdersInput>;
  create?: InputMaybe<UserCreateWithoutSongOrdersInput>;
};

export type UserCreateNestedOneWithoutSongsInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongsInput>;
  create?: InputMaybe<UserCreateWithoutSongsInput>;
};

export type UserCreateNestedOneWithoutTagsInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutTagsInput>;
  create?: InputMaybe<UserCreateWithoutTagsInput>;
};

export type UserCreateOrConnectWithoutLikesInput = {
  create: UserCreateWithoutLikesInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutLyricsInput = {
  create: UserCreateWithoutLyricsInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutRecordingsInput = {
  create: UserCreateWithoutRecordingsInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutScoresInput = {
  create: UserCreateWithoutScoresInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutSongCommentsInput = {
  create: UserCreateWithoutSongCommentsInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutSongOrderTemplatesInput = {
  create: UserCreateWithoutSongOrderTemplatesInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutSongOrdersInput = {
  create: UserCreateWithoutSongOrdersInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutSongsInput = {
  create: UserCreateWithoutSongsInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutTagsInput = {
  create: UserCreateWithoutTagsInput;
  where: UserWhereUniqueInput;
};

export type UserCreateWithoutLikesInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutLyricsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutRecordingsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutScoresInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutSongCommentsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutSongOrderTemplatesInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutSongOrdersInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutSongsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  tags?: InputMaybe<TagCreateNestedManyWithoutUserInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserCreateWithoutTagsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email: Scalars['String'];
  failCount?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isLocked?: InputMaybe<Scalars['Boolean']>;
  likes?: InputMaybe<LikeCreateNestedManyWithoutUserInput>;
  lyrics?: InputMaybe<LyricsCreateNestedManyWithoutCreatorInput>;
  name: Scalars['String'];
  password: Scalars['String'];
  profileImage?: InputMaybe<Scalars['String']>;
  recordings?: InputMaybe<RecordingCreateNestedManyWithoutUserInput>;
  scores?: InputMaybe<ScoreCreateNestedManyWithoutUserInput>;
  songComments?: InputMaybe<SongCommentCreateNestedManyWithoutUserInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateCreateNestedManyWithoutCreatorInput>;
  songOrders?: InputMaybe<SongOrderCreateNestedManyWithoutCreatorInput>;
  songs?: InputMaybe<SongCreateNestedManyWithoutCreatorInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type UserMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  email?: InputMaybe<SortOrder>;
  failCount?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isActive?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  isLocked?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  profileImage?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type UserMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  email?: InputMaybe<SortOrder>;
  failCount?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isActive?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  isLocked?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  profileImage?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type UserOrderByWithAggregationInput = {
  _avg?: InputMaybe<UserAvgOrderByAggregateInput>;
  _count?: InputMaybe<UserCountOrderByAggregateInput>;
  _max?: InputMaybe<UserMaxOrderByAggregateInput>;
  _min?: InputMaybe<UserMinOrderByAggregateInput>;
  _sum?: InputMaybe<UserSumOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  email?: InputMaybe<SortOrder>;
  failCount?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isActive?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  isLocked?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  profileImage?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type UserOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  email?: InputMaybe<SortOrder>;
  failCount?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isActive?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  isLocked?: InputMaybe<SortOrder>;
  likes?: InputMaybe<LikeOrderByRelationAggregateInput>;
  lyrics?: InputMaybe<LyricsOrderByRelationAggregateInput>;
  name?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  profileImage?: InputMaybe<SortOrder>;
  recordings?: InputMaybe<RecordingOrderByRelationAggregateInput>;
  scores?: InputMaybe<ScoreOrderByRelationAggregateInput>;
  songComments?: InputMaybe<SongCommentOrderByRelationAggregateInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateOrderByRelationAggregateInput>;
  songOrders?: InputMaybe<SongOrderOrderByRelationAggregateInput>;
  songs?: InputMaybe<SongOrderByRelationAggregateInput>;
  tags?: InputMaybe<TagOrderByRelationAggregateInput>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type UserRelationFilter = {
  is?: InputMaybe<UserWhereInput>;
  isNot?: InputMaybe<UserWhereInput>;
};

export enum UserScalarFieldEnum {
  CreatedAt = 'createdAt',
  Email = 'email',
  FailCount = 'failCount',
  Id = 'id',
  IsActive = 'isActive',
  IsAdmin = 'isAdmin',
  IsLocked = 'isLocked',
  Name = 'name',
  Password = 'password',
  ProfileImage = 'profileImage',
  UpdatedAt = 'updatedAt'
}

export type UserScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<UserScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<UserScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<UserScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  email?: InputMaybe<StringWithAggregatesFilter>;
  failCount?: InputMaybe<IntWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  isActive?: InputMaybe<BoolWithAggregatesFilter>;
  isAdmin?: InputMaybe<BoolWithAggregatesFilter>;
  isLocked?: InputMaybe<BoolWithAggregatesFilter>;
  name?: InputMaybe<StringWithAggregatesFilter>;
  password?: InputMaybe<StringWithAggregatesFilter>;
  profileImage?: InputMaybe<StringNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
};

export type UserSumOrderByAggregateInput = {
  failCount?: InputMaybe<SortOrder>;
};

export type UserUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateInputWithFile = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<Scalars['Upload']>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateOneRequiredWithoutLikesNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutLikesInput>;
  create?: InputMaybe<UserCreateWithoutLikesInput>;
  update?: InputMaybe<UserUpdateWithoutLikesInput>;
  upsert?: InputMaybe<UserUpsertWithoutLikesInput>;
};

export type UserUpdateOneRequiredWithoutLyricsNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutLyricsInput>;
  create?: InputMaybe<UserCreateWithoutLyricsInput>;
  update?: InputMaybe<UserUpdateWithoutLyricsInput>;
  upsert?: InputMaybe<UserUpsertWithoutLyricsInput>;
};

export type UserUpdateOneRequiredWithoutRecordingsNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutRecordingsInput>;
  create?: InputMaybe<UserCreateWithoutRecordingsInput>;
  update?: InputMaybe<UserUpdateWithoutRecordingsInput>;
  upsert?: InputMaybe<UserUpsertWithoutRecordingsInput>;
};

export type UserUpdateOneRequiredWithoutScoresNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutScoresInput>;
  create?: InputMaybe<UserCreateWithoutScoresInput>;
  update?: InputMaybe<UserUpdateWithoutScoresInput>;
  upsert?: InputMaybe<UserUpsertWithoutScoresInput>;
};

export type UserUpdateOneRequiredWithoutSongCommentsNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongCommentsInput>;
  create?: InputMaybe<UserCreateWithoutSongCommentsInput>;
  update?: InputMaybe<UserUpdateWithoutSongCommentsInput>;
  upsert?: InputMaybe<UserUpsertWithoutSongCommentsInput>;
};

export type UserUpdateOneRequiredWithoutSongOrderTemplatesNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongOrderTemplatesInput>;
  create?: InputMaybe<UserCreateWithoutSongOrderTemplatesInput>;
  update?: InputMaybe<UserUpdateWithoutSongOrderTemplatesInput>;
  upsert?: InputMaybe<UserUpsertWithoutSongOrderTemplatesInput>;
};

export type UserUpdateOneRequiredWithoutSongOrdersNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongOrdersInput>;
  create?: InputMaybe<UserCreateWithoutSongOrdersInput>;
  update?: InputMaybe<UserUpdateWithoutSongOrdersInput>;
  upsert?: InputMaybe<UserUpsertWithoutSongOrdersInput>;
};

export type UserUpdateOneRequiredWithoutSongsNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutSongsInput>;
  create?: InputMaybe<UserCreateWithoutSongsInput>;
  update?: InputMaybe<UserUpdateWithoutSongsInput>;
  upsert?: InputMaybe<UserUpsertWithoutSongsInput>;
};

export type UserUpdateOneWithoutTagsNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutTagsInput>;
  create?: InputMaybe<UserCreateWithoutTagsInput>;
  delete?: InputMaybe<Scalars['Boolean']>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
  update?: InputMaybe<UserUpdateWithoutTagsInput>;
  upsert?: InputMaybe<UserUpsertWithoutTagsInput>;
};

export type UserUpdateWithoutLikesInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutLyricsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutRecordingsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutScoresInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutSongCommentsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutSongOrderTemplatesInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutSongOrdersInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutSongsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  tags?: InputMaybe<TagUpdateManyWithoutUserNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpdateWithoutTagsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  email?: InputMaybe<StringFieldUpdateOperationsInput>;
  failCount?: InputMaybe<IntFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isActive?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  isLocked?: InputMaybe<BoolFieldUpdateOperationsInput>;
  likes?: InputMaybe<LikeUpdateManyWithoutUserNestedInput>;
  lyrics?: InputMaybe<LyricsUpdateManyWithoutCreatorNestedInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  password?: InputMaybe<StringFieldUpdateOperationsInput>;
  profileImage?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  recordings?: InputMaybe<RecordingUpdateManyWithoutUserNestedInput>;
  scores?: InputMaybe<ScoreUpdateManyWithoutUserNestedInput>;
  songComments?: InputMaybe<SongCommentUpdateManyWithoutUserNestedInput>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateUpdateManyWithoutCreatorNestedInput>;
  songOrders?: InputMaybe<SongOrderUpdateManyWithoutCreatorNestedInput>;
  songs?: InputMaybe<SongUpdateManyWithoutCreatorNestedInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type UserUpsertWithoutLikesInput = {
  create: UserCreateWithoutLikesInput;
  update: UserUpdateWithoutLikesInput;
};

export type UserUpsertWithoutLyricsInput = {
  create: UserCreateWithoutLyricsInput;
  update: UserUpdateWithoutLyricsInput;
};

export type UserUpsertWithoutRecordingsInput = {
  create: UserCreateWithoutRecordingsInput;
  update: UserUpdateWithoutRecordingsInput;
};

export type UserUpsertWithoutScoresInput = {
  create: UserCreateWithoutScoresInput;
  update: UserUpdateWithoutScoresInput;
};

export type UserUpsertWithoutSongCommentsInput = {
  create: UserCreateWithoutSongCommentsInput;
  update: UserUpdateWithoutSongCommentsInput;
};

export type UserUpsertWithoutSongOrderTemplatesInput = {
  create: UserCreateWithoutSongOrderTemplatesInput;
  update: UserUpdateWithoutSongOrderTemplatesInput;
};

export type UserUpsertWithoutSongOrdersInput = {
  create: UserCreateWithoutSongOrdersInput;
  update: UserUpdateWithoutSongOrdersInput;
};

export type UserUpsertWithoutSongsInput = {
  create: UserCreateWithoutSongsInput;
  update: UserUpdateWithoutSongsInput;
};

export type UserUpsertWithoutTagsInput = {
  create: UserCreateWithoutTagsInput;
  update: UserUpdateWithoutTagsInput;
};

export type UserWhereInput = {
  AND?: InputMaybe<Array<UserWhereInput>>;
  NOT?: InputMaybe<Array<UserWhereInput>>;
  OR?: InputMaybe<Array<UserWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  email?: InputMaybe<StringFilter>;
  failCount?: InputMaybe<IntFilter>;
  id?: InputMaybe<StringFilter>;
  isActive?: InputMaybe<BoolFilter>;
  isAdmin?: InputMaybe<BoolFilter>;
  isLocked?: InputMaybe<BoolFilter>;
  likes?: InputMaybe<LikeListRelationFilter>;
  lyrics?: InputMaybe<LyricsListRelationFilter>;
  name?: InputMaybe<StringFilter>;
  password?: InputMaybe<StringFilter>;
  profileImage?: InputMaybe<StringNullableFilter>;
  recordings?: InputMaybe<RecordingListRelationFilter>;
  scores?: InputMaybe<ScoreListRelationFilter>;
  songComments?: InputMaybe<SongCommentListRelationFilter>;
  songOrderTemplates?: InputMaybe<SongOrderTemplateListRelationFilter>;
  songOrders?: InputMaybe<SongOrderListRelationFilter>;
  songs?: InputMaybe<SongListRelationFilter>;
  tags?: InputMaybe<TagListRelationFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type UserWhereUniqueInput = {
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type Verse = {
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  lyrics: Lyrics;
  order?: Maybe<Scalars['Int']>;
  text: Scalars['String'];
  type?: Maybe<VerseType>;
  updatedAt: Scalars['DateTime'];
};

export type VerseAvgOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export type VerseCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  lyricsId?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type VerseCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  lyrics: LyricsCreateNestedOneWithoutVersesInput;
  order?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
  type?: InputMaybe<VerseType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type VerseCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  lyricsId: Scalars['String'];
  order?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
  type?: InputMaybe<VerseType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type VerseCreateManyLyricsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
  type?: InputMaybe<VerseType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type VerseCreateManyLyricsInputEnvelope = {
  data: Array<VerseCreateManyLyricsInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']>;
};

export type VerseCreateNestedManyWithoutLyricsInput = {
  connect?: InputMaybe<Array<VerseWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<VerseCreateOrConnectWithoutLyricsInput>>;
  create?: InputMaybe<VerseCreateWithoutLyricsInput>;
  createMany?: InputMaybe<VerseCreateManyLyricsInputEnvelope>;
};

export type VerseCreateOrConnectWithoutLyricsInput = {
  create: VerseCreateWithoutLyricsInput;
  where: VerseWhereUniqueInput;
};

export type VerseCreateWithoutLyricsInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
  type?: InputMaybe<VerseType>;
  updatedAt?: InputMaybe<Scalars['DateTime']>;
};

export type VerseListRelationFilter = {
  every?: InputMaybe<VerseWhereInput>;
  none?: InputMaybe<VerseWhereInput>;
  some?: InputMaybe<VerseWhereInput>;
};

export type VerseMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  lyricsId?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type VerseMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  lyricsId?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type VerseOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type VerseOrderByWithAggregationInput = {
  _avg?: InputMaybe<VerseAvgOrderByAggregateInput>;
  _count?: InputMaybe<VerseCountOrderByAggregateInput>;
  _max?: InputMaybe<VerseMaxOrderByAggregateInput>;
  _min?: InputMaybe<VerseMinOrderByAggregateInput>;
  _sum?: InputMaybe<VerseSumOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  lyricsId?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export type VerseOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  lyrics?: InputMaybe<LyricsOrderByWithRelationInput>;
  lyricsId?: InputMaybe<SortOrder>;
  order?: InputMaybe<SortOrder>;
  text?: InputMaybe<SortOrder>;
  type?: InputMaybe<SortOrder>;
  updatedAt?: InputMaybe<SortOrder>;
};

export enum VerseScalarFieldEnum {
  CreatedAt = 'createdAt',
  Id = 'id',
  LyricsId = 'lyricsId',
  Order = 'order',
  Text = 'text',
  Type = 'type',
  UpdatedAt = 'updatedAt'
}

export type VerseScalarWhereInput = {
  AND?: InputMaybe<Array<VerseScalarWhereInput>>;
  NOT?: InputMaybe<Array<VerseScalarWhereInput>>;
  OR?: InputMaybe<Array<VerseScalarWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  lyricsId?: InputMaybe<StringFilter>;
  order?: InputMaybe<IntNullableFilter>;
  text?: InputMaybe<StringFilter>;
  type?: InputMaybe<EnumVerseTypeNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type VerseScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<VerseScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<VerseScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<VerseScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  lyricsId?: InputMaybe<StringWithAggregatesFilter>;
  order?: InputMaybe<IntNullableWithAggregatesFilter>;
  text?: InputMaybe<StringWithAggregatesFilter>;
  type?: InputMaybe<EnumVerseTypeNullableWithAggregatesFilter>;
  updatedAt?: InputMaybe<DateTimeWithAggregatesFilter>;
};

export type VerseSumOrderByAggregateInput = {
  order?: InputMaybe<SortOrder>;
};

export enum VerseType {
  Bridge = 'Bridge',
  Chorus = 'Chorus',
  Refrain = 'Refrain',
  Verse = 'Verse'
}

export type VerseUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  lyrics?: InputMaybe<LyricsUpdateOneRequiredWithoutVersesNestedInput>;
  order?: InputMaybe<NullableIntFieldUpdateOperationsInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  type?: InputMaybe<NullableEnumVerseTypeFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type VerseUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<NullableIntFieldUpdateOperationsInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  type?: InputMaybe<NullableEnumVerseTypeFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type VerseUpdateManyWithWhereWithoutLyricsInput = {
  data: VerseUpdateManyMutationInput;
  where: VerseScalarWhereInput;
};

export type VerseUpdateManyWithoutLyricsNestedInput = {
  connect?: InputMaybe<Array<VerseWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<VerseCreateOrConnectWithoutLyricsInput>>;
  create?: InputMaybe<VerseCreateWithoutLyricsInput>;
  createMany?: InputMaybe<VerseCreateManyLyricsInputEnvelope>;
  delete?: InputMaybe<Array<VerseWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<VerseScalarWhereInput>>;
  disconnect?: InputMaybe<Array<VerseWhereUniqueInput>>;
  set?: InputMaybe<Array<VerseWhereUniqueInput>>;
  update?: InputMaybe<Array<VerseUpdateWithWhereUniqueWithoutLyricsInput>>;
  updateMany?: InputMaybe<Array<VerseUpdateManyWithWhereWithoutLyricsInput>>;
  upsert?: InputMaybe<Array<VerseUpsertWithWhereUniqueWithoutLyricsInput>>;
};

export type VerseUpdateWithWhereUniqueWithoutLyricsInput = {
  data: VerseUpdateWithoutLyricsInput;
  where: VerseWhereUniqueInput;
};

export type VerseUpdateWithoutLyricsInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  order?: InputMaybe<NullableIntFieldUpdateOperationsInput>;
  text?: InputMaybe<StringFieldUpdateOperationsInput>;
  type?: InputMaybe<NullableEnumVerseTypeFieldUpdateOperationsInput>;
  updatedAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
};

export type VerseUpsertWithWhereUniqueWithoutLyricsInput = {
  create: VerseCreateWithoutLyricsInput;
  update: VerseUpdateWithoutLyricsInput;
  where: VerseWhereUniqueInput;
};

export type VerseWhereInput = {
  AND?: InputMaybe<Array<VerseWhereInput>>;
  NOT?: InputMaybe<Array<VerseWhereInput>>;
  OR?: InputMaybe<Array<VerseWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  lyrics?: InputMaybe<LyricsWhereInput>;
  lyricsId?: InputMaybe<StringFilter>;
  order?: InputMaybe<IntNullableFilter>;
  text?: InputMaybe<StringFilter>;
  type?: InputMaybe<EnumVerseTypeNullableFilter>;
  updatedAt?: InputMaybe<DateTimeFilter>;
};

export type VerseWhereUniqueInput = {
  id?: InputMaybe<Scalars['String']>;
};

export type CreateSlideGroupMutationVariables = Exact<{
  data: SlideGroupCreateInput;
}>;


export type CreateSlideGroupMutation = { createOneSlideGroup: SlideGroupFragment };

export type UpdateSlideGroupMutationVariables = Exact<{
  data: SlideGroupUpdateInput;
  where: SlideGroupWhereUniqueInput;
}>;


export type UpdateSlideGroupMutation = { updateOneSlideGroup?: Maybe<SlideGroupFragment> };

export type DeleteSlideGroupMutationVariables = Exact<{
  where: SlideGroupWhereUniqueInput;
}>;


export type DeleteSlideGroupMutation = { deleteOneSlideGroup?: Maybe<SlideGroupFragment> };

export type InsertSlideGroupMutationVariables = Exact<{
  data: SlideGroupCreateInput;
  whereSongOrder: SongOrderWhereUniqueInput;
}>;


export type InsertSlideGroupMutation = { insertSlideGroup: SlideGroupFragment };

export type SwapSlideGroupsMutationVariables = Exact<{
  groupId1: Scalars['String'];
  groupId2: Scalars['String'];
}>;


export type SwapSlideGroupsMutation = { swapSlideGroups: Array<SlideGroupFragment> };

export type ChangeSongPublicityMutationVariables = Exact<{
  where: SongWhereUniqueInput;
  publicity: Publicity;
}>;


export type ChangeSongPublicityMutation = { changeSongPublicity: SongFragment };

export type ChangeLyricsPublicityMutationVariables = Exact<{
  where: LyricsWhereUniqueInput;
  publicity: Publicity;
}>;


export type ChangeLyricsPublicityMutation = { changeLyricsPublicity: LyricsFragment };

export type ChangeRecordingPublicityMutationVariables = Exact<{
  where: RecordingWhereUniqueInput;
  publicity: Publicity;
}>;


export type ChangeRecordingPublicityMutation = { changeRecordingPublicity: RecordingDetailsFragment };

export type ChangeScorePublicityMutationVariables = Exact<{
  where: ScoreWhereUniqueInput;
  publicity: Publicity;
}>;


export type ChangeScorePublicityMutation = { changeScorePublicity: ScoreDetailsFragment };

export type ChangeTemplatePublicityMutationVariables = Exact<{
  where: SongOrderTemplateWhereUniqueInput;
  publicity: Publicity;
}>;


export type ChangeTemplatePublicityMutation = { changeSongOrderTemplatePublicity: TemplateFragment };

export type ChangeCommentPublicityMutationVariables = Exact<{
  where: SongCommentWhereUniqueInput;
  publicity: Publicity;
}>;


export type ChangeCommentPublicityMutation = { changeCommentPublicity: CommentFragment };

export type SearchScoresQueryVariables = Exact<{
  where?: Maybe<ScoreWhereWithPrivateField>;
  orderBy?: Maybe<Array<ScoreOrderByWithRelationInput> | ScoreOrderByWithRelationInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type SearchScoresQuery = { scoresCount: number, scores: Array<ScoreDetailsFragment> };

export type SearchRecordingsQueryVariables = Exact<{
  where?: Maybe<ScoreWhereWithPrivateField>;
  orderBy?: Maybe<Array<RecordingOrderByWithRelationInput> | RecordingOrderByWithRelationInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type SearchRecordingsQuery = { recordingsCount: number, recordings: Array<RecordingDetailsFragment> };

export type SearchCommentsQueryVariables = Exact<{
  where?: Maybe<SongCommentWhereInput>;
  orderBy?: Maybe<Array<SongCommentOrderByWithRelationInput> | SongCommentOrderByWithRelationInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type SearchCommentsQuery = { commentsCount: number, searchComments: Array<CommentFragment> };

export type SongFragment = { id: string, name: string, writer?: Maybe<string>, createdAt: Date, publishStatus?: Maybe<PublishStatus>, userTags: Array<TagFragment>, creator: UserFragment };

export type SongDetailsFragment = { id: string, createdAt: Date, updatedAt: Date, writer?: Maybe<string>, translator?: Maybe<string>, name: string, creator: UserFragment, userTags: Array<TagFragment>, lyrics: Array<LyricsFragment>, recordings: Array<RecordingDetailsFragment>, scores: Array<ScoreDetailsFragment>, comments: Array<CommentFragment> };

export type VerseFragment = { id: string, text: string, order?: Maybe<number>, type?: Maybe<VerseType> };

export type LyricsFragment = { id: string, creator: UserFragment, verses: Array<VerseFragment>, song?: Maybe<{ id: string }> };

export type LyricsMetaFragment = { id: string, publishStatus?: Maybe<PublishStatus>, createdAt: Date, creator: UserFragment, song?: Maybe<{ id: string, name: string }> };

export type ScoreDetailsFragment = { id: string, url: string, description: string, publishStatus?: Maybe<PublishStatus>, createdAt: Date, user: UserFragment, song: { id: string, name: string } };

export type RecordingDetailsFragment = { id: string, url: string, type?: Maybe<RecordingType>, description: string, publishStatus?: Maybe<PublishStatus>, createdAt: Date, user: UserFragment, song: { id: string, name: string } };

export type CommentFragment = { id: string, text: string, createdAt: Date, publishStatus?: Maybe<PublishStatus>, user: UserFragment, song: { id: string, name: string } };

export type AddLyricsMutationVariables = Exact<{
  songId: Scalars['String'];
  verses?: Maybe<VerseCreateManyLyricsInputEnvelope>;
}>;


export type AddLyricsMutation = { createOneLyrics: LyricsFragment };

export type AddSongMutationVariables = Exact<{
  data: SongCreateInput;
}>;


export type AddSongMutation = { createOneSong: SongFragment };

export type AddTagToSongMutationVariables = Exact<{
  songId: Scalars['String'];
  text: Scalars['String'];
  tagId?: Maybe<Scalars['String']>;
}>;


export type AddTagToSongMutation = { addTagToSong?: Maybe<SongFragment> };

export type DeleteTagFromSongMutationVariables = Exact<{
  tagId: Scalars['String'];
  songId: Scalars['String'];
}>;


export type DeleteTagFromSongMutation = { deleteTagFromSong: SongFragment };

export type UploadScoreMutationVariables = Exact<{
  data: ScoreCreateInputWithFile;
}>;


export type UploadScoreMutation = { createOneScore: ScoreDetailsFragment };

export type UploadRecordingMutationVariables = Exact<{
  data: RecordingCreateInputWithFile;
}>;


export type UploadRecordingMutation = { createOneRecording: RecordingDetailsFragment };

export type SongQueryVariables = Exact<{
  where: SongWhereUniqueInput;
  recordingsTake?: Maybe<Scalars['Int']>;
  recordingsSkip?: Maybe<Scalars['Int']>;
  scoresTake?: Maybe<Scalars['Int']>;
  scoresSkip?: Maybe<Scalars['Int']>;
}>;


export type SongQuery = { song?: Maybe<SongDetailsFragment> };

export type NumberOfPublishedSongAssetsQueryVariables = Exact<{
  songId: Scalars['String'];
}>;


export type NumberOfPublishedSongAssetsQuery = { recordingsCount: number, scoresCount: number };

export type SongWithAllLyricsQueryVariables = Exact<{
  where: SongWhereUniqueInput;
}>;


export type SongWithAllLyricsQuery = { song?: Maybe<{ lyrics: Array<LyricsFragment> }> };

export type LyricsQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type LyricsQuery = { lyrics?: Maybe<LyricsFragment> };

export type SearchSongsQueryVariables = Exact<{
  where?: Maybe<SongWhereInput>;
  orderBy?: Maybe<Array<SongOrderByWithRelationInput> | SongOrderByWithRelationInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type SearchSongsQuery = { publishedSongsCount: number, publishedSongs: Array<SongFragment> };

export type SearchLyricsQueryVariables = Exact<{
  where?: Maybe<LyricsWhereInput>;
  orderBy?: Maybe<Array<LyricsOrderByWithRelationInput> | LyricsOrderByWithRelationInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type SearchLyricsQuery = { songlyricsCount: number, searchLyrics: Array<LyricsMetaFragment> };

export type AddCommentMutationVariables = Exact<{
  data: SongCommentCreateInput;
}>;


export type AddCommentMutation = { createOneSongComment: CommentFragment };

export type SongOrderMetaFragment = { id: string, title?: Maybe<string>, updatedAt: Date };

export type SlideFragment = { id: string, order: number, title?: Maybe<string>, content: string };

export type SlideGroupFragment = { id: string, order: number, title?: Maybe<string>, isSongOrderItem?: Maybe<boolean>, slides: Array<SlideFragment>, song?: Maybe<{ id: string, name: string }> };

export type TemplateFragment = { id: string, title: string, publishStatus?: Maybe<PublishStatus>, createdAt: Date, items: Array<TemplateItemFragment>, creator: UserFragment };

export type TemplateItemFragment = { id: string, order: number, title?: Maybe<string> };

export type SongOrderWithGroupsFragment = { id: string, title?: Maybe<string>, note?: Maybe<string>, presentationConfig: { [key: string]: any }, groups: Array<SlideGroupFragment> };

export type SongOrderFragment = { id: string, title?: Maybe<string> };

export type AddTemplateMutationVariables = Exact<{
  data: SongOrderTemplateCreateInput;
}>;


export type AddTemplateMutation = { createOneSongOrderTemplate: TemplateFragment };

export type AddSongOrderMutationVariables = Exact<{
  data: SongOrderCreateInput;
}>;


export type AddSongOrderMutation = { createOneSongOrder: SongOrderWithGroupsFragment };

export type UpdateSongOrderMutationVariables = Exact<{
  where: SongOrderWhereUniqueInput;
  data: SongOrderUpdateInput;
}>;


export type UpdateSongOrderMutation = { updateOneSongOrder?: Maybe<SongOrderWithGroupsFragment> };

export type UpdateSongOrderNoteMutationVariables = Exact<{
  where: SongOrderWhereUniqueInput;
  note?: Maybe<Scalars['String']>;
}>;


export type UpdateSongOrderNoteMutation = { updateOneSongOrder?: Maybe<{ id: string }> };

export type UpdatePresentationConfigMutationVariables = Exact<{
  where: SongOrderWhereUniqueInput;
  config?: Maybe<Scalars['Json']>;
}>;


export type UpdatePresentationConfigMutation = { updateOneSongOrder?: Maybe<{ id: string }> };

export type SearchSongOrdersQueryVariables = Exact<{
  where?: Maybe<SongOrderWhereInput>;
  orderBy?: Maybe<Array<SongOrderOrderByWithRelationInput> | SongOrderOrderByWithRelationInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type SearchSongOrdersQuery = { userSongOrdersCount: number, userSongOrders: Array<SongOrderMetaFragment> };

export type SearchTemplatesQueryVariables = Exact<{
  where?: Maybe<SongOrderTemplateWhereInput>;
  orderBy?: Maybe<Array<SongOrderTemplateOrderByWithRelationInput> | SongOrderTemplateOrderByWithRelationInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type SearchTemplatesQuery = { userTemplatesCount: number, userTemplates: Array<TemplateFragment> };

export type TemplateQueryVariables = Exact<{
  where: SongOrderTemplateWhereUniqueInput;
}>;


export type TemplateQuery = { songOrderTemplate?: Maybe<TemplateFragment> };

export type SongOrderQueryVariables = Exact<{
  where: SongOrderWhereUniqueInput;
}>;


export type SongOrderQuery = { songOrder?: Maybe<SongOrderWithGroupsFragment> };

export type TagFragment = { id: string, text: string };

export type UserTagsQueryVariables = Exact<{
  where?: Maybe<TagWhereInput>;
  take?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
}>;


export type UserTagsQuery = { userTagsCount: number, userTags: Array<TagFragment> };

export type SignupMutationVariables = Exact<{
  email: Scalars['String'];
  userName: Scalars['String'];
  password: Scalars['String'];
}>;


export type SignupMutation = { signup: AccountFragment };

export type LoginMutationVariables = Exact<{
  emailOrName: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { login: AccountFragment };

export type AccountFragment = { token: string, user?: Maybe<{ id: string, email: string }> };

export type UserFragment = { id: string, profileImage?: Maybe<string>, email: string, name: string };

export type UpdateUserMutationVariables = Exact<{
  data: UserUpdateInputWithFile;
  where: UserWhereUniqueInput;
}>;


export type UpdateUserMutation = { updateOneUser?: Maybe<UserFragment> };

export type GetUserQueryVariables = Exact<{
  where: UserWhereUniqueInput;
}>;


export type GetUserQuery = { user?: Maybe<UserFragment> };

export const TagFragmentDoc = /*#__PURE__*/ gql`
    fragment Tag on Tag {
  id
  text
}
    `;
export const UserFragmentDoc = /*#__PURE__*/ gql`
    fragment User on User {
  id
  profileImage
  email
  name
}
    `;
export const SongFragmentDoc = /*#__PURE__*/ gql`
    fragment Song on Song {
  id
  name
  writer
  createdAt
  userTags {
    ...Tag
  }
  creator {
    ...User
  }
  publishStatus
}
    ${TagFragmentDoc}
${UserFragmentDoc}`;
export const VerseFragmentDoc = /*#__PURE__*/ gql`
    fragment Verse on Verse {
  id
  text
  order
  type
}
    `;
export const LyricsFragmentDoc = /*#__PURE__*/ gql`
    fragment Lyrics on Lyrics {
  id
  creator {
    ...User
  }
  verses(orderBy: [{type: asc}, {order: asc}]) {
    ...Verse
  }
  song {
    id
  }
}
    ${UserFragmentDoc}
${VerseFragmentDoc}`;
export const RecordingDetailsFragmentDoc = /*#__PURE__*/ gql`
    fragment RecordingDetails on Recording {
  id
  url
  type
  description
  user {
    ...User
  }
  song {
    id
    name
  }
  publishStatus
  createdAt
}
    ${UserFragmentDoc}`;
export const ScoreDetailsFragmentDoc = /*#__PURE__*/ gql`
    fragment ScoreDetails on Score {
  id
  url
  description
  user {
    ...User
  }
  song {
    id
    name
  }
  publishStatus
  createdAt
}
    ${UserFragmentDoc}`;
export const CommentFragmentDoc = /*#__PURE__*/ gql`
    fragment Comment on SongComment {
  id
  text
  createdAt
  user {
    ...User
  }
  song {
    id
    name
  }
  publishStatus
}
    ${UserFragmentDoc}`;
export const SongDetailsFragmentDoc = /*#__PURE__*/ gql`
    fragment SongDetails on Song {
  id
  createdAt
  updatedAt
  writer
  translator
  name
  creator {
    ...User
  }
  userTags {
    ...Tag
  }
  lyrics {
    ...Lyrics
  }
  recordings(take: $recordingsTake, skip: $recordingsSkip) {
    ...RecordingDetails
  }
  scores(take: $scoresTake, skip: $scoresSkip) {
    ...ScoreDetails
  }
  comments {
    ...Comment
  }
}
    ${UserFragmentDoc}
${TagFragmentDoc}
${LyricsFragmentDoc}
${RecordingDetailsFragmentDoc}
${ScoreDetailsFragmentDoc}
${CommentFragmentDoc}`;
export const LyricsMetaFragmentDoc = /*#__PURE__*/ gql`
    fragment LyricsMeta on Lyrics {
  id
  creator {
    ...User
  }
  song {
    id
    name
  }
  publishStatus
  createdAt
}
    ${UserFragmentDoc}`;
export const SongOrderMetaFragmentDoc = /*#__PURE__*/ gql`
    fragment SongOrderMeta on SongOrder {
  id
  title
  updatedAt
}
    `;
export const TemplateItemFragmentDoc = /*#__PURE__*/ gql`
    fragment TemplateItem on SongOrderTemplateItem {
  id
  order
  title
}
    `;
export const TemplateFragmentDoc = /*#__PURE__*/ gql`
    fragment Template on SongOrderTemplate {
  id
  title
  publishStatus
  items(orderBy: {order: asc}) {
    ...TemplateItem
  }
  creator {
    ...User
  }
  createdAt
}
    ${TemplateItemFragmentDoc}
${UserFragmentDoc}`;
export const SlideFragmentDoc = /*#__PURE__*/ gql`
    fragment Slide on Slide {
  id
  order
  title
  content
}
    `;
export const SlideGroupFragmentDoc = /*#__PURE__*/ gql`
    fragment SlideGroup on SlideGroup {
  id
  order
  title
  slides(orderBy: {order: asc}) {
    ...Slide
  }
  song {
    id
    name
  }
  isSongOrderItem
}
    ${SlideFragmentDoc}`;
export const SongOrderWithGroupsFragmentDoc = /*#__PURE__*/ gql`
    fragment SongOrderWithGroups on SongOrder {
  id
  title
  note
  presentationConfig
  groups(orderBy: {order: asc}) {
    ...SlideGroup
  }
}
    ${SlideGroupFragmentDoc}`;
export const SongOrderFragmentDoc = /*#__PURE__*/ gql`
    fragment SongOrder on SongOrder {
  id
  title
}
    `;
export const AccountFragmentDoc = /*#__PURE__*/ gql`
    fragment Account on AuthPayload {
  token
  user {
    id
    email
  }
}
    `;
export const CreateSlideGroupDocument = /*#__PURE__*/ gql`
    mutation createSlideGroup($data: SlideGroupCreateInput!) {
  createOneSlideGroup(data: $data) {
    ...SlideGroup
  }
}
    ${SlideGroupFragmentDoc}`;
export type CreateSlideGroupMutationFn = Apollo.MutationFunction<CreateSlideGroupMutation, CreateSlideGroupMutationVariables>;

/**
 * __useCreateSlideGroupMutation__
 *
 * To run a mutation, you first call `useCreateSlideGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSlideGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSlideGroupMutation, { data, loading, error }] = useCreateSlideGroupMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useCreateSlideGroupMutation(baseOptions?: Apollo.MutationHookOptions<CreateSlideGroupMutation, CreateSlideGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateSlideGroupMutation, CreateSlideGroupMutationVariables>(CreateSlideGroupDocument, options);
      }
export type CreateSlideGroupMutationHookResult = ReturnType<typeof useCreateSlideGroupMutation>;
export type CreateSlideGroupMutationResult = Apollo.MutationResult<CreateSlideGroupMutation>;
export type CreateSlideGroupMutationOptions = Apollo.BaseMutationOptions<CreateSlideGroupMutation, CreateSlideGroupMutationVariables>;
export const UpdateSlideGroupDocument = /*#__PURE__*/ gql`
    mutation updateSlideGroup($data: SlideGroupUpdateInput!, $where: SlideGroupWhereUniqueInput!) {
  updateOneSlideGroup(data: $data, where: $where) {
    ...SlideGroup
  }
}
    ${SlideGroupFragmentDoc}`;
export type UpdateSlideGroupMutationFn = Apollo.MutationFunction<UpdateSlideGroupMutation, UpdateSlideGroupMutationVariables>;

/**
 * __useUpdateSlideGroupMutation__
 *
 * To run a mutation, you first call `useUpdateSlideGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSlideGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSlideGroupMutation, { data, loading, error }] = useUpdateSlideGroupMutation({
 *   variables: {
 *      data: // value for 'data'
 *      where: // value for 'where'
 *   },
 * });
 */
export function useUpdateSlideGroupMutation(baseOptions?: Apollo.MutationHookOptions<UpdateSlideGroupMutation, UpdateSlideGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateSlideGroupMutation, UpdateSlideGroupMutationVariables>(UpdateSlideGroupDocument, options);
      }
export type UpdateSlideGroupMutationHookResult = ReturnType<typeof useUpdateSlideGroupMutation>;
export type UpdateSlideGroupMutationResult = Apollo.MutationResult<UpdateSlideGroupMutation>;
export type UpdateSlideGroupMutationOptions = Apollo.BaseMutationOptions<UpdateSlideGroupMutation, UpdateSlideGroupMutationVariables>;
export const DeleteSlideGroupDocument = /*#__PURE__*/ gql`
    mutation deleteSlideGroup($where: SlideGroupWhereUniqueInput!) {
  deleteOneSlideGroup(where: $where) {
    ...SlideGroup
  }
}
    ${SlideGroupFragmentDoc}`;
export type DeleteSlideGroupMutationFn = Apollo.MutationFunction<DeleteSlideGroupMutation, DeleteSlideGroupMutationVariables>;

/**
 * __useDeleteSlideGroupMutation__
 *
 * To run a mutation, you first call `useDeleteSlideGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteSlideGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteSlideGroupMutation, { data, loading, error }] = useDeleteSlideGroupMutation({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useDeleteSlideGroupMutation(baseOptions?: Apollo.MutationHookOptions<DeleteSlideGroupMutation, DeleteSlideGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteSlideGroupMutation, DeleteSlideGroupMutationVariables>(DeleteSlideGroupDocument, options);
      }
export type DeleteSlideGroupMutationHookResult = ReturnType<typeof useDeleteSlideGroupMutation>;
export type DeleteSlideGroupMutationResult = Apollo.MutationResult<DeleteSlideGroupMutation>;
export type DeleteSlideGroupMutationOptions = Apollo.BaseMutationOptions<DeleteSlideGroupMutation, DeleteSlideGroupMutationVariables>;
export const InsertSlideGroupDocument = /*#__PURE__*/ gql`
    mutation insertSlideGroup($data: SlideGroupCreateInput!, $whereSongOrder: SongOrderWhereUniqueInput!) {
  insertSlideGroup(data: $data, whereSongOrder: $whereSongOrder) {
    ...SlideGroup
  }
}
    ${SlideGroupFragmentDoc}`;
export type InsertSlideGroupMutationFn = Apollo.MutationFunction<InsertSlideGroupMutation, InsertSlideGroupMutationVariables>;

/**
 * __useInsertSlideGroupMutation__
 *
 * To run a mutation, you first call `useInsertSlideGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertSlideGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertSlideGroupMutation, { data, loading, error }] = useInsertSlideGroupMutation({
 *   variables: {
 *      data: // value for 'data'
 *      whereSongOrder: // value for 'whereSongOrder'
 *   },
 * });
 */
export function useInsertSlideGroupMutation(baseOptions?: Apollo.MutationHookOptions<InsertSlideGroupMutation, InsertSlideGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertSlideGroupMutation, InsertSlideGroupMutationVariables>(InsertSlideGroupDocument, options);
      }
export type InsertSlideGroupMutationHookResult = ReturnType<typeof useInsertSlideGroupMutation>;
export type InsertSlideGroupMutationResult = Apollo.MutationResult<InsertSlideGroupMutation>;
export type InsertSlideGroupMutationOptions = Apollo.BaseMutationOptions<InsertSlideGroupMutation, InsertSlideGroupMutationVariables>;
export const SwapSlideGroupsDocument = /*#__PURE__*/ gql`
    mutation swapSlideGroups($groupId1: String!, $groupId2: String!) {
  swapSlideGroups(groupId1: $groupId1, groupId2: $groupId2) {
    ...SlideGroup
  }
}
    ${SlideGroupFragmentDoc}`;
export type SwapSlideGroupsMutationFn = Apollo.MutationFunction<SwapSlideGroupsMutation, SwapSlideGroupsMutationVariables>;

/**
 * __useSwapSlideGroupsMutation__
 *
 * To run a mutation, you first call `useSwapSlideGroupsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSwapSlideGroupsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [swapSlideGroupsMutation, { data, loading, error }] = useSwapSlideGroupsMutation({
 *   variables: {
 *      groupId1: // value for 'groupId1'
 *      groupId2: // value for 'groupId2'
 *   },
 * });
 */
export function useSwapSlideGroupsMutation(baseOptions?: Apollo.MutationHookOptions<SwapSlideGroupsMutation, SwapSlideGroupsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SwapSlideGroupsMutation, SwapSlideGroupsMutationVariables>(SwapSlideGroupsDocument, options);
      }
export type SwapSlideGroupsMutationHookResult = ReturnType<typeof useSwapSlideGroupsMutation>;
export type SwapSlideGroupsMutationResult = Apollo.MutationResult<SwapSlideGroupsMutation>;
export type SwapSlideGroupsMutationOptions = Apollo.BaseMutationOptions<SwapSlideGroupsMutation, SwapSlideGroupsMutationVariables>;
export const ChangeSongPublicityDocument = /*#__PURE__*/ gql`
    mutation changeSongPublicity($where: SongWhereUniqueInput!, $publicity: Publicity!) {
  changeSongPublicity(where: $where, publishStatus: $publicity) {
    ...Song
  }
}
    ${SongFragmentDoc}`;
export type ChangeSongPublicityMutationFn = Apollo.MutationFunction<ChangeSongPublicityMutation, ChangeSongPublicityMutationVariables>;

/**
 * __useChangeSongPublicityMutation__
 *
 * To run a mutation, you first call `useChangeSongPublicityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeSongPublicityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeSongPublicityMutation, { data, loading, error }] = useChangeSongPublicityMutation({
 *   variables: {
 *      where: // value for 'where'
 *      publicity: // value for 'publicity'
 *   },
 * });
 */
export function useChangeSongPublicityMutation(baseOptions?: Apollo.MutationHookOptions<ChangeSongPublicityMutation, ChangeSongPublicityMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeSongPublicityMutation, ChangeSongPublicityMutationVariables>(ChangeSongPublicityDocument, options);
      }
export type ChangeSongPublicityMutationHookResult = ReturnType<typeof useChangeSongPublicityMutation>;
export type ChangeSongPublicityMutationResult = Apollo.MutationResult<ChangeSongPublicityMutation>;
export type ChangeSongPublicityMutationOptions = Apollo.BaseMutationOptions<ChangeSongPublicityMutation, ChangeSongPublicityMutationVariables>;
export const ChangeLyricsPublicityDocument = /*#__PURE__*/ gql`
    mutation changeLyricsPublicity($where: LyricsWhereUniqueInput!, $publicity: Publicity!) {
  changeLyricsPublicity(where: $where, publishStatus: $publicity) {
    ...Lyrics
  }
}
    ${LyricsFragmentDoc}`;
export type ChangeLyricsPublicityMutationFn = Apollo.MutationFunction<ChangeLyricsPublicityMutation, ChangeLyricsPublicityMutationVariables>;

/**
 * __useChangeLyricsPublicityMutation__
 *
 * To run a mutation, you first call `useChangeLyricsPublicityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeLyricsPublicityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeLyricsPublicityMutation, { data, loading, error }] = useChangeLyricsPublicityMutation({
 *   variables: {
 *      where: // value for 'where'
 *      publicity: // value for 'publicity'
 *   },
 * });
 */
export function useChangeLyricsPublicityMutation(baseOptions?: Apollo.MutationHookOptions<ChangeLyricsPublicityMutation, ChangeLyricsPublicityMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeLyricsPublicityMutation, ChangeLyricsPublicityMutationVariables>(ChangeLyricsPublicityDocument, options);
      }
export type ChangeLyricsPublicityMutationHookResult = ReturnType<typeof useChangeLyricsPublicityMutation>;
export type ChangeLyricsPublicityMutationResult = Apollo.MutationResult<ChangeLyricsPublicityMutation>;
export type ChangeLyricsPublicityMutationOptions = Apollo.BaseMutationOptions<ChangeLyricsPublicityMutation, ChangeLyricsPublicityMutationVariables>;
export const ChangeRecordingPublicityDocument = /*#__PURE__*/ gql`
    mutation changeRecordingPublicity($where: RecordingWhereUniqueInput!, $publicity: Publicity!) {
  changeRecordingPublicity(where: $where, publishStatus: $publicity) {
    ...RecordingDetails
  }
}
    ${RecordingDetailsFragmentDoc}`;
export type ChangeRecordingPublicityMutationFn = Apollo.MutationFunction<ChangeRecordingPublicityMutation, ChangeRecordingPublicityMutationVariables>;

/**
 * __useChangeRecordingPublicityMutation__
 *
 * To run a mutation, you first call `useChangeRecordingPublicityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeRecordingPublicityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeRecordingPublicityMutation, { data, loading, error }] = useChangeRecordingPublicityMutation({
 *   variables: {
 *      where: // value for 'where'
 *      publicity: // value for 'publicity'
 *   },
 * });
 */
export function useChangeRecordingPublicityMutation(baseOptions?: Apollo.MutationHookOptions<ChangeRecordingPublicityMutation, ChangeRecordingPublicityMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeRecordingPublicityMutation, ChangeRecordingPublicityMutationVariables>(ChangeRecordingPublicityDocument, options);
      }
export type ChangeRecordingPublicityMutationHookResult = ReturnType<typeof useChangeRecordingPublicityMutation>;
export type ChangeRecordingPublicityMutationResult = Apollo.MutationResult<ChangeRecordingPublicityMutation>;
export type ChangeRecordingPublicityMutationOptions = Apollo.BaseMutationOptions<ChangeRecordingPublicityMutation, ChangeRecordingPublicityMutationVariables>;
export const ChangeScorePublicityDocument = /*#__PURE__*/ gql`
    mutation changeScorePublicity($where: ScoreWhereUniqueInput!, $publicity: Publicity!) {
  changeScorePublicity(where: $where, publishStatus: $publicity) {
    ...ScoreDetails
  }
}
    ${ScoreDetailsFragmentDoc}`;
export type ChangeScorePublicityMutationFn = Apollo.MutationFunction<ChangeScorePublicityMutation, ChangeScorePublicityMutationVariables>;

/**
 * __useChangeScorePublicityMutation__
 *
 * To run a mutation, you first call `useChangeScorePublicityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeScorePublicityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeScorePublicityMutation, { data, loading, error }] = useChangeScorePublicityMutation({
 *   variables: {
 *      where: // value for 'where'
 *      publicity: // value for 'publicity'
 *   },
 * });
 */
export function useChangeScorePublicityMutation(baseOptions?: Apollo.MutationHookOptions<ChangeScorePublicityMutation, ChangeScorePublicityMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeScorePublicityMutation, ChangeScorePublicityMutationVariables>(ChangeScorePublicityDocument, options);
      }
export type ChangeScorePublicityMutationHookResult = ReturnType<typeof useChangeScorePublicityMutation>;
export type ChangeScorePublicityMutationResult = Apollo.MutationResult<ChangeScorePublicityMutation>;
export type ChangeScorePublicityMutationOptions = Apollo.BaseMutationOptions<ChangeScorePublicityMutation, ChangeScorePublicityMutationVariables>;
export const ChangeTemplatePublicityDocument = /*#__PURE__*/ gql`
    mutation changeTemplatePublicity($where: SongOrderTemplateWhereUniqueInput!, $publicity: Publicity!) {
  changeSongOrderTemplatePublicity(where: $where, publishStatus: $publicity) {
    ...Template
  }
}
    ${TemplateFragmentDoc}`;
export type ChangeTemplatePublicityMutationFn = Apollo.MutationFunction<ChangeTemplatePublicityMutation, ChangeTemplatePublicityMutationVariables>;

/**
 * __useChangeTemplatePublicityMutation__
 *
 * To run a mutation, you first call `useChangeTemplatePublicityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeTemplatePublicityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeTemplatePublicityMutation, { data, loading, error }] = useChangeTemplatePublicityMutation({
 *   variables: {
 *      where: // value for 'where'
 *      publicity: // value for 'publicity'
 *   },
 * });
 */
export function useChangeTemplatePublicityMutation(baseOptions?: Apollo.MutationHookOptions<ChangeTemplatePublicityMutation, ChangeTemplatePublicityMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeTemplatePublicityMutation, ChangeTemplatePublicityMutationVariables>(ChangeTemplatePublicityDocument, options);
      }
export type ChangeTemplatePublicityMutationHookResult = ReturnType<typeof useChangeTemplatePublicityMutation>;
export type ChangeTemplatePublicityMutationResult = Apollo.MutationResult<ChangeTemplatePublicityMutation>;
export type ChangeTemplatePublicityMutationOptions = Apollo.BaseMutationOptions<ChangeTemplatePublicityMutation, ChangeTemplatePublicityMutationVariables>;
export const ChangeCommentPublicityDocument = /*#__PURE__*/ gql`
    mutation changeCommentPublicity($where: SongCommentWhereUniqueInput!, $publicity: Publicity!) {
  changeCommentPublicity(where: $where, publishStatus: $publicity) {
    ...Comment
  }
}
    ${CommentFragmentDoc}`;
export type ChangeCommentPublicityMutationFn = Apollo.MutationFunction<ChangeCommentPublicityMutation, ChangeCommentPublicityMutationVariables>;

/**
 * __useChangeCommentPublicityMutation__
 *
 * To run a mutation, you first call `useChangeCommentPublicityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeCommentPublicityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeCommentPublicityMutation, { data, loading, error }] = useChangeCommentPublicityMutation({
 *   variables: {
 *      where: // value for 'where'
 *      publicity: // value for 'publicity'
 *   },
 * });
 */
export function useChangeCommentPublicityMutation(baseOptions?: Apollo.MutationHookOptions<ChangeCommentPublicityMutation, ChangeCommentPublicityMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeCommentPublicityMutation, ChangeCommentPublicityMutationVariables>(ChangeCommentPublicityDocument, options);
      }
export type ChangeCommentPublicityMutationHookResult = ReturnType<typeof useChangeCommentPublicityMutation>;
export type ChangeCommentPublicityMutationResult = Apollo.MutationResult<ChangeCommentPublicityMutation>;
export type ChangeCommentPublicityMutationOptions = Apollo.BaseMutationOptions<ChangeCommentPublicityMutation, ChangeCommentPublicityMutationVariables>;
export const SearchScoresDocument = /*#__PURE__*/ gql`
    query searchScores($where: ScoreWhereWithPrivateField, $orderBy: [ScoreOrderByWithRelationInput!], $take: Int, $skip: Int) {
  scores(where: $where, orderBy: $orderBy, take: $take, skip: $skip) {
    ...ScoreDetails
  }
  scoresCount(where: $where)
}
    ${ScoreDetailsFragmentDoc}`;

/**
 * __useSearchScoresQuery__
 *
 * To run a query within a React component, call `useSearchScoresQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchScoresQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchScoresQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useSearchScoresQuery(baseOptions?: Apollo.QueryHookOptions<SearchScoresQuery, SearchScoresQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchScoresQuery, SearchScoresQueryVariables>(SearchScoresDocument, options);
      }
export function useSearchScoresLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchScoresQuery, SearchScoresQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchScoresQuery, SearchScoresQueryVariables>(SearchScoresDocument, options);
        }
export type SearchScoresQueryHookResult = ReturnType<typeof useSearchScoresQuery>;
export type SearchScoresLazyQueryHookResult = ReturnType<typeof useSearchScoresLazyQuery>;
export type SearchScoresQueryResult = Apollo.QueryResult<SearchScoresQuery, SearchScoresQueryVariables>;
export const SearchRecordingsDocument = /*#__PURE__*/ gql`
    query searchRecordings($where: ScoreWhereWithPrivateField, $orderBy: [RecordingOrderByWithRelationInput!], $take: Int, $skip: Int) {
  recordings(where: $where, orderBy: $orderBy, take: $take, skip: $skip) {
    ...RecordingDetails
  }
  recordingsCount(where: $where)
}
    ${RecordingDetailsFragmentDoc}`;

/**
 * __useSearchRecordingsQuery__
 *
 * To run a query within a React component, call `useSearchRecordingsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchRecordingsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchRecordingsQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useSearchRecordingsQuery(baseOptions?: Apollo.QueryHookOptions<SearchRecordingsQuery, SearchRecordingsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchRecordingsQuery, SearchRecordingsQueryVariables>(SearchRecordingsDocument, options);
      }
export function useSearchRecordingsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchRecordingsQuery, SearchRecordingsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchRecordingsQuery, SearchRecordingsQueryVariables>(SearchRecordingsDocument, options);
        }
export type SearchRecordingsQueryHookResult = ReturnType<typeof useSearchRecordingsQuery>;
export type SearchRecordingsLazyQueryHookResult = ReturnType<typeof useSearchRecordingsLazyQuery>;
export type SearchRecordingsQueryResult = Apollo.QueryResult<SearchRecordingsQuery, SearchRecordingsQueryVariables>;
export const SearchCommentsDocument = /*#__PURE__*/ gql`
    query searchComments($where: SongCommentWhereInput, $orderBy: [SongCommentOrderByWithRelationInput!], $take: Int, $skip: Int) {
  searchComments(where: $where, orderBy: $orderBy, take: $take, skip: $skip) {
    ...Comment
  }
  commentsCount(where: $where)
}
    ${CommentFragmentDoc}`;

/**
 * __useSearchCommentsQuery__
 *
 * To run a query within a React component, call `useSearchCommentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchCommentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchCommentsQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useSearchCommentsQuery(baseOptions?: Apollo.QueryHookOptions<SearchCommentsQuery, SearchCommentsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchCommentsQuery, SearchCommentsQueryVariables>(SearchCommentsDocument, options);
      }
export function useSearchCommentsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchCommentsQuery, SearchCommentsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchCommentsQuery, SearchCommentsQueryVariables>(SearchCommentsDocument, options);
        }
export type SearchCommentsQueryHookResult = ReturnType<typeof useSearchCommentsQuery>;
export type SearchCommentsLazyQueryHookResult = ReturnType<typeof useSearchCommentsLazyQuery>;
export type SearchCommentsQueryResult = Apollo.QueryResult<SearchCommentsQuery, SearchCommentsQueryVariables>;
export const AddLyricsDocument = /*#__PURE__*/ gql`
    mutation addLyrics($songId: String!, $verses: VerseCreateManyLyricsInputEnvelope) {
  createOneLyrics(
    data: {song: {connect: {id: $songId}}, verses: {createMany: $verses}}
  ) {
    ...Lyrics
  }
}
    ${LyricsFragmentDoc}`;
export type AddLyricsMutationFn = Apollo.MutationFunction<AddLyricsMutation, AddLyricsMutationVariables>;

/**
 * __useAddLyricsMutation__
 *
 * To run a mutation, you first call `useAddLyricsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddLyricsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addLyricsMutation, { data, loading, error }] = useAddLyricsMutation({
 *   variables: {
 *      songId: // value for 'songId'
 *      verses: // value for 'verses'
 *   },
 * });
 */
export function useAddLyricsMutation(baseOptions?: Apollo.MutationHookOptions<AddLyricsMutation, AddLyricsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddLyricsMutation, AddLyricsMutationVariables>(AddLyricsDocument, options);
      }
export type AddLyricsMutationHookResult = ReturnType<typeof useAddLyricsMutation>;
export type AddLyricsMutationResult = Apollo.MutationResult<AddLyricsMutation>;
export type AddLyricsMutationOptions = Apollo.BaseMutationOptions<AddLyricsMutation, AddLyricsMutationVariables>;
export const AddSongDocument = /*#__PURE__*/ gql`
    mutation addSong($data: SongCreateInput!) {
  createOneSong(data: $data) {
    ...Song
  }
}
    ${SongFragmentDoc}`;
export type AddSongMutationFn = Apollo.MutationFunction<AddSongMutation, AddSongMutationVariables>;

/**
 * __useAddSongMutation__
 *
 * To run a mutation, you first call `useAddSongMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddSongMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addSongMutation, { data, loading, error }] = useAddSongMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useAddSongMutation(baseOptions?: Apollo.MutationHookOptions<AddSongMutation, AddSongMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddSongMutation, AddSongMutationVariables>(AddSongDocument, options);
      }
export type AddSongMutationHookResult = ReturnType<typeof useAddSongMutation>;
export type AddSongMutationResult = Apollo.MutationResult<AddSongMutation>;
export type AddSongMutationOptions = Apollo.BaseMutationOptions<AddSongMutation, AddSongMutationVariables>;
export const AddTagToSongDocument = /*#__PURE__*/ gql`
    mutation addTagToSong($songId: String!, $text: String!, $tagId: String) {
  addTagToSong(songId: $songId, text: $text, tagId: $tagId) {
    ...Song
  }
}
    ${SongFragmentDoc}`;
export type AddTagToSongMutationFn = Apollo.MutationFunction<AddTagToSongMutation, AddTagToSongMutationVariables>;

/**
 * __useAddTagToSongMutation__
 *
 * To run a mutation, you first call `useAddTagToSongMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddTagToSongMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addTagToSongMutation, { data, loading, error }] = useAddTagToSongMutation({
 *   variables: {
 *      songId: // value for 'songId'
 *      text: // value for 'text'
 *      tagId: // value for 'tagId'
 *   },
 * });
 */
export function useAddTagToSongMutation(baseOptions?: Apollo.MutationHookOptions<AddTagToSongMutation, AddTagToSongMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddTagToSongMutation, AddTagToSongMutationVariables>(AddTagToSongDocument, options);
      }
export type AddTagToSongMutationHookResult = ReturnType<typeof useAddTagToSongMutation>;
export type AddTagToSongMutationResult = Apollo.MutationResult<AddTagToSongMutation>;
export type AddTagToSongMutationOptions = Apollo.BaseMutationOptions<AddTagToSongMutation, AddTagToSongMutationVariables>;
export const DeleteTagFromSongDocument = /*#__PURE__*/ gql`
    mutation deleteTagFromSong($tagId: String!, $songId: String!) {
  deleteTagFromSong(tagId: $tagId, songId: $songId) {
    ...Song
  }
}
    ${SongFragmentDoc}`;
export type DeleteTagFromSongMutationFn = Apollo.MutationFunction<DeleteTagFromSongMutation, DeleteTagFromSongMutationVariables>;

/**
 * __useDeleteTagFromSongMutation__
 *
 * To run a mutation, you first call `useDeleteTagFromSongMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteTagFromSongMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteTagFromSongMutation, { data, loading, error }] = useDeleteTagFromSongMutation({
 *   variables: {
 *      tagId: // value for 'tagId'
 *      songId: // value for 'songId'
 *   },
 * });
 */
export function useDeleteTagFromSongMutation(baseOptions?: Apollo.MutationHookOptions<DeleteTagFromSongMutation, DeleteTagFromSongMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteTagFromSongMutation, DeleteTagFromSongMutationVariables>(DeleteTagFromSongDocument, options);
      }
export type DeleteTagFromSongMutationHookResult = ReturnType<typeof useDeleteTagFromSongMutation>;
export type DeleteTagFromSongMutationResult = Apollo.MutationResult<DeleteTagFromSongMutation>;
export type DeleteTagFromSongMutationOptions = Apollo.BaseMutationOptions<DeleteTagFromSongMutation, DeleteTagFromSongMutationVariables>;
export const UploadScoreDocument = /*#__PURE__*/ gql`
    mutation uploadScore($data: ScoreCreateInputWithFile!) {
  createOneScore(data: $data) {
    ...ScoreDetails
  }
}
    ${ScoreDetailsFragmentDoc}`;
export type UploadScoreMutationFn = Apollo.MutationFunction<UploadScoreMutation, UploadScoreMutationVariables>;

/**
 * __useUploadScoreMutation__
 *
 * To run a mutation, you first call `useUploadScoreMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadScoreMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadScoreMutation, { data, loading, error }] = useUploadScoreMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUploadScoreMutation(baseOptions?: Apollo.MutationHookOptions<UploadScoreMutation, UploadScoreMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UploadScoreMutation, UploadScoreMutationVariables>(UploadScoreDocument, options);
      }
export type UploadScoreMutationHookResult = ReturnType<typeof useUploadScoreMutation>;
export type UploadScoreMutationResult = Apollo.MutationResult<UploadScoreMutation>;
export type UploadScoreMutationOptions = Apollo.BaseMutationOptions<UploadScoreMutation, UploadScoreMutationVariables>;
export const UploadRecordingDocument = /*#__PURE__*/ gql`
    mutation uploadRecording($data: RecordingCreateInputWithFile!) {
  createOneRecording(data: $data) {
    ...RecordingDetails
  }
}
    ${RecordingDetailsFragmentDoc}`;
export type UploadRecordingMutationFn = Apollo.MutationFunction<UploadRecordingMutation, UploadRecordingMutationVariables>;

/**
 * __useUploadRecordingMutation__
 *
 * To run a mutation, you first call `useUploadRecordingMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadRecordingMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadRecordingMutation, { data, loading, error }] = useUploadRecordingMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUploadRecordingMutation(baseOptions?: Apollo.MutationHookOptions<UploadRecordingMutation, UploadRecordingMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UploadRecordingMutation, UploadRecordingMutationVariables>(UploadRecordingDocument, options);
      }
export type UploadRecordingMutationHookResult = ReturnType<typeof useUploadRecordingMutation>;
export type UploadRecordingMutationResult = Apollo.MutationResult<UploadRecordingMutation>;
export type UploadRecordingMutationOptions = Apollo.BaseMutationOptions<UploadRecordingMutation, UploadRecordingMutationVariables>;
export const SongDocument = /*#__PURE__*/ gql`
    query song($where: SongWhereUniqueInput!, $recordingsTake: Int, $recordingsSkip: Int, $scoresTake: Int, $scoresSkip: Int) {
  song(where: $where) {
    ...SongDetails
  }
}
    ${SongDetailsFragmentDoc}`;

/**
 * __useSongQuery__
 *
 * To run a query within a React component, call `useSongQuery` and pass it any options that fit your needs.
 * When your component renders, `useSongQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSongQuery({
 *   variables: {
 *      where: // value for 'where'
 *      recordingsTake: // value for 'recordingsTake'
 *      recordingsSkip: // value for 'recordingsSkip'
 *      scoresTake: // value for 'scoresTake'
 *      scoresSkip: // value for 'scoresSkip'
 *   },
 * });
 */
export function useSongQuery(baseOptions: Apollo.QueryHookOptions<SongQuery, SongQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SongQuery, SongQueryVariables>(SongDocument, options);
      }
export function useSongLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SongQuery, SongQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SongQuery, SongQueryVariables>(SongDocument, options);
        }
export type SongQueryHookResult = ReturnType<typeof useSongQuery>;
export type SongLazyQueryHookResult = ReturnType<typeof useSongLazyQuery>;
export type SongQueryResult = Apollo.QueryResult<SongQuery, SongQueryVariables>;
export const NumberOfPublishedSongAssetsDocument = /*#__PURE__*/ gql`
    query numberOfPublishedSongAssets($songId: String!) {
  recordingsCount(where: {songId: {equals: $songId}, private: true})
  scoresCount(where: {songId: {equals: $songId}, private: true})
}
    `;

/**
 * __useNumberOfPublishedSongAssetsQuery__
 *
 * To run a query within a React component, call `useNumberOfPublishedSongAssetsQuery` and pass it any options that fit your needs.
 * When your component renders, `useNumberOfPublishedSongAssetsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useNumberOfPublishedSongAssetsQuery({
 *   variables: {
 *      songId: // value for 'songId'
 *   },
 * });
 */
export function useNumberOfPublishedSongAssetsQuery(baseOptions: Apollo.QueryHookOptions<NumberOfPublishedSongAssetsQuery, NumberOfPublishedSongAssetsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<NumberOfPublishedSongAssetsQuery, NumberOfPublishedSongAssetsQueryVariables>(NumberOfPublishedSongAssetsDocument, options);
      }
export function useNumberOfPublishedSongAssetsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<NumberOfPublishedSongAssetsQuery, NumberOfPublishedSongAssetsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<NumberOfPublishedSongAssetsQuery, NumberOfPublishedSongAssetsQueryVariables>(NumberOfPublishedSongAssetsDocument, options);
        }
export type NumberOfPublishedSongAssetsQueryHookResult = ReturnType<typeof useNumberOfPublishedSongAssetsQuery>;
export type NumberOfPublishedSongAssetsLazyQueryHookResult = ReturnType<typeof useNumberOfPublishedSongAssetsLazyQuery>;
export type NumberOfPublishedSongAssetsQueryResult = Apollo.QueryResult<NumberOfPublishedSongAssetsQuery, NumberOfPublishedSongAssetsQueryVariables>;
export const SongWithAllLyricsDocument = /*#__PURE__*/ gql`
    query songWithAllLyrics($where: SongWhereUniqueInput!) {
  song(where: $where) {
    lyrics {
      ...Lyrics
    }
  }
}
    ${LyricsFragmentDoc}`;

/**
 * __useSongWithAllLyricsQuery__
 *
 * To run a query within a React component, call `useSongWithAllLyricsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSongWithAllLyricsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSongWithAllLyricsQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useSongWithAllLyricsQuery(baseOptions: Apollo.QueryHookOptions<SongWithAllLyricsQuery, SongWithAllLyricsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SongWithAllLyricsQuery, SongWithAllLyricsQueryVariables>(SongWithAllLyricsDocument, options);
      }
export function useSongWithAllLyricsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SongWithAllLyricsQuery, SongWithAllLyricsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SongWithAllLyricsQuery, SongWithAllLyricsQueryVariables>(SongWithAllLyricsDocument, options);
        }
export type SongWithAllLyricsQueryHookResult = ReturnType<typeof useSongWithAllLyricsQuery>;
export type SongWithAllLyricsLazyQueryHookResult = ReturnType<typeof useSongWithAllLyricsLazyQuery>;
export type SongWithAllLyricsQueryResult = Apollo.QueryResult<SongWithAllLyricsQuery, SongWithAllLyricsQueryVariables>;
export const LyricsDocument = /*#__PURE__*/ gql`
    query lyrics($id: String!) {
  lyrics(where: {id: $id}) {
    ...Lyrics
  }
}
    ${LyricsFragmentDoc}`;

/**
 * __useLyricsQuery__
 *
 * To run a query within a React component, call `useLyricsQuery` and pass it any options that fit your needs.
 * When your component renders, `useLyricsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useLyricsQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useLyricsQuery(baseOptions: Apollo.QueryHookOptions<LyricsQuery, LyricsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<LyricsQuery, LyricsQueryVariables>(LyricsDocument, options);
      }
export function useLyricsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<LyricsQuery, LyricsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<LyricsQuery, LyricsQueryVariables>(LyricsDocument, options);
        }
export type LyricsQueryHookResult = ReturnType<typeof useLyricsQuery>;
export type LyricsLazyQueryHookResult = ReturnType<typeof useLyricsLazyQuery>;
export type LyricsQueryResult = Apollo.QueryResult<LyricsQuery, LyricsQueryVariables>;
export const SearchSongsDocument = /*#__PURE__*/ gql`
    query searchSongs($where: SongWhereInput, $orderBy: [SongOrderByWithRelationInput!], $take: Int, $skip: Int) {
  publishedSongs(where: $where, orderBy: $orderBy, take: $take, skip: $skip) {
    ...Song
  }
  publishedSongsCount(where: $where)
}
    ${SongFragmentDoc}`;

/**
 * __useSearchSongsQuery__
 *
 * To run a query within a React component, call `useSearchSongsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchSongsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchSongsQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useSearchSongsQuery(baseOptions?: Apollo.QueryHookOptions<SearchSongsQuery, SearchSongsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchSongsQuery, SearchSongsQueryVariables>(SearchSongsDocument, options);
      }
export function useSearchSongsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchSongsQuery, SearchSongsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchSongsQuery, SearchSongsQueryVariables>(SearchSongsDocument, options);
        }
export type SearchSongsQueryHookResult = ReturnType<typeof useSearchSongsQuery>;
export type SearchSongsLazyQueryHookResult = ReturnType<typeof useSearchSongsLazyQuery>;
export type SearchSongsQueryResult = Apollo.QueryResult<SearchSongsQuery, SearchSongsQueryVariables>;
export const SearchLyricsDocument = /*#__PURE__*/ gql`
    query searchLyrics($where: LyricsWhereInput, $orderBy: [LyricsOrderByWithRelationInput!], $take: Int, $skip: Int) {
  searchLyrics(where: $where, orderBy: $orderBy, take: $take, skip: $skip) {
    ...LyricsMeta
  }
  songlyricsCount(where: $where)
}
    ${LyricsMetaFragmentDoc}`;

/**
 * __useSearchLyricsQuery__
 *
 * To run a query within a React component, call `useSearchLyricsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchLyricsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchLyricsQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useSearchLyricsQuery(baseOptions?: Apollo.QueryHookOptions<SearchLyricsQuery, SearchLyricsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchLyricsQuery, SearchLyricsQueryVariables>(SearchLyricsDocument, options);
      }
export function useSearchLyricsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchLyricsQuery, SearchLyricsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchLyricsQuery, SearchLyricsQueryVariables>(SearchLyricsDocument, options);
        }
export type SearchLyricsQueryHookResult = ReturnType<typeof useSearchLyricsQuery>;
export type SearchLyricsLazyQueryHookResult = ReturnType<typeof useSearchLyricsLazyQuery>;
export type SearchLyricsQueryResult = Apollo.QueryResult<SearchLyricsQuery, SearchLyricsQueryVariables>;
export const AddCommentDocument = /*#__PURE__*/ gql`
    mutation addComment($data: SongCommentCreateInput!) {
  createOneSongComment(data: $data) {
    ...Comment
  }
}
    ${CommentFragmentDoc}`;
export type AddCommentMutationFn = Apollo.MutationFunction<AddCommentMutation, AddCommentMutationVariables>;

/**
 * __useAddCommentMutation__
 *
 * To run a mutation, you first call `useAddCommentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddCommentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addCommentMutation, { data, loading, error }] = useAddCommentMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useAddCommentMutation(baseOptions?: Apollo.MutationHookOptions<AddCommentMutation, AddCommentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddCommentMutation, AddCommentMutationVariables>(AddCommentDocument, options);
      }
export type AddCommentMutationHookResult = ReturnType<typeof useAddCommentMutation>;
export type AddCommentMutationResult = Apollo.MutationResult<AddCommentMutation>;
export type AddCommentMutationOptions = Apollo.BaseMutationOptions<AddCommentMutation, AddCommentMutationVariables>;
export const AddTemplateDocument = /*#__PURE__*/ gql`
    mutation addTemplate($data: SongOrderTemplateCreateInput!) {
  createOneSongOrderTemplate(data: $data) {
    ...Template
  }
}
    ${TemplateFragmentDoc}`;
export type AddTemplateMutationFn = Apollo.MutationFunction<AddTemplateMutation, AddTemplateMutationVariables>;

/**
 * __useAddTemplateMutation__
 *
 * To run a mutation, you first call `useAddTemplateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddTemplateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addTemplateMutation, { data, loading, error }] = useAddTemplateMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useAddTemplateMutation(baseOptions?: Apollo.MutationHookOptions<AddTemplateMutation, AddTemplateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddTemplateMutation, AddTemplateMutationVariables>(AddTemplateDocument, options);
      }
export type AddTemplateMutationHookResult = ReturnType<typeof useAddTemplateMutation>;
export type AddTemplateMutationResult = Apollo.MutationResult<AddTemplateMutation>;
export type AddTemplateMutationOptions = Apollo.BaseMutationOptions<AddTemplateMutation, AddTemplateMutationVariables>;
export const AddSongOrderDocument = /*#__PURE__*/ gql`
    mutation addSongOrder($data: SongOrderCreateInput!) {
  createOneSongOrder(data: $data) {
    ...SongOrderWithGroups
  }
}
    ${SongOrderWithGroupsFragmentDoc}`;
export type AddSongOrderMutationFn = Apollo.MutationFunction<AddSongOrderMutation, AddSongOrderMutationVariables>;

/**
 * __useAddSongOrderMutation__
 *
 * To run a mutation, you first call `useAddSongOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddSongOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addSongOrderMutation, { data, loading, error }] = useAddSongOrderMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useAddSongOrderMutation(baseOptions?: Apollo.MutationHookOptions<AddSongOrderMutation, AddSongOrderMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddSongOrderMutation, AddSongOrderMutationVariables>(AddSongOrderDocument, options);
      }
export type AddSongOrderMutationHookResult = ReturnType<typeof useAddSongOrderMutation>;
export type AddSongOrderMutationResult = Apollo.MutationResult<AddSongOrderMutation>;
export type AddSongOrderMutationOptions = Apollo.BaseMutationOptions<AddSongOrderMutation, AddSongOrderMutationVariables>;
export const UpdateSongOrderDocument = /*#__PURE__*/ gql`
    mutation updateSongOrder($where: SongOrderWhereUniqueInput!, $data: SongOrderUpdateInput!) {
  updateOneSongOrder(where: $where, data: $data) {
    ...SongOrderWithGroups
  }
}
    ${SongOrderWithGroupsFragmentDoc}`;
export type UpdateSongOrderMutationFn = Apollo.MutationFunction<UpdateSongOrderMutation, UpdateSongOrderMutationVariables>;

/**
 * __useUpdateSongOrderMutation__
 *
 * To run a mutation, you first call `useUpdateSongOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSongOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSongOrderMutation, { data, loading, error }] = useUpdateSongOrderMutation({
 *   variables: {
 *      where: // value for 'where'
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUpdateSongOrderMutation(baseOptions?: Apollo.MutationHookOptions<UpdateSongOrderMutation, UpdateSongOrderMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateSongOrderMutation, UpdateSongOrderMutationVariables>(UpdateSongOrderDocument, options);
      }
export type UpdateSongOrderMutationHookResult = ReturnType<typeof useUpdateSongOrderMutation>;
export type UpdateSongOrderMutationResult = Apollo.MutationResult<UpdateSongOrderMutation>;
export type UpdateSongOrderMutationOptions = Apollo.BaseMutationOptions<UpdateSongOrderMutation, UpdateSongOrderMutationVariables>;
export const UpdateSongOrderNoteDocument = /*#__PURE__*/ gql`
    mutation updateSongOrderNote($where: SongOrderWhereUniqueInput!, $note: String) {
  updateOneSongOrder(where: $where, data: {note: {set: $note}}) {
    id
  }
}
    `;
export type UpdateSongOrderNoteMutationFn = Apollo.MutationFunction<UpdateSongOrderNoteMutation, UpdateSongOrderNoteMutationVariables>;

/**
 * __useUpdateSongOrderNoteMutation__
 *
 * To run a mutation, you first call `useUpdateSongOrderNoteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSongOrderNoteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSongOrderNoteMutation, { data, loading, error }] = useUpdateSongOrderNoteMutation({
 *   variables: {
 *      where: // value for 'where'
 *      note: // value for 'note'
 *   },
 * });
 */
export function useUpdateSongOrderNoteMutation(baseOptions?: Apollo.MutationHookOptions<UpdateSongOrderNoteMutation, UpdateSongOrderNoteMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateSongOrderNoteMutation, UpdateSongOrderNoteMutationVariables>(UpdateSongOrderNoteDocument, options);
      }
export type UpdateSongOrderNoteMutationHookResult = ReturnType<typeof useUpdateSongOrderNoteMutation>;
export type UpdateSongOrderNoteMutationResult = Apollo.MutationResult<UpdateSongOrderNoteMutation>;
export type UpdateSongOrderNoteMutationOptions = Apollo.BaseMutationOptions<UpdateSongOrderNoteMutation, UpdateSongOrderNoteMutationVariables>;
export const UpdatePresentationConfigDocument = /*#__PURE__*/ gql`
    mutation updatePresentationConfig($where: SongOrderWhereUniqueInput!, $config: Json) {
  updateOneSongOrder(where: $where, data: {presentationConfig: $config}) {
    id
  }
}
    `;
export type UpdatePresentationConfigMutationFn = Apollo.MutationFunction<UpdatePresentationConfigMutation, UpdatePresentationConfigMutationVariables>;

/**
 * __useUpdatePresentationConfigMutation__
 *
 * To run a mutation, you first call `useUpdatePresentationConfigMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePresentationConfigMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePresentationConfigMutation, { data, loading, error }] = useUpdatePresentationConfigMutation({
 *   variables: {
 *      where: // value for 'where'
 *      config: // value for 'config'
 *   },
 * });
 */
export function useUpdatePresentationConfigMutation(baseOptions?: Apollo.MutationHookOptions<UpdatePresentationConfigMutation, UpdatePresentationConfigMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdatePresentationConfigMutation, UpdatePresentationConfigMutationVariables>(UpdatePresentationConfigDocument, options);
      }
export type UpdatePresentationConfigMutationHookResult = ReturnType<typeof useUpdatePresentationConfigMutation>;
export type UpdatePresentationConfigMutationResult = Apollo.MutationResult<UpdatePresentationConfigMutation>;
export type UpdatePresentationConfigMutationOptions = Apollo.BaseMutationOptions<UpdatePresentationConfigMutation, UpdatePresentationConfigMutationVariables>;
export const SearchSongOrdersDocument = /*#__PURE__*/ gql`
    query searchSongOrders($where: SongOrderWhereInput, $orderBy: [SongOrderOrderByWithRelationInput!], $take: Int, $skip: Int) {
  userSongOrders(where: $where, orderBy: $orderBy, take: $take, skip: $skip) {
    ...SongOrderMeta
  }
  userSongOrdersCount(where: $where)
}
    ${SongOrderMetaFragmentDoc}`;

/**
 * __useSearchSongOrdersQuery__
 *
 * To run a query within a React component, call `useSearchSongOrdersQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchSongOrdersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchSongOrdersQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useSearchSongOrdersQuery(baseOptions?: Apollo.QueryHookOptions<SearchSongOrdersQuery, SearchSongOrdersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchSongOrdersQuery, SearchSongOrdersQueryVariables>(SearchSongOrdersDocument, options);
      }
export function useSearchSongOrdersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchSongOrdersQuery, SearchSongOrdersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchSongOrdersQuery, SearchSongOrdersQueryVariables>(SearchSongOrdersDocument, options);
        }
export type SearchSongOrdersQueryHookResult = ReturnType<typeof useSearchSongOrdersQuery>;
export type SearchSongOrdersLazyQueryHookResult = ReturnType<typeof useSearchSongOrdersLazyQuery>;
export type SearchSongOrdersQueryResult = Apollo.QueryResult<SearchSongOrdersQuery, SearchSongOrdersQueryVariables>;
export const SearchTemplatesDocument = /*#__PURE__*/ gql`
    query searchTemplates($where: SongOrderTemplateWhereInput, $orderBy: [SongOrderTemplateOrderByWithRelationInput!], $take: Int, $skip: Int) {
  userTemplates(where: $where, orderBy: $orderBy, take: $take, skip: $skip) {
    ...Template
  }
  userTemplatesCount(where: $where)
}
    ${TemplateFragmentDoc}`;

/**
 * __useSearchTemplatesQuery__
 *
 * To run a query within a React component, call `useSearchTemplatesQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchTemplatesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchTemplatesQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useSearchTemplatesQuery(baseOptions?: Apollo.QueryHookOptions<SearchTemplatesQuery, SearchTemplatesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchTemplatesQuery, SearchTemplatesQueryVariables>(SearchTemplatesDocument, options);
      }
export function useSearchTemplatesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchTemplatesQuery, SearchTemplatesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchTemplatesQuery, SearchTemplatesQueryVariables>(SearchTemplatesDocument, options);
        }
export type SearchTemplatesQueryHookResult = ReturnType<typeof useSearchTemplatesQuery>;
export type SearchTemplatesLazyQueryHookResult = ReturnType<typeof useSearchTemplatesLazyQuery>;
export type SearchTemplatesQueryResult = Apollo.QueryResult<SearchTemplatesQuery, SearchTemplatesQueryVariables>;
export const TemplateDocument = /*#__PURE__*/ gql`
    query template($where: SongOrderTemplateWhereUniqueInput!) {
  songOrderTemplate(where: $where) {
    ...Template
  }
}
    ${TemplateFragmentDoc}`;

/**
 * __useTemplateQuery__
 *
 * To run a query within a React component, call `useTemplateQuery` and pass it any options that fit your needs.
 * When your component renders, `useTemplateQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTemplateQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useTemplateQuery(baseOptions: Apollo.QueryHookOptions<TemplateQuery, TemplateQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TemplateQuery, TemplateQueryVariables>(TemplateDocument, options);
      }
export function useTemplateLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TemplateQuery, TemplateQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TemplateQuery, TemplateQueryVariables>(TemplateDocument, options);
        }
export type TemplateQueryHookResult = ReturnType<typeof useTemplateQuery>;
export type TemplateLazyQueryHookResult = ReturnType<typeof useTemplateLazyQuery>;
export type TemplateQueryResult = Apollo.QueryResult<TemplateQuery, TemplateQueryVariables>;
export const SongOrderDocument = /*#__PURE__*/ gql`
    query songOrder($where: SongOrderWhereUniqueInput!) {
  songOrder(where: $where) {
    ...SongOrderWithGroups
  }
}
    ${SongOrderWithGroupsFragmentDoc}`;

/**
 * __useSongOrderQuery__
 *
 * To run a query within a React component, call `useSongOrderQuery` and pass it any options that fit your needs.
 * When your component renders, `useSongOrderQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSongOrderQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useSongOrderQuery(baseOptions: Apollo.QueryHookOptions<SongOrderQuery, SongOrderQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SongOrderQuery, SongOrderQueryVariables>(SongOrderDocument, options);
      }
export function useSongOrderLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SongOrderQuery, SongOrderQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SongOrderQuery, SongOrderQueryVariables>(SongOrderDocument, options);
        }
export type SongOrderQueryHookResult = ReturnType<typeof useSongOrderQuery>;
export type SongOrderLazyQueryHookResult = ReturnType<typeof useSongOrderLazyQuery>;
export type SongOrderQueryResult = Apollo.QueryResult<SongOrderQuery, SongOrderQueryVariables>;
export const UserTagsDocument = /*#__PURE__*/ gql`
    query userTags($where: TagWhereInput, $take: Int, $skip: Int) {
  userTags(where: $where, take: $take, skip: $skip) {
    ...Tag
  }
  userTagsCount(where: $where)
}
    ${TagFragmentDoc}`;

/**
 * __useUserTagsQuery__
 *
 * To run a query within a React component, call `useUserTagsQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserTagsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserTagsQuery({
 *   variables: {
 *      where: // value for 'where'
 *      take: // value for 'take'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useUserTagsQuery(baseOptions?: Apollo.QueryHookOptions<UserTagsQuery, UserTagsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserTagsQuery, UserTagsQueryVariables>(UserTagsDocument, options);
      }
export function useUserTagsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserTagsQuery, UserTagsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserTagsQuery, UserTagsQueryVariables>(UserTagsDocument, options);
        }
export type UserTagsQueryHookResult = ReturnType<typeof useUserTagsQuery>;
export type UserTagsLazyQueryHookResult = ReturnType<typeof useUserTagsLazyQuery>;
export type UserTagsQueryResult = Apollo.QueryResult<UserTagsQuery, UserTagsQueryVariables>;
export const SignupDocument = /*#__PURE__*/ gql`
    mutation signup($email: String!, $userName: String!, $password: String!) {
  signup(email: $email, userName: $userName, password: $password) {
    ...Account
  }
}
    ${AccountFragmentDoc}`;
export type SignupMutationFn = Apollo.MutationFunction<SignupMutation, SignupMutationVariables>;

/**
 * __useSignupMutation__
 *
 * To run a mutation, you first call `useSignupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signupMutation, { data, loading, error }] = useSignupMutation({
 *   variables: {
 *      email: // value for 'email'
 *      userName: // value for 'userName'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignupMutation(baseOptions?: Apollo.MutationHookOptions<SignupMutation, SignupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SignupMutation, SignupMutationVariables>(SignupDocument, options);
      }
export type SignupMutationHookResult = ReturnType<typeof useSignupMutation>;
export type SignupMutationResult = Apollo.MutationResult<SignupMutation>;
export type SignupMutationOptions = Apollo.BaseMutationOptions<SignupMutation, SignupMutationVariables>;
export const LoginDocument = /*#__PURE__*/ gql`
    mutation login($emailOrName: String!, $password: String!) {
  login(emailOrName: $emailOrName, password: $password) {
    ...Account
  }
}
    ${AccountFragmentDoc}`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      emailOrName: // value for 'emailOrName'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const UpdateUserDocument = /*#__PURE__*/ gql`
    mutation updateUser($data: UserUpdateInputWithFile!, $where: UserWhereUniqueInput!) {
  updateOneUser(data: $data, where: $where) {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type UpdateUserMutationFn = Apollo.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      data: // value for 'data'
 *      where: // value for 'where'
 *   },
 * });
 */
export function useUpdateUserMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, options);
      }
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<UpdateUserMutation, UpdateUserMutationVariables>;
export const GetUserDocument = /*#__PURE__*/ gql`
    query getUser($where: UserWhereUniqueInput!) {
  user(where: $where) {
    ...User
  }
}
    ${UserFragmentDoc}`;

/**
 * __useGetUserQuery__
 *
 * To run a query within a React component, call `useGetUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useGetUserQuery(baseOptions: Apollo.QueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, options);
      }
export function useGetUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, options);
        }
export type GetUserQueryHookResult = ReturnType<typeof useGetUserQuery>;
export type GetUserLazyQueryHookResult = ReturnType<typeof useGetUserLazyQuery>;
export type GetUserQueryResult = Apollo.QueryResult<GetUserQuery, GetUserQueryVariables>;