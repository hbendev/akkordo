import { useEffect, useState } from 'react'
import type { AppContext, AppInitialProps } from 'next/app'

import 'util/dayJsSetup'

import { ApolloProvider } from '@apollo/client'
import { initializeApollo, useApollo } from 'apolloClient'
import { ClickToComponent } from 'click-to-react-component'
import Layout from 'components/Layout'
import { UserContext } from 'contexts/UserContext'
import { useGetUserLazyQuery, UserFragment } from 'generated/apollo-components'
import { CookieChangeListener } from 'universal-cookie'
import { getUser } from 'util/auth'
import { cookieManager, JWTPayload } from 'util/jwt'

import '../styles/globals.css'

function MyApp({ Component, pageProps, ctx }: AppContext & AppInitialProps) {
  const { client } = useApollo(pageProps, ctx)
  const [jwtPayload, setJwtPayload] = useState<JWTPayload | null>(null)
  const [user, setUser] = useState<UserFragment | null>(null)

  const [getUserQuery] = useGetUserLazyQuery({
    client: client || initializeApollo(),
    ssr: false,
    fetchPolicy: 'cache-and-network',
    onCompleted: (data) => {
      setUser(data.user || null)
    }
  })

  useEffect(() => {
    const userFromJwt = getUser()
    setJwtPayload(userFromJwt)

    if (userFromJwt.isLoggedIn) {
      getUserQuery({ variables: { where: { id: userFromJwt.id } } })
    }

    const onCookieChange: CookieChangeListener = ({ name, value }) => {
      if (name === 'token') {
        if (!value) {
          // user logged out
          setUser(null)
          setJwtPayload(null)

          return
        }

        const newUserFromJwt = getUser(ctx)
        setJwtPayload(newUserFromJwt)

        if (newUserFromJwt.isLoggedIn) {
          getUserQuery({ variables: { where: { id: newUserFromJwt.id } } })
        }
      }
    }

    cookieManager.addChangeListener((...args) => {
      onCookieChange(...args)
    })
    return cookieManager.removeChangeListener(onCookieChange)
  }, [ctx, setJwtPayload, getUserQuery])

  // We need to wait for the client side cache to be restored before rendering
  // the application
  if (!client) {
    return (
      <UserContext.Provider value={user}>
        <Layout loggedIn={jwtPayload?.isLoggedIn || false} isAdmin={jwtPayload?.isAdmin}>
          <div className="flex items-center justify-center h-full">
            <h2 className="text-xl">Betöltés..</h2>
          </div>
        </Layout>
      </UserContext.Provider>
    )
  }

  return (
    <ApolloProvider client={client}>
      <ClickToComponent />
      <UserContext.Provider value={user}>
        <Layout loggedIn={jwtPayload?.isLoggedIn || false} isAdmin={jwtPayload?.isAdmin}>
          <Component {...pageProps} />
        </Layout>
        <div id="modal-root" />
      </UserContext.Provider>
    </ApolloProvider>
  )
}
export default MyApp
