import React from 'react'

import clsx from 'clsx'
import LoginForm from 'components/auth/LoginForm'
import CommonHead from 'components/CommonHead'

export default function LoginPage() {
  return (
    <div className={clsx('flex justify-center items-center h-screen')}>
      <CommonHead pageName="Belépés" />
      <LoginForm />
    </div>
  )
}
