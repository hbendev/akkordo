import CommonHead from 'components/CommonHead'

export default function OfflinePage() {
  return (
    <div className="flex justify-center items-center h-full space-y-4 flex-col">
      <CommonHead pageName="Offline" />
      <h1 className="text-center text-xl">
        Sajnos jelenleg offline vagy, ezért nem éred el ezt az oldalt.
      </h1>
    </div>
  )
}
