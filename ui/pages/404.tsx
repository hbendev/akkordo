import { TiHome } from 'react-icons/ti'
import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import Button from 'components/ui/Button'

export default function Custom404() {
  const router = useRouter()
  return (
    <div className="flex justify-center items-center h-full space-y-4 flex-col">
      <CommonHead pageName="Ismeretlen oldal" />
      <h1 className="text-center text-xl">404 - A keresett oldal nem található</h1>
      <Button
        className="flex items-center space-x-4"
        color="gold"
        onClick={() => {
          router.replace('/')
        }}
      >
        <span>Ugrás a főoldalra</span>
        <TiHome />
      </Button>
    </div>
  )
}
