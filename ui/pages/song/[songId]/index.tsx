import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import SongDetails from 'components/SongDetails'
import { numberOfAssetsPerPage } from 'components/SongDetails/constants'
import { useNumberOfPublishedSongAssetsQuery, useSongQuery } from 'generated/apollo-components'
import useUser from 'hooks/useUser'

const Song = () => {
  const router = useRouter()
  const user = useUser()
  const { data, loading, variables, refetch } = useSongQuery({
    variables: {
      where: { id: router.query.songId as string },
      recordingsTake: numberOfAssetsPerPage,
      scoresTake: numberOfAssetsPerPage
    },
    skip: !router.isReady
  })

  const { data: assetNumber } = useNumberOfPublishedSongAssetsQuery({
    variables: { songId: router.query.songId as string },
    skip: !router.isReady
  })

  if (loading || !data || !data.song) {
    return null
  }

  return (
    <>
      <CommonHead pageName={data.song.name} />
      <SongDetails
        song={data.song}
        onFetchMoreRecordings={() => {
          refetch({ ...variables, recordingsTake: (variables?.recordingsTake ?? 0) + 6 })
        }}
        onFetchMoreScores={() => {
          refetch({ ...variables, scoresTake: (variables?.scoresTake ?? 0) + 6 })
        }}
        numberOfScores={assetNumber?.scoresCount}
        numberOfRecordings={assetNumber?.recordingsCount}
        hideAddComment={!user?.id}
        hideAddTag={!user?.id}
        disableUpload={!user?.id}
        disableLyricsAdd={!user?.id}
      />
    </>
  )
}

export default Song
