import CommonHead from 'components/CommonHead'
import SongTable from 'components/Table/SongTable'
import Button from 'components/ui/Button'
import Link from 'components/ui/Link'
import useUser from 'hooks/useUser'

const Songs = () => {
  const user = useUser()
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Énekek" />
      <SongTable />
      <Link href="/song/new" passHref={false} disabled={!user?.id}>
        <Button color="gold" className="mt-4" disabled={!user?.id}>
          Új ének
        </Button>
      </Link>
    </div>
  )
}

export default Songs
