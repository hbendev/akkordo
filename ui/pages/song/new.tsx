import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import SongForm from 'components/SongForm'
import { useAddSongMutation } from 'generated/apollo-components'

const NewSong = () => {
  const router = useRouter()
  const [addSong] = useAddSongMutation({
    onCompleted: (data) => {
      router.push(`/song/${data.createOneSong.id}`)
    }
  })

  return (
    <div className="container mx-auto">
      <CommonHead pageName="Új ének" />
      <SongForm onSubmit={(data) => addSong({ variables: { data } })} />
    </div>
  )
}

export default NewSong
