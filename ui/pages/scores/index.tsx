import CommonHead from 'components/CommonHead'
import ScoreTable from 'components/Table/ScoreTable'

const Scores = () => {
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Kották" />
      <ScoreTable />
    </div>
  )
}

export default Scores
