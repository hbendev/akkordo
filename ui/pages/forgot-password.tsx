import CommonHead from 'components/CommonHead'

export default function ForgotPassword() {
  return (
    <div className="flex justify-center items-center h-full space-y-4 flex-col">
      <CommonHead pageName="Elfelejtett jelszó" />
      <h1 className="text-center text-xl">Elfelejtett jelszó</h1>
      <p className="text-center">
        Kérlek arról a címről, amellyel az oldalon vagy regisztrálva küldj egy levelet erre a címre
        az oldal fenntartójának: {process.env.NEXT_PUBLIC_MAINTAINER_EMAIL}
      </p>
    </div>
  )
}
