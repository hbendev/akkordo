import CommonHead from 'components/CommonHead'
import CommentTable from 'components/Table/CommentTable'

const Comments = () => {
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Kommentek" />

      <CommentTable />
    </div>
  )
}

export default Comments
