import { Meta } from '@storybook/react'
import { mockUser } from 'components/Profile/mocks'

import Component from './UserIcon'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'UserIcon'
}
export default meta

const template = storyTemplate(Component)

export const Default = template({
  user: mockUser
})
export const NoImage = template({
  user: { ...mockUser, profileImage: undefined }
})
