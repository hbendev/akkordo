import { Meta } from '@storybook/react'

import Component from './Comment'
import { mockComment } from './mocks'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'Comment'
}
export default meta

const template = storyTemplate(Component)

export const Comment = template({ comment: mockComment })
