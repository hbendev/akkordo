import React from 'react'

import clsx from 'clsx'
import Link from 'components/ui/Link'
import UserIcon from 'components/UserIcon'
import useUser from 'hooks/useUser'

type HeaderProps = {
  onLogout: () => void
  loggedIn: boolean
  className?: string
  isAdmin?: boolean
}

export const height = 'h-16'

export default function Header({ onLogout = () => {}, loggedIn, isAdmin, className }: HeaderProps) {
  const user = useUser()
  return (
    <nav
      className={clsx(
        'px-4 flex justify-between bg-indigo-100 h-16 border-b-1 border-gold-300 w-full text-purple-700 overflow-x-auto',
        height,
        className
      )}
    >
      <ul className="flex items-center space-x-4 flex-1 mr-auto">
        <li className="hover:text-purple-800">
          <Link href="/song">Énekek</Link>
        </li>
        {loggedIn && (
          <>
            <li className="hover:text-purple-800">
              <Link href="/songorder">Énekrendjeim</Link>
            </li>
            <li className="hover:text-purple-800">
              <Link href="/templates">Sablonjaim</Link>
            </li>
          </>
        )}
        {loggedIn && isAdmin && (
          <>
            <li className="hover:text-purple-800">
              <Link href="/recordings">Felvételek</Link>
            </li>
            <li className="hover:text-purple-800">
              <Link href="/scores">Kották</Link>
            </li>
            <li className="hover:text-purple-800">
              <Link href="/lyrics">Szövegek</Link>
            </li>
            <li className="hover:text-purple-800">
              <Link href="/comments">Hozzászólások</Link>
            </li>
          </>
        )}
      </ul>

      <ul className="flex items-center">
        <li>
          <h1 className="pl-8 lg:pl-0 text-purple-800 font-bold">
            <Link href="/" className="text-md">
              Akkordo
            </Link>
          </h1>
        </li>
      </ul>

      {!loggedIn && (
        <ul className="flex items-center space-x-4 flex-1 justify-end ml-auto">
          <li className="hover:text-purple-800">
            <Link href="/register">Regisztráció</Link>
          </li>
          <li className="hover:text-purple-800">
            <Link href="/login">Belépés</Link>
          </li>
        </ul>
      )}

      {loggedIn && (
        <ul className="flex items-center flex-1 justify-end ml-auto space-x-2 text-purple-700">
          <li className="hover:text-purple-800 relative">
            <Link href="/profile" className="block">
              {user && <UserIcon className="hover:text-purple-800" user={user} small />}
              <span className="sr-only">Profil</span>
            </Link>
          </li>
          <li
            className="cursor-pointer text-indigo-500 hover:text-indigo-800 text-sm"
            onClick={async () => {
              await onLogout()
            }}
          >
            Kilépés
          </li>
        </ul>
      )}
    </nav>
  )
}
