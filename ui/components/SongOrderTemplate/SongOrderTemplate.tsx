import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { FiEdit } from 'react-icons/fi'
import { TiPlus } from 'react-icons/ti'
import Link from 'next/link'

import Button from 'components/ui/Button'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import { TemplateFragment } from 'generated/apollo-components'
import { noop } from 'lodash'

type SongOrderTemplateProps = {
  template: TemplateFragment
  editable?: boolean
  onEdit?: (newTitle: string) => void
}

export default function SongOrderTemplate({
  template,
  editable = false,
  onEdit = noop
}: SongOrderTemplateProps) {
  const { register, handleSubmit } = useForm<{ title: string }>({
    defaultValues: { title: template.title }
  })

  const [editing, setEditing] = useState<boolean>(false)

  const onSubmit = handleSubmit(async (data) => {
    onEdit(data.title)
  })

  return (
    <form className="text-indigo-900 p-2 space-y-4" onSubmit={onSubmit}>
      <h2 className="text-xl ">
        <Label>Elnevezés</Label>
        <Input type="text" disabled={!editing} {...register('title')} />
      </h2>
      <div>
        <h3 className="text-lg">Elemek</h3>
        <ol className="list-decimal p-2 space-y-2">
          {template.items.map((item) => {
            return <li key={item.id}>{item.title}</li>
          })}
        </ol>
      </div>
      {editable && (
        <Button
          type={editing ? 'submit' : 'button'}
          color="indigo"
          className="relative"
          onClick={() => setEditing((val) => !val)}
        >
          {editing ? 'Mentés' : <FiEdit className="w-4 h-4" />}
          {!editing && <span className="sr-only">Szerkesztés</span>}
        </Button>
      )}
      <Link href={`/songorder/new/${template.id}`} passHref={false}>
        <Button color="gold" className="flex space-x-2 items-center">
          <TiPlus />
          <span>Új énekrend</span>
        </Button>
      </Link>
    </form>
  )
}
