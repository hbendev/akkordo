import React from 'react'
import { useForm } from 'react-hook-form'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import FormError from 'components/ui/FormError'
import HelperText from 'components/ui/HelperText'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import { noop } from 'lodash'

type FormData = {
  name: string
  writer: string
  translator: string
}

type SongFormProps = {
  className?: string
  onSubmit?: (song: FormData) => void
}

export default function SongForm({ className, onSubmit = noop }: SongFormProps) {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<FormData>()

  const onSubmitCallback = handleSubmit(async (data) => {
    onSubmit(data)
  })

  return (
    <form className={clsx('space-y-2', className)} onSubmit={onSubmitCallback}>
      <div className="p-2">
        <Label>Ének neve</Label>
        <Input type="text" {...register('name')} required placeholder="" />
        <FormError>
          {errors.name?.type === 'required' && 'Kötelező megadni az új ének nevét'}
        </FormError>
      </div>
      <div className="flex items-center justify-between">
        <div className="flex-1 p-2">
          <Label>Szerző</Label>
          <Input type="text" {...register('writer')} placeholder="Ismeretlen" />
          <HelperText>Ha nem tudod, hagyd üresen a mezőt</HelperText>
        </div>
        <div className="flex-1 p-2">
          <Label>Fordító</Label>
          <Input type="text" {...register('translator')} placeholder="Ismeretlen" />
          <HelperText>Ha nem tudod, hagyd üresen a mezőt</HelperText>
        </div>
      </div>
      <div className="flex justify-center">
        <Button color="indigo" type="submit">
          Mentés
        </Button>
      </div>
    </form>
  )
}
