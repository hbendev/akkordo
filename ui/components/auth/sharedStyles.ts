import clsx from 'clsx'

const field = clsx('my-6 w-full max-w-md')

const sharedAuthFormStyles = {
  form: clsx('bg-white shadow-xl rounded px-12 pt-6 pb-8 mb-4 max-w-4xl'),
  title: clsx('text-gray-800 text-2xl flex justify-center border-b py-2 mb-4'),
  fields: clsx('flex flex-col items-center'),
  field,
  buttonContainer: clsx(field, 'flex items-center justify-between w-full'),
  button: clsx(
    'px-4 py-2 rounded text-white inline-block shadow-lg bg-indigo-500 hover:bg-indigo-600 focus:bg-indigo-700 mr-2'
  ),
  link: clsx('text-sm text-indigo-500 hover:text-indigo-800 ml-2')
}

export default sharedAuthFormStyles
