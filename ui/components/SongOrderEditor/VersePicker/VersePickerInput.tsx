import React, { useEffect, useState } from 'react'

import {
  VerseTypeHungarianLabel,
  verseTypeLabels
} from 'components/SongDetails/LyricsEditor/LyricsEditor'
import HelperText from 'components/ui/HelperText'
import Input from 'components/ui/Input'
import { VerseFragment, VerseType } from 'generated/apollo-components'

type VerseTypeAbbreviation = 'b' | 'v' | 'c' | 'r'

export const verseTypeAbbreviations: Record<VerseType, VerseTypeAbbreviation> = {
  Bridge: 'b',
  Chorus: 'c',
  Refrain: 'r',
  Verse: 'v'
}

export function isVerseTypeAbbreviated(
  lowerCaseString: string
): lowerCaseString is VerseTypeAbbreviation {
  return Object.values(verseTypeAbbreviations).includes(lowerCaseString as VerseTypeAbbreviation)
}
export function isVerseTypeLabel(
  lowerCaseString: string
): lowerCaseString is VerseTypeHungarianLabel {
  return Object.values(verseTypeLabels).includes(lowerCaseString as VerseTypeHungarianLabel)
}

function checkVerseType(string: string): VerseTypeAbbreviation | VerseTypeHungarianLabel | '' {
  const lowerCase = string.toLowerCase()
  if (!(isVerseTypeAbbreviated(lowerCase) || isVerseTypeLabel(lowerCase))) {
    return ''
  }

  return lowerCase
}

// What separates different verses in input
const separators = [' ', ',', ';', '-']

export function getVerseType(
  reference: VerseTypeAbbreviation | VerseTypeHungarianLabel
): VerseType | null {
  if (reference === 'r' || reference === 'refrén') {
    return VerseType.Refrain
  }
  if (reference === 'b' || reference === 'bridge') {
    return VerseType.Bridge
  }
  if (reference === 'c' || reference === 'chorus') {
    return VerseType.Chorus
  }
  if (reference === 'v' || reference === 'versszak') {
    return VerseType.Verse
  }

  return null
}

export function getTypeString(reference: string): string {
  const firstWordBeforeNumberRegex = /\D+/
  const typeString = (reference.match(firstWordBeforeNumberRegex)?.[0] || '') as string
  return typeString
}

export function getOrder(reference: string): number {
  const firstNumberRegex = /\d+/
  const number = Number(reference.match(firstNumberRegex)?.[0] || 0)
  return number
}

// this can be optimized
function splitByMultipleSeparators(source: string, separators: string[]): string[] {
  return separators.reduce((old, c) => old.map((v) => v.split(c)).flat(), [source])
}

// This is a textfield to pick from different avaible verse types
// the result will be put into a textarea, where it can be further break into smaller parts

export type VerseOrderAndType = Required<Pick<VerseFragment, 'order' | 'type'>>

type VersePickerInputProps = {
  initialValue?: string
  className?: string
  onChange: (selectedVerses: VerseOrderAndType[]) => void
  disabled?: boolean
}

export default function VersePickerInput({
  initialValue = '',
  onChange,
  className,
  disabled
}: VersePickerInputProps) {
  const [value, setValue] = useState<string>(initialValue)
  useEffect(() => {
    const parts: VerseOrderAndType[] = splitByMultipleSeparators(value, separators)
      .map((verse) => ({
        order: getOrder(verse),
        type: getTypeString(verse)
      }))
      .filter((verse) => Boolean(checkVerseType(verse.type)))
      .map((verse) => ({
        order: verse.order,
        type: getVerseType(verse.type as VerseTypeAbbreviation | VerseTypeHungarianLabel)
      }))
    onChange(parts)
  }, [value])

  return (
    <div className={className}>
      <Input
        type="text"
        placeholder="Válassz egy sorrendet"
        value={value}
        disabled={disabled}
        className="w-full h-10"
        onChange={(e) => {
          const newValue = e.target.value
          setValue(newValue)
        }}
      />
      <HelperText>
        Válassz a következő versszakok közül. Ezeket a karaktereket használhatod a részek
        elválasztásához: {separators.join(' vagy ')}.
      </HelperText>
    </div>
  )
}
