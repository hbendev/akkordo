import dynamic from 'next/dynamic'
export default dynamic(() => import('./SongOrderEditor'), {
  ssr: false
})
