import React, { useState } from 'react'
import { NestedValue, useFieldArray, useForm } from 'react-hook-form'
import { TiPlus } from 'react-icons/ti'

import Modal from 'components/Modal'
import SongForm from 'components/SongForm'
import Button from 'components/ui/Button'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import Tooltip from 'components/ui/Tooltip'
import {
  SlideGroupFragment,
  SongOrderWithGroupsFragment,
  useAddSongMutation,
  useSwapSlideGroupsMutation
} from 'generated/apollo-components'
import { PartialBy } from 'util/utilTypes'

import SongPicker from './SongPicker'

export type LocalSongOrderItem = PartialBy<SlideGroupFragment, 'id'>

type SongOrderEditorProps = {
  songOrder?: SongOrderWithGroupsFragment | null
  onSubmit?: (songOrderItems: LocalSongOrderItem[], title?: string) => void
  className?: string
  onSaveAsTemplate?: (items: Pick<LocalSongOrderItem, 'title' | 'order'>[]) => void
}

export default function SongOrderEditor({
  songOrder,
  onSubmit = () => {},
  onSaveAsTemplate = () => {},
  className
}: SongOrderEditorProps) {
  const [songModalOpen, setSongModalOpen] = useState(false)
  const [addSong] = useAddSongMutation({
    onCompleted: () => {
      setSongModalOpen(false)
    }
  })
  const [swapSlideGroups] = useSwapSlideGroupsMutation({ refetchQueries: ['songOrder'] })
  const { control, handleSubmit, register, getValues } = useForm<{
    songOrderItems: NestedValue<LocalSongOrderItem[]>
    title?: string
  }>({
    defaultValues: {
      songOrderItems: songOrder
        ? songOrder.groups.map((song) => ({
            ...song,
            song: song.song ? { ...song.song, label: song.song?.name } : null
          }))
        : [],
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      title: songOrder ? songOrder.title : ''
    }
  })
  const { fields, append, remove, move } = useFieldArray({
    control,
    name: `songOrderItems`,
    keyName: 'formKey'
  })

  const onFormSubmit = handleSubmit((data) => {
    onSubmit(
      data.songOrderItems.map((item, i) => {
        return {
          ...item,
          order: i,
          song: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            id: item.song?.id || item.song?.value,
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            name: item.song?.name || item.song?.label
          },
          isSongOrderItem: true
        }
      }),
      data.title
    )
  })

  return (
    <div className={className}>
      <form onSubmit={onFormSubmit}>
        <Label>Az énekrend címe</Label>
        <Input type="text" {...register(`title`)} placeholder="Cím" className="w-full h-10 mb-2" />
        <div className="space-y-4">
          {fields.map((verse, index) => {
            return (
              <>
                <SongPicker
                  key={verse.id || index}
                  titleInputProps={register(`songOrderItems.${index}.title`)}
                  moveDownButtonProps={{
                    disabled: index === fields.length - 1,
                    onClick: () => {
                      if (index === fields.length - 1) {
                        return
                      }
                      move(index, index + 1)
                      if (fields[index].id && fields[index + 1].id) {
                        swapSlideGroups({
                          variables: {
                            groupId1: fields[index].id as string,
                            groupId2: fields[index + 1].id as string
                          }
                        })
                      }
                    }
                  }}
                  moveUpButtonProps={{
                    disabled: index === 0,
                    onClick: () => {
                      if (index === 0) {
                        return
                      }
                      move(index, index - 1)
                      if (fields[index].id && fields[index - 1].id) {
                        swapSlideGroups({
                          variables: {
                            groupId1: fields[index].id as string,
                            groupId2: fields[index - 1].id as string
                          }
                        })
                      }
                    }
                  }}
                  removeButtonProps={{
                    onClick: () => {
                      remove(index)
                    }
                  }}
                  songControllerProps={{
                    name: `songOrderItems.${index}.song`,
                    // @ts-expect-error bad typing
                    control
                  }}
                />
                <hr className="border-t-2 border-gold-400 border-dashed py-2" />
              </>
            )
          })}
        </div>
        <div className="space-y-2 mt-2">
          <div className="w-2/3 flex justify-center">
            <Tooltip label="Hozzáadás">
              <Button
                color="indigo"
                type="button"
                className="flex items-center"
                onClick={() => {
                  append([
                    {
                      title: ''
                    }
                  ])
                }}
              >
                <TiPlus />
              </Button>
            </Tooltip>
          </div>

          <hr className="border-t-2 border-gold-400 border-dashed py-2" />

          <div className="flex space-x-2 items-center">
            <Button color="indigo" type="submit">
              Mentés
            </Button>
            <Button
              color="indigo"
              type="button"
              className=""
              onClick={() =>
                onSaveAsTemplate(
                  getValues().songOrderItems.map((item, i) => ({ order: i, title: item.title }))
                )
              }
            >
              Mentés sablonként
            </Button>
          </div>
        </div>
        <div>
          <Button
            color="gold"
            className="mt-4"
            onClick={() => setSongModalOpen(true)}
            type="button"
          >
            Új ének feltöltése
          </Button>
        </div>
      </form>
      <Modal isOpen={songModalOpen} onClose={() => setSongModalOpen(false)}>
        <SongForm
          onSubmit={(data) => {
            addSong({ variables: { data } })
          }}
        />
      </Modal>
    </div>
  )
}
