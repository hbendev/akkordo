import { SongOrderWithGroupsFragment } from 'generated/apollo-components'

export const mockPresentation: SongOrderWithGroupsFragment = {
  id: '2',
  presentationConfig: {},
  groups: new Array(4).fill(null).map((group, i) => ({
    id: i.toString(),
    title: `title ${i}`,
    order: i,
    slides: [
      { id: '1', order: 0, title: 'v1', content: 'szöveg' },
      { id: '2', config: {}, order: 1, title: 'v2', content: 'szöveg' }
    ],
    song: { id: 'Songid', name: 'wat', song: { id: '1', name: 'Name of song' } }
  }))
}
