import { Meta } from '@storybook/react'

import Component from './Slides'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'PresentationEditor/Slides'
}
export default meta

const template = storyTemplate(Component)

export const Slides = template({})
