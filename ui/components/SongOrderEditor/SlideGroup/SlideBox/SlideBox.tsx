import React from 'react'

import clsx from 'clsx'

export type SlideBoxProps = {
  className?: string
  onClick?: () => void
  children?: React.ReactNode
}

export default function SlideBox({ className, onClick, children }: SlideBoxProps) {
  return (
    <div
      className={clsx(
        'w-40 h-28 bg-indigo-300 border border-indigo-500 rounded flex justify-center items-center p-2 overflow-hidden',
        className
      )}
      onClick={onClick}
    >
      {children}
    </div>
  )
}
