export { default } from './SlideBox'

import type { SlideBoxProps } from './SlideBox'
export type { SlideBoxProps }
