import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './SlideBox'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'PresentationEditor/SlideBox'
}
export default meta

const template = storyTemplate(Component)

export const SlideBox = template({
  children: 'content',
  className: '',
  onClick: action('click')
})
