import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import { mockSlideGroup } from './mocks'
import Component from './SlideGroup'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'PresentationEditor/SlideGroup'
}
export default meta

const template = storyTemplate(Component)

export const SlideGroupBoxes = template({
  group: mockSlideGroup,
  onDelete: action('delete'),
  onEdit: action('edit'),
  onJump: action('jump')
})
