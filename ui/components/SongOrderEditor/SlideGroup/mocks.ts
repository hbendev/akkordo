import { SlideGroupFragment } from 'generated/apollo-components'

export const mockSlideGroup: SlideGroupFragment = {
  id: '1',
  order: 0,
  slides: [
    {
      id: '1',
      order: 0,
      // config: {},
      title: 'v1',
      content: ''
    },
    {
      id: '2',
      order: 1,
      // config: {},
      title: 'v2',
      content: ''
    }
  ]
}
