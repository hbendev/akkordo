import React from 'react'
import { TiArrowBack, TiCog, TiEdit } from 'react-icons/ti'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import Tooltip from 'components/ui/Tooltip'

type SideBarHeaderProps = {
  showBackButton?: boolean | string
  showNoteButton?: boolean
  showSettingsButton?: boolean
  onBackClick?: () => void
  onNotesButtonClick?: () => void
  onEditButtonClick?: () => void
  className?: string
  title?: string
}

export default function SideBarHeader({
  showBackButton = false,
  showNoteButton = false,
  showSettingsButton = false,
  onBackClick = () => {},
  onNotesButtonClick = () => {},
  onEditButtonClick = () => {},
  className,
  title
}: SideBarHeaderProps) {
  return (
    <div className={clsx('flex items-center justify-between w-full', className)}>
      <div className={clsx('flex space-x-2 items-center')}>
        {showBackButton && (
          <Tooltip label={typeof showBackButton === 'string' ? showBackButton : 'Vissza'}>
            <Button color="indigo" onClick={onBackClick}>
              <TiArrowBack />
            </Button>
          </Tooltip>
        )}
        <span>{title}</span>
      </div>
      <div className="flex space-x-2 items-center">
        {showNoteButton && (
          <Tooltip label="Jegyzetek">
            <Button color="indigo" onClick={onNotesButtonClick}>
              <TiEdit />
            </Button>
          </Tooltip>
        )}
        {showSettingsButton && (
          <Tooltip label="Beállítások">
            <Button color="indigo" onClick={onEditButtonClick}>
              <TiCog />
            </Button>
          </Tooltip>
        )}
      </div>
    </div>
  )
}
