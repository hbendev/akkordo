import React from 'react'
import Head from 'next/head'

export type RevealJsTheme =
  | 'black'
  | 'beige'
  | 'blood'
  | 'league'
  | 'moon'
  | 'night'
  | 'serif'
  | 'simple'
  | 'sky'
  | 'solarized'
  | 'white'

type RevealJsStylesProps = {
  theme: RevealJsTheme
  print?: boolean
}

export default function RevealJsStyles({ theme, print }: RevealJsStylesProps) {
  return (
    <Head>
      {/* eslint-disable-next-line @next/next/no-css-tags */}
      <link rel="stylesheet" href="/reveal-reset.css" />
      {/* eslint-disable-next-line @next/next/no-css-tags */}
      <link rel="stylesheet" href="/reveal.css" />
      {/* eslint-disable-next-line @next/next/no-css-tags */}
      <link rel="stylesheet" href={`/theme/${theme}.css`} />
      {/* eslint-disable-next-line @next/next/no-css-tags */}
      {print && <link rel="stylesheet" href={`/reveal-print.css`} />}
    </Head>
  )
}
