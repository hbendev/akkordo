import React, { useCallback } from 'react'
import { Column } from 'react-table'
import router from 'next/router'

import Table, { FetchParameters } from 'components/Table/Table'
import Link from 'components/ui/Link'
import UserIcon from 'components/UserIcon'
import {
  CommentFragment,
  useChangeCommentPublicityMutation,
  useChangeLyricsPublicityMutation,
  useSearchCommentsQuery
} from 'generated/apollo-components'
import usePageSize from 'hooks/usePageSize'
import { transformToPrismaSort } from 'util/sort'

import { getDateColumn, getPublicityColumns } from '../util'

export default function CommentTable() {
  const [pageSize, setPageSize] = usePageSize()
  const [changeCommentPublicity] = useChangeCommentPublicityMutation()
  const columns = React.useMemo<Column<CommentFragment>[]>(
    () => [
      {
        Header: 'Ének',
        accessor: 'song',
        Cell: ({ row }) => {
          return (
            <Link
              href={`/song/${row.original.song.id}`}
              onClick={(e) => {
                e.stopPropagation()
              }}
            >
              {row.original.song.name}
            </Link>
          )
        }
      },
      {
        Header: 'Felhasználó',
        accessor: 'user',
        Cell: ({ row }) => {
          return (
            <div>
              <UserIcon user={row.original.user} />
            </div>
          )
        }
      },
      { Header: 'Tartalom', accessor: 'text' },
      getDateColumn<CommentFragment>({ Header: 'Hozzáadva', accessor: 'createdAt' }),
      ...getPublicityColumns<CommentFragment>(changeCommentPublicity)
    ],
    [changeCommentPublicity]
  )

  const { loading, data, refetch } = useSearchCommentsQuery({
    variables: { take: 10, skip: 0 }
  })

  const refetchData = useCallback(
    ({ pageSize, pageIndex, sortBy }: FetchParameters<CommentFragment>) => {
      refetch({
        skip: pageSize * pageIndex,
        take: pageSize,
        orderBy: sortBy.length
          ? sortBy.reduce((sorts, curr) => {
              if (curr.id === 'song') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              if (curr.id === 'user') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              return { ...sorts, [curr.id]: transformToPrismaSort(curr) }
            }, {})
          : null
      })
    },
    [refetch]
  )

  return (
    <>
      <Table
        columns={columns}
        data={data?.searchComments || []}
        fetchData={refetchData}
        loading={loading}
        pageCount={Math.ceil((data?.commentsCount || 1) / pageSize)}
        pageSize={pageSize}
        setPageSize={setPageSize}
        dataCount={data?.commentsCount}
      />
    </>
  )
}
