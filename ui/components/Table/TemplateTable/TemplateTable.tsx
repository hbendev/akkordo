import React, { useCallback } from 'react'
import { Column } from 'react-table'
import { useRouter } from 'next/router'

import Table, { FetchParameters } from 'components/Table/Table'
import UserIcon from 'components/UserIcon'
import {
  TemplateFragment,
  useChangeTemplatePublicityMutation,
  useSearchTemplatesQuery
} from 'generated/apollo-components'
import usePageSize from 'hooks/usePageSize'
import { transformToPrismaSort } from 'util/sort'

import { getDateColumn, getPublicityColumns } from '../util'

export default function TemplateTable() {
  const [pageSize, setPageSize] = usePageSize()
  const router = useRouter()

  const [changeTemplatePublicity] = useChangeTemplatePublicityMutation()

  const columns = React.useMemo<Column<TemplateFragment>[]>(
    () => [
      {
        Header: 'Cím',
        accessor: 'title'
      },
      getDateColumn<TemplateFragment>({ Header: 'Létrehozva', accessor: 'createdAt' }),
      {
        Header: 'Feltöltő',
        accessor: 'creator',
        Cell: function TagCell({ row }) {
          return <UserIcon user={row.original.creator} />
        }
      },
      ...getPublicityColumns<TemplateFragment>(changeTemplatePublicity)
    ],
    [changeTemplatePublicity]
  )

  const { loading, data, refetch } = useSearchTemplatesQuery({
    variables: { take: 10, skip: 0 }
  })

  const refetchData = useCallback(
    ({ pageSize, pageIndex, sortBy }: FetchParameters<TemplateFragment>) => {
      refetch({
        skip: pageSize * pageIndex,
        take: pageSize,
        orderBy: sortBy.length
          ? sortBy.reduce((sorts, curr) => {
              if (curr.id === 'creator') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              return { ...sorts, [curr.id]: transformToPrismaSort(curr) }
            }, {})
          : null
      })
    },
    [refetch]
  )

  return (
    <Table
      columns={columns}
      data={data?.userTemplates || []}
      fetchData={refetchData}
      loading={loading}
      pageCount={Math.ceil((data?.userTemplatesCount || 1) / pageSize)}
      pageSize={pageSize}
      setPageSize={setPageSize}
      dataCount={data?.userTemplatesCount}
      onRowClick={(row) => {
        router.push(`/templates/${row.original.id}`)
      }}
    />
  )
}
