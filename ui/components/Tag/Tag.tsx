import React from 'react'
import { TiDeleteOutline } from 'react-icons/ti'

import clsx from 'clsx'
import Tooltip from 'components/ui/Tooltip'
import { noop } from 'lodash'

type TagProps = React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> & {
  deletable?: boolean
  onDelete?: () => void
}

export default function Tag({ deletable, onDelete = noop, children, ...props }: TagProps) {
  return (
    <span
      {...props}
      className={clsx(
        'px-4 py-2 rounded-lg border border-indigo-800 bg-indigo-500 text-white flex items-center justify-start max-w-[8rem] flex-nowrap',
        props.className
      )}
    >
      <div className={clsx('truncate', deletable && 'w-4/5')}>{children}</div>
      {deletable && (
        <div>
          <Tooltip label="Törlés">
            <div onClick={onDelete} className="cursor-pointer relative">
              <span className="sr-only">Törlés</span>
              <TiDeleteOutline className="w-4 h-4" />
            </div>
          </Tooltip>
        </div>
      )}
    </span>
  )
}
