import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './AddComment'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'AddComment'
}
export default meta

const template = storyTemplate(Component)

export const AddComment = template({ onSubmit: action('submit') })
