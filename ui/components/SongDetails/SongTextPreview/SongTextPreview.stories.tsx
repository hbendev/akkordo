import { Meta } from '@storybook/react'
import { mockTextPreview } from 'components/SongDetails/mocks'

import Component from './SongTextPreview'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongDetails/SongTextPreview'
}
export default meta

const template = storyTemplate(Component)

export const SongTextPreview = template({ lyrics: mockTextPreview })
