import React from 'react'

import { verseTypeAbbreviations } from 'components/SongOrderEditor/VersePicker/VersePickerInput'
import Link from 'components/ui/Link'
import UserIcon from 'components/UserIcon'
import { LyricsFragment, VerseType } from 'generated/apollo-components'

type SongTextPreviewProps = {
  lyrics: LyricsFragment
  disableLyricsAdd?: boolean
}

export default function SongTextPreview({ lyrics, disableLyricsAdd }: SongTextPreviewProps) {
  return (
    <div className="space-y-8">
      <div className="space-y-2 bg-gray-50 p-4 outline-black">
        {lyrics.verses.map((verse) => {
          return (
            <div key={verse.id}>
              <span className="text-xs select-none">
                {verseTypeAbbreviations[verse.type || VerseType.Verse].toUpperCase()}
                {verse.order}
              </span>
              <p className="whitespace-pre-line">{verse.text}</p>
            </div>
          )
        })}
      </div>
      {/* todo below should be extracted to components - the button and the description are not actually a part of a lyrics preview*/}
      <div className="">
        <span>Feltöltötte:</span>
        <UserIcon className="inline-block" user={lyrics.creator} small />
      </div>
      <Link href={`/song/${lyrics.song?.id}/lyrics/new`} disabled={disableLyricsAdd}>
        Szöveg hozzáadása
      </Link>
    </div>
  )
}
