import { Meta } from '@storybook/react'
import { mockRecording } from 'components/SongDetails/mocks'

import Component from './RecordingBox'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongDetails/RecordingBox'
}
export default meta

const template = storyTemplate(Component)

export const RecordingBox = template({ recording: mockRecording })
