import React from 'react'

import Tag from 'components/Tag'
import { TagFragment } from 'generated/apollo-components'

import './SongTags.module.css'

type SongTagsProps = {
  tags: TagFragment[]
  onDeleteTag?: (id: string) => void
}

export default function SongTags({ tags, onDeleteTag = () => {} }: SongTagsProps) {
  return (
    <ul className="flex space-x-2 w-full overflow-x-auto flex-nowrap">
      {tags.map((tag) => (
        <Tag
          key={tag.id}
          className="flex items-center justify-between"
          onDelete={() => onDeleteTag(tag.id)}
          deletable
        >
          {tag.text}
        </Tag>
      ))}
    </ul>
  )
}
