import React, { useState } from 'react'
import { TiArrowLeft, TiArrowRight } from 'react-icons/ti'

import { useApolloClient } from '@apollo/client'
import Tooltip from '@reach/tooltip'
import clsx from 'clsx'
import AddComment from 'components/AddComment'
import AddRecording from 'components/AddRecording'
import Comment from 'components/Comment'
import Modal from 'components/Modal'
import ScoreUpload from 'components/ScoreUpload'
import RecordingBox from 'components/SongDetails/RecordBox'
import ScoreBox from 'components/SongDetails/ScoreBox'
import SongMeta from 'components/SongDetails/SongMeta'
import SongTags from 'components/SongDetails/SongTags'
import SongTextPreview from 'components/SongDetails/SongTextPreview'
import Button from 'components/ui/Button'
import { HelperText } from 'components/ui/HelperText/HelperText.stories'
import Link from 'components/ui/Link'
import {
  SongDetailsFragment,
  useAddCommentMutation,
  useAddTagToSongMutation,
  useDeleteTagFromSongMutation
} from 'generated/apollo-components'
import useArrayIndex from 'hooks/useArrayIndex'
import { noop } from 'lodash'

import NewTag from './NewTag'

type SongDetailsProps = {
  song: SongDetailsFragment
  numberOfScores?: number | null
  numberOfRecordings?: number | null
  onFetchMoreRecordings?: () => void
  onFetchMoreScores?: () => void
  hideAddComment?: boolean
  hideAddTag?: boolean
  disableUpload?: boolean
  disableLyricsAdd?: boolean
}

const title = clsx('text-2xl font-bold')
const button = clsx('text-white font-bold py-2 px-4 rounded')

export default function SongDetails({
  song,
  onFetchMoreRecordings = noop,
  onFetchMoreScores = noop,
  numberOfRecordings,
  numberOfScores,
  hideAddComment,
  hideAddTag,
  disableUpload,
  disableLyricsAdd
}: SongDetailsProps) {
  // todo accept more props
  const client = useApolloClient()
  const [scoreUploadModalOpen, setScoreUploadModalOpen] = useState<boolean>(false)
  const [recordingUploadModalOpen, setRecordingUploadModalOpen] = useState<boolean>(false)
  const [addTag] = useAddTagToSongMutation()
  const [deleteTag] = useDeleteTagFromSongMutation()
  const { selectedItem, next, previous, index } = useArrayIndex(song.lyrics)
  const [addComment] = useAddCommentMutation({
    onCompleted: () => {
      client.refetchQueries({ include: ['song'] })
    }
  })

  return (
    <>
      <div className="flex gap-8 container mx-auto">
        <Modal isOpen={scoreUploadModalOpen} onClose={() => setScoreUploadModalOpen(false)}>
          <ScoreUpload
            onSucces={() => {
              client.refetchQueries({ include: ['song'] })
              setScoreUploadModalOpen(false)
            }}
          />
        </Modal>
        <Modal isOpen={recordingUploadModalOpen} onClose={() => setRecordingUploadModalOpen(false)}>
          <AddRecording
            onSucces={() => {
              client.refetchQueries({ include: ['song'] })
              setRecordingUploadModalOpen(false)
            }}
          />
        </Modal>
        <div className="w-1/2 space-y-6">
          <SongMeta song={song} />

          <section className="space-y-4">
            <h2 className={title}>Kották</h2>
            <div className="flex gap-4 flex-wrap justify-start w-full">
              {song.scores.map((score) => (
                <ScoreBox key={score.id} score={score} />
              ))}
            </div>
            <div className="flex space-x-2">
              <Button
                color="indigo"
                className={button}
                onClick={onFetchMoreScores}
                disabled={(numberOfScores ?? 0) <= song.scores.length}
              >
                Több betöltése
              </Button>
              <Button
                color="indigo"
                className={button}
                disabled={disableUpload}
                onClick={() => {
                  setScoreUploadModalOpen(true)
                }}
              >
                Feltöltés
              </Button>
            </div>
          </section>

          <section className="space-y-4">
            <h2 className={title}>Felvételek</h2>
            <div className="flex gap-4 flex-wrap w-full justify-start">
              {song.recordings.map((recording) => (
                <RecordingBox key={recording.id} recording={recording} />
              ))}
            </div>
            <div className="flex space-x-2">
              <Button
                color="indigo"
                className={button}
                onClick={onFetchMoreRecordings}
                disabled={(numberOfRecordings ?? 0) <= song.recordings.length}
              >
                Több betöltése
              </Button>
              <Button
                color="indigo"
                className={button}
                disabled={disableUpload}
                onClick={() => {
                  setRecordingUploadModalOpen(true)
                }}
              >
                Feltöltés
              </Button>
            </div>
          </section>
        </div>

        <div className="w-1/2 space-y-6">
          <div>
            <h2 className={title}>Cimkék</h2>
            {hideAddTag ? (
              <HelperText>A saját cimkéid elmentéséhez jelentkezz be.</HelperText>
            ) : (
              <>
                <SongTags
                  tags={song.userTags}
                  onDeleteTag={(id) => deleteTag({ variables: { tagId: id, songId: song.id } })}
                />
                <NewTag onAdd={(text) => addTag({ variables: { songId: song.id, text } })} />
              </>
            )}
          </div>

          <hr className="border-t-2 border-gold-400 border-dashed" />

          <div>
            <h2 className={title}>Szövegek</h2>
            <div className="flex space-x-2 items-center">
              {song.lyrics.length > 1 && (
                <div className="flex gap-x-2">
                  <Tooltip label="Előző szöveg">
                    <Button color="gold" onClick={previous}>
                      <TiArrowLeft />
                    </Button>
                  </Tooltip>
                  <Tooltip label="Következő szöveg">
                    <Button color="gold" onClick={next}>
                      <TiArrowRight />
                    </Button>
                  </Tooltip>
                </div>
              )}
              {
                <div>
                  {song.lyrics.length === 0
                    ? 'Sajnos még nincs publikus szöveg feltöltve ehhez az énekhez'
                    : `${index + 1}/${song.lyrics.length}`}
                </div>
              }
            </div>
          </div>

          {selectedItem && (
            <>
              <SongTextPreview lyrics={selectedItem} disableLyricsAdd={disableLyricsAdd} />
            </>
          )}

          {!song.lyrics.length && (
            <Link href={`/song/${song.id}/lyrics/new`} disabled={disableLyricsAdd}>
              Szöveg hozzáadása
            </Link>
          )}
        </div>
      </div>
      <section className="container mx-auto mt-6">
        <h2 className={title}>Kommentek</h2>
        <div className="space-y-4">
          {song.comments.map((comment) => {
            return <Comment key={comment.id} comment={comment} />
          })}
        </div>
        <div>
          {hideAddComment ? (
            <HelperText>Hozzászólás írása bejelentkezett felhasználóként lehetséges.</HelperText>
          ) : (
            <AddComment
              onSubmit={(text) =>
                addComment({ variables: { data: { text, song: { connect: { id: song.id } } } } })
              }
            />
          )}
        </div>
      </section>
    </>
  )
}
