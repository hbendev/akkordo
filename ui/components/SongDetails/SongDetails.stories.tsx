import { Meta } from '@storybook/react'
import { mockSong } from 'components/SongDetails/mocks'

import Component from './SongDetails'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongDetails/All',
  parameters: { storyshots: { disable: true } }
}
export default meta

const template = storyTemplate(Component)

export const Page = template({ song: mockSong })
export const Unauthorized = template({
  song: mockSong,
  hideAddComment: true,
  hideAddTag: true,
  disableUpload: true
})
