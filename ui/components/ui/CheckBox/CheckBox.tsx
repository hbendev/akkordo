import React from 'react'

import clsx from 'clsx'

type InputProps = Omit<
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  'type'
>

const CheckBox = React.forwardRef<HTMLInputElement, InputProps>(function Input(
  { className, ...props },
  ref
) {
  return (
    <input
      type="checkbox"
      className={clsx('block rounded w-4 h-4', className, props.disabled && 'border-gray-200')}
      {...props}
      ref={ref}
    />
  )
})

export default CheckBox
