import React from 'react'

import ReachTooltip, { TooltipProps as ReachTooltipProps } from '@reach/tooltip'

import '@reach/tooltip/styles.css'

type TooltipProps = ReachTooltipProps

export default function Tooltip({ children, ...props }: TooltipProps) {
  return (
    <ReachTooltip {...props}>
      <div>{children}</div>
    </ReachTooltip>
  )
}
