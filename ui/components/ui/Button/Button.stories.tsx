import { Meta } from '@storybook/react'

import Component from './Button'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'UI/Button'
}
export default meta

const template = storyTemplate(Component)

export const Indigo = template({ children: 'Tag#1', color: 'indigo' })
export const Small = template({ children: 'Tag#1', color: 'gold', size: 'small' })
export const Large = template({ children: 'Tag#1', color: 'purple', size: 'large' })
