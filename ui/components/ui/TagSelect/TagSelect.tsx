import React from 'react'

import AsyncSelect from 'components/ui/AsyncSelect'

import { AsnycSelectProps } from '../AsyncSelect/AsyncSelect'

type TagSelectProps = Omit<AsnycSelectProps, 'components' | 'isMulti'> & {
  components?: Omit<AsnycSelectProps['components'], 'Option'>
}

// todo refactor as a Creatable?
export default function TagSelect({ components, ...props }: TagSelectProps) {
  return (
    <AsyncSelect
      isMulti
      // components={{ ...components, Option: (props) => <Tag></Tag> }}
      {...props}
    />
  )
}
