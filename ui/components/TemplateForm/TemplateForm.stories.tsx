import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './TemplateForm'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'TemplateForm'
}
export default meta

const template = storyTemplate(Component)

export const TemplateForm = template({ onSubmit: action('submit') })
