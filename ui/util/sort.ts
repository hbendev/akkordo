import { SortingRule } from 'react-table'

import { SortOrder } from 'generated/apollo-components'

export function transformToPrismaSort<T>(useTableSort: SortingRule<T>): SortOrder {
  return useTableSort.desc ? SortOrder.Desc : SortOrder.Asc
}
