import { addDecorator } from '@storybook/react'
import { ApolloProvider } from '@apollo/client'
import { initializeApollo } from '../apolloClient'
import * as NextImage from 'next/image'
import { withLinks } from '@storybook/addon-links';

import '../styles/globals.css'

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/
    }
  },
}

const apolloClient = initializeApollo()

const apolloDecorator = (story) => <ApolloProvider client={apolloClient}>{story()}</ApolloProvider>
addDecorator(apolloDecorator)

const OriginalNextImage = NextImage.default

// eslint-disable-next-line no-import-assign
Object.defineProperty(NextImage, 'default', {
  configurable: true,
  value: (/** @type {import('next/image').ImageProps} */ props) => {
    if (typeof props.src === 'string') {
      return (
        <OriginalNextImage {...props} unoptimized blurDataURL={props.src} />
      )
    } else {
      // don't need blurDataURL here since it is already defined on the StaticImport type
      return <OriginalNextImage {...props} unoptimized />
    }
  }
})

export const decorators = [withLinks, apolloDecorator];