import React from 'react'

import { UserFragment } from 'generated/apollo-components'

type UserContext = UserFragment

export const UserContext = React.createContext<UserContext | null>(null)
