/* eslint-disable @typescript-eslint/no-var-requires */
const { PHASE_DEVELOPMENT_SERVER, PHASE_PRODUCTION_BUILD } = require('next/constants')
const withPWA = require('next-pwa')
const runtimeCaching = require('next-pwa/cache')
const { envsafe, str, bool, email } = require('envsafe')

const env = envsafe({
  NODE_ENV: str({
    devDefault: 'development',
    choices: ['development', 'production', 'test']
  }),
  IS_STORYBOOK: bool(),
  NEXT_PUBLIC_API_URL: str({ devDefault: 'http://localhost:4000/gql' }),
  NEXT_PUBLIC_SPACES_ORIGIN: str(),
  NEXT_PUBLIC_SPACES_EDGE: str(),
  NEXT_PUBLIC_MAINTAINER_EMAIL: email()
})

const rewritesConfig = [
  {
    source: '/gql',
    destination: env.NEXT_PUBLIC_API_URL
  }
]

module.exports = withPWA({
  reactStrictMode: true,
  async rewrites() {
    return rewritesConfig
  },
  [PHASE_PRODUCTION_BUILD]: {
    env: {
      API_URL: env.NEXT_PUBLIC_API_URL
    }
  },
  [PHASE_DEVELOPMENT_SERVER]: {
    env: {
      API_URL: env.NEXT_PUBLIC_API_URL
    }
  },
  pwa: {
    dest: 'public',
    register: true,
    cacheOnFrontEndNav: true,
    runtimeCaching,
    buildExcludes: [/middleware-manifest\.json$/],
    fallbacks: {
      document: '/_offline' // if you want to fallback to a custom    page other than /_offline
    },
    reloadOnOnline: false
  },
  webpack(config, { webpack }) {
    config.plugins.push(new webpack.IgnorePlugin({ resourceRegExp: /\.stories\./ }))
    config.plugins.push(new webpack.IgnorePlugin({ resourceRegExp: /mocks\./ }))

    return config
  },
  images: {
    domains: [
      env.NEXT_PUBLIC_SPACES_ORIGIN,
      env.NEXT_PUBLIC_SPACES_EDGE,
      ...(env.IS_STORYBOOK ? ['picsum.photos'] : [])
    ]
  }
})
