const path = require('path')
const fs = require('fs')
const proxy = require('http-proxy')

const cert = path.join(process.cwd(), 'localhost.crt')
const key = path.join(process.cwd(), 'localhost.key')

proxy
  .createServer({
    xfwd: true,
    ws: true,
    target: {
      host: 'localhost',
      port: 3001
    },
    ssl: {
      key: fs.readFileSync(key, 'utf8'),
      cert: fs.readFileSync(cert, 'utf8')
    }
  })
  .on('error', function (e) {
    console.error(`Request failed to proxy: ${e.code}`)
  })
  .listen(3000)
