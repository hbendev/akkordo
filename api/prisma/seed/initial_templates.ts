type TemplateItem = {
  order: number
  title: string
}

type TemplateData = {
  items: TemplateItem[]
  title: string
}

const templates: TemplateData[] = [
  {
    title: 'Szentmise',
    items: [
      { order: 1, title: 'Bevonulás' },
      { order: 2, title: 'Kyrie' },
      { order: 3, title: 'Gloria' },
      { order: 4, title: 'Olvasmányközi ének' },
      { order: 5, title: 'Alleluja' },
      { order: 6, title: 'Felajánlás' },
      { order: 7, title: 'Sanctus' },
      { order: 8, title: 'Agnus Dei' },
      { order: 9, title: 'Áldozás' },
      { order: 10, title: 'Kivonulás' }
    ]
  },
  {
    title: 'Igeliturgia',
    items: [
      { order: 1, title: 'Bevonulás' },
      { order: 2, title: 'Kyrie' },
      { order: 3, title: 'Gloria' },
      { order: 4, title: 'Olvasmányközi ének' },
      { order: 5, title: 'Alleluja' },
      { order: 6, title: 'Oltáriszentség fogadása' },
      { order: 7, title: 'Agnus Dei' },
      { order: 8, title: 'Áldozás' },
      { order: 9, title: 'Kivonulás' }
    ]
  },
  {
    title: 'Református Istentisztelet',
    items: [
      { order: 1, title: 'Kezdő ének' },
      { order: 2, title: 'Fő ének' },
      { order: 3, title: 'Igehirdetésre készítő ének' },
      { order: 4, title: 'Ráfelelő ének' },
      { order: 5, title: 'Záró ének' },
      { order: 6, title: 'Kivonuló ének' }
    ]
  },
  {
    title: 'Evangélikus Istentisztelet',
    items: [
      { order: 1, title: 'Gyülekezeti ének' },
      { order: 2, title: 'Bevezető zsoltár' },
      { order: 3, title: 'Dicsőítés' },
      { order: 4, title: 'Responzórium' },
      { order: 5, title: 'Graduálének' },
      { order: 6, title: 'Kivonuló ének' }
    ]
  }
]

export default templates
