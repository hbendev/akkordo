/** @type {import('prisma-generator-pothos-codegen').Config} */
// eslint-disable-next-line no-undef
module.exports = {
  inputs: {
    outputFilePath: './src/generated/inputs.ts',
    prismaImporter: `import { Prisma } from '@prisma/client'`
  },
  crud: {
    outputDir: './src/generated/',
    prismaImporter: `import { Prisma } from '@prisma/client'`
  },
  global: {
    builderImporter: `import { builder } from 'builder'`
  }
}
