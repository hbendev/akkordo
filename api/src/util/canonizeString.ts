export default function canonizeString(str: string | null | undefined) {
  return str
    ?.normalize('NFKD')
    .replace(/\p{Diacritic}/gu, '')
    .toLowerCase()
}
