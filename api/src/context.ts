import { PrismaClient } from '@prisma/client'
import express from 'express'
import { ExpressContextFunctionArgument } from '@apollo/server/express4'

import { verify } from 'jsonwebtoken'

import { JWTPayload } from './types'
import env from './util/env'

export const prisma = new PrismaClient({ log: ['info'] })

export interface Context {
  prisma: PrismaClient
  user?: JWTPayload
}

function getJWTPayload(request: express.Request) {
  const Authorization = request.get('Authorization') || request.get('authorization')

  if (!Authorization) {
    return null
  }

  const token = Authorization.replace('Bearer ', '')

  try {
    const verifiedToken = verify(token, env.APP_SECRET)

    return verifiedToken as JWTPayload
  } catch (err) {
    return null
  }
}

export async function createContext(request: ExpressContextFunctionArgument): Promise<Context> {
  const payload = getJWTPayload(request.req)

  const context: Context = {
    prisma
  }

  if (payload) {
    context.user = payload
  }

  return context
}
