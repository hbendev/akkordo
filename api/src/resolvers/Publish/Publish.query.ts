import { builder } from '../../builder'
import {
  SongWhereUniqueInput,
  LyricsWhereUniqueInput,
  RecordingWhereUniqueInput,
  SongCommentWhereUniqueInput,
  ScoreWhereUniqueInput,
  SongOrderTemplateWhereUniqueInput
} from '../../generated/inputs'
import AppError, { AppErrorType } from '../../AppError'
import { Context } from '../../context'

export async function authorizeAdmin(context: Context): Promise<void> {
  const user = await context.prisma.user.findFirst({ where: { id: context.user?.id } })

  if (!user?.isAdmin) {
    throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
  }
}

export enum Publicity {
  Published = 'Published',
  Rejected = 'Rejected'
}

const publicity = builder.enumType(Publicity, { name: 'Publicity' })

builder.mutationField('changeSongPublicity', (t) => {
  return t.prismaField({
    type: 'Song',
    args: {
      where: t.arg({ type: SongWhereUniqueInput }),
      publishStatus: t.arg({ type: publicity })
    },
    async resolve(query, root, { where, publishStatus }, ctx, _info) {
      await authorizeAdmin(ctx)

      return ctx.prisma.song.update({
        where: { id: where?.id || undefined },
        data: { publishStatus },
        ...query
      })
    }
  })
})

builder.mutationField('changeLyricsPublicity', (t) => {
  return t.prismaField({
    type: 'Lyrics',
    args: {
      where: t.arg({ type: LyricsWhereUniqueInput }),
      publishStatus: t.arg({ type: publicity })
    },
    async resolve(query, root, { where, publishStatus }, ctx, _info) {
      await authorizeAdmin(ctx)

      return ctx.prisma.lyrics.update({
        where: { id: where?.id || undefined },
        data: { publishStatus },
        ...query
      })
    }
  })
})

builder.mutationField('changeRecordingPublicity', (t) => {
  return t.prismaField({
    type: 'Recording',
    args: {
      where: t.arg({ type: RecordingWhereUniqueInput }),
      publishStatus: t.arg({ type: publicity })
    },
    async resolve(query, root, { where, publishStatus }, ctx, _info) {
      await authorizeAdmin(ctx)

      return ctx.prisma.recording.update({
        where: { id: where?.id || undefined },
        data: { publishStatus },
        ...query
      })
    }
  })
})

builder.mutationField('changeScorePublicity', (t) => {
  return t.prismaField({
    type: 'Score',
    args: {
      where: t.arg({ type: ScoreWhereUniqueInput }),
      publishStatus: t.arg({ type: publicity })
    },
    async resolve(query, root, { where, publishStatus }, ctx, _info) {
      await authorizeAdmin(ctx)

      return ctx.prisma.score.update({
        where: { id: where?.id || undefined },
        data: { publishStatus },
        ...query
      })
    }
  })
})

builder.mutationField('changeSongOrderTemplatePublicity', (t) => {
  return t.prismaField({
    type: 'SongOrderTemplate',
    args: {
      where: t.arg({ type: SongOrderTemplateWhereUniqueInput }),
      publishStatus: t.arg({ type: publicity })
    },
    async resolve(query, root, { where, publishStatus }, ctx, _info) {
      await authorizeAdmin(ctx)

      return ctx.prisma.songOrderTemplate.update({
        where: { id: where?.id || undefined },
        data: { publishStatus },
        ...query
      })
    }
  })
})

builder.mutationField('changeCommentPublicity', (t) => {
  return t.prismaField({
    type: 'SongComment',
    args: {
      where: t.arg({ type: SongCommentWhereUniqueInput }),
      publishStatus: t.arg({ type: publicity })
    },
    async resolve(query, root, { where, publishStatus }, ctx, _info) {
      await authorizeAdmin(ctx)

      return ctx.prisma.songComment.update({
        where: { id: where?.id || undefined },
        data: { publishStatus },
        ...query
      })
    }
  })
})
