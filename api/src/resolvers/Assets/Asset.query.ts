import { Prisma } from '.prisma/client'

import { handleUploads, UploadResultImpl, Upload } from '../scalars/Upload.scalar'

import { builder } from '../../builder'
import {
  RecordingCreateInputFields,
  RecordingWhereInputFields,
  ScoreCreateInputFields,
  ScoreWhereInputFields
} from '../../generated/inputs'

import {
  countScoreQueryObject,
  createOneScoreMutationObject,
  findManyScoreQueryObject
} from '../../generated/Score'

import {
  countRecordingQueryObject,
  createOneRecordingMutationObject,
  findManyRecordingQueryObject
} from '../../generated/Recording'

import AppError, { AppErrorType } from '../../AppError'

// ==================================================== Recordings ====================================================

export const recordingWhereWithPrivateField = builder
  .inputRef<Prisma.RecordingWhereInput & { private: boolean }>('RecordingWhereWithPrivateField')
  .implement({
    fields: (t) => ({
      ...RecordingWhereInputFields(t),
      private: t.field({ required: true, type: 'Boolean' })
    })
  })

export const RecordingCreateInputWithFile = builder
  .inputRef<Prisma.RecordingCreateInput & { url: UploadResultImpl }>('RecordingCreateInputWithFile')
  .implement({
    fields: (t) => ({
      ...RecordingCreateInputFields(t),
      // @ts-expect-error
      url: t.field({ type: 'Upload' })
    })
  })

builder.queryFields((t) => {
  const findManyRecording = findManyRecordingQueryObject(t)
  const recordingCount = countRecordingQueryObject(t)

  return {
    recordings: t.prismaField({
      ...findManyRecording,
      args: { ...findManyRecording.args, where: t.arg({ type: scoreWhereWithPrivateField }) },
      resolve: async (query, root, args, ctx, info) => {
        const isPrivate = args.where?.private
        if (args.where?.private) {
          // @ts-expect-error field is not part of schema
          delete args.where.private
        }

        return findManyRecording.resolve(
          query,
          root,
          {
            ...args,
            where: {
              ...args.where,
              ...(isPrivate
                ? { OR: [{ publishStatus: 'Published' }, { userId: ctx.user?.id }] }
                : {})
            }
          },
          ctx,
          info
        )
      }
    }),
    recordingsCount: t.field({
      args: { ...recordingCount.args, where: t.arg({ type: scoreWhereWithPrivateField }) },
      type: 'Int',
      resolve: async (root, args, ctx, info) => {
        const isPrivate = args.where?.private

        if (args.where?.private) {
          // @ts-expect-error field is not part of schema
          delete args.where.private
        }

        return recordingCount.resolve(
          root,
          {
            ...args,
            where: {
              ...args.where,
              ...(isPrivate
                ? { OR: [{ publishStatus: 'Published' }, { userId: ctx.user?.id }] }
                : {})
            }
          },
          ctx,
          info
        )
      }
    })
  }
})

builder.mutationFields((t) => {
  const createOneRecording = createOneRecordingMutationObject(t)

  return {
    createOneRecording: t.prismaField({
      ...createOneRecording,
      args: {
        ...createOneRecording.args,
        data: t.arg({ type: RecordingCreateInputWithFile, required: true })
      },
      async resolve(query, root, args, context, info) {
        const userId = context.user?.id

        if (!userId || !args.data.song?.connect?.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const song = await context.prisma.song.findFirst({
          where: { id: { equals: args.data.song.connect.id } }
        })

        if (song?.creatorId !== userId && song?.publishStatus !== 'Published') {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        if (args.data.url) {
          args.data.type = 'Raw'
        }

        args.data.user = { connect: { id: context.user?.id } }
        const argsAfterUpload: typeof args = await handleUploads(args)

        return createOneRecording.resolve(query, root, argsAfterUpload, context, info)
      }
    })
  }
})

// ==================================================== Score ====================================================

export const scoreWhereWithPrivateField = builder
  .inputRef<Prisma.ScoreWhereInput & { private: boolean }>('ScoreWhereWithPrivateField')
  .implement({
    fields: (t) => ({
      ...ScoreWhereInputFields(t),
      private: t.field({ required: true, type: 'Boolean' })
    })
  })

export const ScoreCreateInputWithFile = builder
  .inputRef<Prisma.ScoreCreateInput & { url: UploadResultImpl }>('ScoreCreateInputWithFile')
  .implement({
    fields: (t) => ({
      ...ScoreCreateInputFields(t),
      // @ts-expect-error
      url: t.field({ type: 'Upload' })
    })
  })

builder.queryFields((t) => {
  const findManyScore = findManyScoreQueryObject(t)
  const scoreCount = countScoreQueryObject(t)

  return {
    scores: t.prismaField({
      ...findManyScore,
      args: { ...findManyScore.args, where: t.arg({ type: scoreWhereWithPrivateField }) },
      resolve: async (query, root, args, ctx, info) => {
        const isPrivate = args.where?.private
        if (args.where?.private) {
          // @ts-expect-error field is not part of schema
          delete args.where.private
        }

        return findManyScore.resolve(
          query,
          root,
          {
            ...args,
            where: {
              ...args.where,
              ...(isPrivate
                ? { OR: [{ publishStatus: 'Published' }, { userId: ctx.user?.id }] }
                : {})
            }
          },
          ctx,
          info
        )
      }
    }),
    scoresCount: t.field({
      args: { ...scoreCount.args, where: t.arg({ type: scoreWhereWithPrivateField }) },
      type: 'Int',
      resolve: async (root, args, ctx, info) => {
        const isPrivate = args.where?.private

        if (args.where?.private) {
          // @ts-expect-error field is not part of schema
          delete args.where.private
        }

        return scoreCount.resolve(
          root,
          {
            ...args,
            where: {
              ...args.where,
              ...(isPrivate
                ? { OR: [{ publishStatus: 'Published' }, { userId: ctx.user?.id }] }
                : {})
            }
          },
          ctx,
          info
        )
      }
    })
  }
})

builder.mutationFields((t) => {
  const createOneScore = createOneScoreMutationObject(t)

  return {
    createOneScore: t.prismaField({
      ...createOneScore,
      args: {
        ...createOneScore.args,
        data: t.arg({ type: ScoreCreateInputWithFile, required: true })
      },
      async resolve(query, root, args, context, info) {
        const userId = context.user?.id

        if (!userId || !args.data.song?.connect?.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const song = await context.prisma.song.findFirst({
          where: { id: { equals: args.data.song.connect.id } }
        })

        if (song?.creatorId !== userId && song?.publishStatus !== 'Published') {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        args.data.user = { connect: { id: context.user?.id } }
        const argsAfterUpload: typeof args = await handleUploads(args)

        return createOneScore.resolve(query, root, argsAfterUpload, context, info)
      }
    })
  }
})
