import { sign } from 'jsonwebtoken'
import { compare, hash } from 'bcrypt'

import env from '../../util/env'
import { JWTPayload } from '../../types'
import AppError, { AppErrorType } from '../../AppError'
import { builder } from '../../builder'
import { UserObject } from '../../generated/objects'

// maybe move to env variable?
const userLockAttemptLimit = 10

builder.objectType('AuthPayload', {
  fields: (t) => ({
    token: t.exposeString('token', {}),
    user: t.prismaField({
      type: 'User',
      description: UserObject.description,
      nullable: true,
      resolve: async (query, root, args, ctx) =>
        ctx.user
          ? ctx.prisma.user.findUniqueOrThrow({
              where: { id: ctx.user?.id },
              ...query
            })
          : null
    })
  })
})

builder.mutationField('login', (t) =>
  t.field({
    type: 'AuthPayload',
    args: { emailOrName: t.arg({ type: 'String' }), password: t.arg({ type: 'String' }) },
    async resolve(root, { password, emailOrName }, ctx) {
      const user = await ctx.prisma.user.findFirst({
        where: { OR: [{ email: emailOrName || undefined }, { name: emailOrName || undefined }] }
      })

      if (!user || user.isLocked) {
        throw new AppError(AppErrorType.AUTH_INVALID)
      }

      const passwordValid = await compare(password, user.password)

      if (!passwordValid) {
        const { failCount } = await ctx.prisma.user.update({
          data: {
            failCount: {
              increment: 1
            }
          },
          where: {
            id: user.id
          }
        })

        if (failCount > userLockAttemptLimit) {
          await ctx.prisma.user.update({
            data: {
              isLocked: true
            },
            where: {
              id: user.id
            }
          })

          throw new AppError(AppErrorType.AUTH_LOCKED)
        }
        throw new AppError(AppErrorType.AUTH_INVALID)
      }

      await ctx.prisma.user.update({
        data: {
          failCount: 0
        },
        where: {
          id: user.id
        }
      })

      const payload: JWTPayload = {
        id: user.id,
        isAdmin: user.isAdmin
      }

      const token = sign(payload, env.APP_SECRET)

      return { token, user }
    }
  })
)

builder.mutationField('signup', (t) =>
  t.field({
    type: 'AuthPayload',
    args: {
      email: t.arg({ type: 'String', required: true }),
      password: t.arg({ type: 'String', required: true }),
      userName: t.arg({ type: 'String', required: true })
    },
    resolve: async (parent, { email, password, userName }, ctx) => {
      const hashedPassword = await hash(password, env.SALT_ROUNDS)
      const user = await ctx.prisma.user.create({
        data: {
          email,
          password: hashedPassword,
          name: userName
        }
      })
      return {
        // never make a user admin
        token: sign({ id: user.id, isAdmin: false }, env.APP_SECRET),
        user
      }
    }
  })
)
